import React from "react";
import PropTypes from "prop-types";
import {
  Grid,
  Typography,
  Box,
  IconButton,
  Hidden,
  withStyles,
  withWidth,

  TextField,
  Icon,
  colors
} from "@material-ui/core";
import { isWidthUp } from "@material-ui/core/withWidth";
import PhoneIcon from "@material-ui/icons/Phone";
import MailIcon from "@material-ui/icons/Mail";
// import WaveBorder from "../../../shared/components/WaveBorder";
import transitions from "@material-ui/core/styles/transitions";
// import ColoredButton from "../../../shared/components/ColoredButton";
import { makeStyles } from '@material-ui/core/styles';
import { loadCSS } from 'fg-loadcss';
import { Link } from 'react-router-dom';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem";

import Button from '@material-ui/core/Button';

import stylesCss from "../../../src/assets/css/views/global.module.css"

const useStyles = theme => ({
  email: {

    width: '500x',
    paddingTop: '10px',
    paddingBottom: '10px'

  }


});



const styles = theme => ({

  footerInner: {
    backgroundColor: '#054737',
    marginTop: "0px",


    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(5),
    paddingBottom: theme.spacing(6),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(2),
      paddingLeft: theme.spacing(5),
      paddingRight: theme.spacing(5),
      paddingBottom: theme.spacing(6)
    },
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(2),
      paddingLeft: theme.spacing(5),
      paddingRight: theme.spacing(5),
      paddingBottom: theme.spacing(6)
    }
  },
  brandText: {
    fontFamily: "'Baloo Bhaijaan', cursive",
    fontWeight: 400,
    color: theme.palette.common.white
  },
  footerLinks: {
    marginTop: theme.spacing(2.5),
    marginBot: theme.spacing(1.5),
    color: theme.palette.common.white
  },
  infoIcon: {
    color: `${theme.palette.common.white} !important`,
    backgroundColor: "#33383b !important"
  },
  socialIcon: {
    fill: theme.palette.common.white,
    backgroundColor: "#33383b",
    borderRadius: theme.shape.borderRadius,
    "&:hover": {
      backgroundColor: "#000000"
    }
  },
  link: {
    cursor: "Pointer",
    color: theme.palette.common.white,
    transition: transitions.create(["color"], {
      duration: theme.transitions.duration.shortest,
      easing: theme.transitions.easing.easeIn
    }),
    "&:hover": {
      color: theme.palette.primary.light
    }
  },
  whiteBg: {
    backgroundColor: theme.palette.common.white
  },


});





function Footer(props) {

  const { classes, theme, width } = props;
  const styleClass = useStyles()

  React.useEffect(() => {
    loadCSS(
      'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
      document.querySelector('#font-awesome-css')
    );
  }, []);

  return (
    <footer style={{ backgroundColor: '#054737' }} >
      <div className={classes.footerInner} >
        <GridContainer>
          <GridItem xs={12}  md={4} >
              <Typography style={{ color: "#FFFFFF" }}  className={stylesCss.inputEmail}>
                Dapatkan informasi terkini:
            </Typography>
           

            <TextField id="outlined-basic" className={stylesCss.inputEmail} placeholder="masukan alamat email" variant="outlined" InputProps={{
              style: { width: '300px', height: '40px', backgroundColor: 'white', borderRadius:'4px' }
            }} />
                        

              <Button variant="contained" className={stylesCss.subscribe} >
                Subsrcibe
      </Button>
            
          </GridItem>



          <GridItem xs={12} md={4} >
            <Typography className="text-white" style={{ marginLeft: "20px", color: "#FFFFFF" }}>
              Tetap terhubung:
            </Typography>
            <Box display="flex" style={{ marginLeft: "20px" }}>
              <a href="http://facebook.com" >
                <Icon className="fab fa-facebook-square" style={{ color: 'white', fontSize: '50px' }} />
              </a>
              <a href="https://plus.google.com/" >
                <Icon className="fab fa-google-plus-square" style={{ color: 'white', fontSize: '50px' }} />

              </a>
              <a href="http://instagram.com" >
                <Icon className="fab fa-instagram" style={{ color: 'white', fontSize: '50px' }} />
              </a>
              <a href="http://twitter.com" >
                <Icon className="fab fa-twitter-square" style={{ color: 'white', fontSize: '50px' }} />
              </a>


            </Box>
          </GridItem>

          <GridItem xs={12} md={4} >
            <Typography className="text-white" style={{ marginLeft: "20px", color: "#FFFFFF" }}>
              Hubungi Kami:
            </Typography>

            <Box display="flex" style={{ marginLeft: "20px" }}>

              <div style={{ color: "white" }}>

                <Icon className="fas fa-map-marker-alt" style={{ fontSize: '20px' }} />
                <text style={{ fontSize: "15px", marginLeft: "5px" }}>
                  Gedung Manggala Wanabakti Blok I lt. 2 Jl. Jenderal Gatot Subroto - Jakarta 10270, Po Box 6505, Indonesia
                   </text>

              </div>
            </Box>

            <Box display="flex" style={{ marginLeft: "20px" }}>

              <div style={{ color: "white", marginTop: '5px' }}>

                <Icon className="fas fa-phone-alt" style={{ fontSize: '20px' }} />
                <text style={{ fontSize: "15px", marginLeft: "5px" }}>0812-9879-7657 </text>
              </div>
            </Box>

            <Box display="flex" style={{ marginLeft: "20px" }}>

              <div style={{ color: "white", marginTop: '5px' }}>

                <Icon className="fas fa-envelope-open-text" style={{ fontSize: '20px' }} />
                <text style={{ fontSize: "15px", marginLeft: "5px" }}>pusdatin@menlhk.go.id </text>
              </div>
            </Box>


          </GridItem>

        </GridContainer>

      </div>
      <div style={{ paddingTop: '0px', paddingLeft: '20px', textAlign: 'center' }}>
        <text style={{ color: 'white', marginLeft: '5px', }}>© Copyright 2020 APIK, Powered by <a style={{ cursor: 'pointer' }} onClick={() => window.location.href = "https://piarea.co.id/"}>piarea.co.id</a></text>
      </div>

    </footer>
  );
}

Footer.propTypes = {
  theme: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,

};

export default withWidth()(withStyles(styles, { withTheme: true })(Footer));
