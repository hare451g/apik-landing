import axios from 'axios';

const config = {
    headers: {
        'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    }
};
const base = 'https://pi-dev.co.id/apik/api/'
axios.defaults.baseURL = base;
axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token')
const sso = 'https://pi-dev.co.id/apik/sso/api/';
const ssoLogin = 'https://pi-dev.co.id/apik/sso/';


const getLayers = () => {
    axios.defaults.baseURL = base;
    return (
        axios.get('map-layers')
    )
};

const getMasterType = (type, key) => {
    axios.defaults.baseURL = base;
    let hasKey = ''
    if (key) {
        hasKey = ',group:' + key
    }
    return (
        axios.get('master-type?criteria=type:' + type + hasKey)
    )
};

const getGeojson = (type, filter) => {
    axios.defaults.baseURL = base;
    let hasKey = ''
    if (filter) {
        hasKey = '&filter=' + filter
    }
    return (
        axios.get('data-geo-json?layer=' + type + hasKey)
    )
};
const getGeojsonDesa = (type, id) => {
    axios.defaults.baseURL = base;
    let idProv = id.substring(0, 2)
    let idKab = id.substring(2, 4)

    return (
        axios.get('data-geo-json?layer=' + type + "&filter=" + idKab + "&prov_no=" + idProv)
    )
};



const getActivityById = (id) => {
    axios.defaults.baseURL = base;
    return (
        axios.get('activities?criteria=created_by:' + id)
    )
};


const createProgram = (data) => {

    var newData = new FormData()
    newData.append("name_program", data.name_program);
    newData.append("id_level_program", data.programLevel);
    newData.append("description_program", data.description_program);
    newData.append("pj_program", data.pj_program);
    newData.append("from_date", data.from_date);
    newData.append("to_date", data.to_date);
    // newData.append("province_id", data.idProvinsi);
    // newData.append("district_id", data.idKabupaten);
    // newData.append("subdistrict_id", data.idKecamatan);
    // newData.append("village_id", data.idKelurahan);
    newData.append("created_by", 1);
    newData.append("photo", data.upload);

    return (
        axios.post('program/post', newData)
    )
};

const editProgram = (data) => {

    var newData = new FormData()
    newData.append('_method', 'PATCH');
    newData.append("id_program", data.id_program);
    newData.append("name_program", data.name_program);
    newData.append("id_level_program", data.programLevel);
    newData.append("description_program", data.description_program);
    newData.append("pj_program", data.pj_program);
    newData.append("from_date", data.from_date);
    newData.append("to_date", data.to_date);
    // newData.append("province_id", data.idProvinsi);
    // newData.append("district_id", data.idKabupaten);
    // newData.append("subdistrict_id", data.idKecamatan);
    // newData.append("village_id", data.idKelurahan);
    newData.append("image_survey", data.upload);
    newData.append("created_by", 1);

    if (data.upload) {
        newData.append("photo", data.upload);
    }
    console.log(data.upload, 'dada upload')

    return (
        axios.post('program/update', newData)
    )
};

const deleteProgram = (id) => {
    return (
        axios.delete('program/delete?id_program=' + id)
    )
}

const getProvinsi = () => {

    return (
        axios.get('provinsi')
    )
};

const getKabupaten = (id) => {

    return (
        axios.get('kab-kot?criteria=provinsi__name:' + id)
    )
};

const getKecamatan = (id) => {

    return (
        axios.get('subdistrict/get?id_district=' + id)
    )
};

const getKelurahan = (id) => {

    return (
        axios.get('village/get?id_subdistrict=' + id)
    )
};


const dataLegend = () => {

    return (
        axios.get('https://pi-dev.co.id/kalfor/be/pi/api/data/legend')
    )
}
const validateToken = () => {
    axios.defaults.baseURL = sso
    return (
        axios.get('user', {
            'headers': {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        })
    )
}


const searchData = (dropdown, iddes, stt) => {
    axios.defaults.baseURL = base;

    let state = stt
    console.log(state, 'sd')

    let objTingkat = JSON.parse(dropdown.tingkat)
    let objJenis = JSON.parse(dropdown.jenis)
    let objTahun = JSON.parse(dropdown.tahun)
    let objModel = dropdown.model ? JSON.parse(dropdown.tahun) : ''

    let url = 'faktor'

    let model = objModel ? '&model_id=' + objModel.type_id : ''
    let modelCrit = objModel ? ',model_id:' + objModel.type_id : ''

    url = url + '?wilayah_id=' + iddes + '&level_id=' + objTingkat.type_id + '&jenis_id=' + objJenis.type_id + '&period_id=' + objTahun.type_id + model


    return (
        axios.get(url)
    )
}
const getUserById = (id) => {
    axios.defaults.baseURL = base;
    return (
        axios.get('user?criteria=email:' + id)
    )
}
const getKerentananParamUser = (id) => {
    axios.defaults.baseURL = base;
    return (
        axios.get('kerentanan-params-user?criteria=data_collection_id:' + id)
    )
}
const getBahayaByDataVisual = (id) => {
    axios.defaults.baseURL = base;
    return (
        axios.get('visualization-data?criteria=user_id:' + id)
    )
}
const getValidasi = (id, dtingkat, djenis, dtahun, dmodel) => {
    axios.defaults.baseURL = base;
    let model = dmodel !== undefined ? ',model_id:' + dmodel.type_id : ''
    return (
        axios.get('ad-validations?criteria=wilayah_id:' + id + ',level_id:' + dtingkat.type_id + ',penyakit_id:' + djenis.type_id + ',period_id:' + dtahun.type_id + model)
    )
}
const getValidasiProfile = (id) => {
    axios.defaults.baseURL = base;

    return (
        axios.get('ad-validations?criteria=created_by:' + id)
    )
}
const getBahayaByParam = (id, dtingkat, djenis, dtahun, dmodel) => {
    axios.defaults.baseURL = base;
    let model = dmodel !== undefined ? ',model_id:' + dmodel.type_id : ''
    return (
        axios.get('bahaya-params?criteria=wilayah_id:' + id + ',penyakit_id:' + djenis.type_id + ',period_id:' + dtahun.type_id + model)
    )
}
const createValidasi = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.post('ad-validations', data)
    )
}
const editValidasi = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.put('ad-validations', data)
    )
}

const createAdaptasi = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.post('ad-adaptations', data)
    )
}
const editAdaptasi = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.put('ad-adaptations', data)
    )
}

const getAdaptasi = (id) => {
    axios.defaults.baseURL = base;
    return (
        axios.get('ad-adaptations?criteria=wilayah_id:' + id)
    )
}

const getLayerPrograms = (id) => {
    axios.defaults.baseURL = base;
    return (
        axios.get('list-layer-program')
    )
}

const getDefaultAdaptasi = (iddesa, levelId, jenisId, periodId, modelId) => {
    axios.defaults.baseURL = base;
    let model = modelId ? '&model_id=' + modelId : ''
    return (
        axios.get('master-adaptations-filtered?wilayah_id=' + iddesa + '&level_id=' + levelId + '&jenis_id=' + jenisId + '&period_id=' + periodId + model)
    )



}

const getAdaptasiProfile = (id) => {
    axios.defaults.baseURL = base;

    return (
        axios.get('ad-adaptations?criteria=created_by:' + id)
    )
}

const getKerentananTemplate = () => {
    return base + 'kerentanan-params-user/download-template';
}

const getProgramExcelTemplate = () => {
    return base + 'form-layers/download-template';
}

const importDataKerentanan = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.post('kerentanan-params-user/import', data)
    )
}

const deleteDataKerentanan = (iddata) => {

    axios.defaults.baseURL = base;
    return (
        axios.delete('kerentanan-params-user?data_collection_id=' + iddata)
    )
}

const getDataCollection = (id) => {
    axios.defaults.baseURL = base;

    return (
        axios.get('data-collection?criteria=created_by:' + id)
    )
}
const getlayerProgramExcel = (id) => {
    axios.defaults.baseURL = base;

    return (
        axios.get('form-data-layers?form_layer_id=' + id)
    )
}


const updateProfile = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.put('user', data)
    )
}
const getVisual = (id, dtingkat, djenis, dtahun, dmodel) => {
    axios.defaults.baseURL = base;
    let model = dmodel !== undefined ? ',model_id:' + dmodel.type_id : ''
    return (
        axios.get('visualization-data?criteria=wilayah_id:' + id + ',penyakit_id:' + djenis.type_id + ',period_id:' + dtahun.type_id + model)
    )
}

const createVisual = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.post('visualization-data', data)
    )
}
const editVisual = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.put('visualization-data', data)
    )
}

const login = (username, password,) => {
    axios.defaults.baseURL = ssoLogin
    var newData = new FormData()
    newData.append("grant_type", "password");
    newData.append("client_id", '1');
    newData.append("client_secret", 'W3kbyNIgXyKrbh5QB9lRADsonRfzWUqOPmr9F2JS');
    newData.append("username", username);
    newData.append("password", password);
    return (
        axios.post('oauth/token', newData)
    )
};

const register = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.post('user/register', data)
    )
}

const changePassword = (data) => {
    axios.defaults.baseURL = sso;
    return (
        axios.post('password/change', data)
    )
}

const uploadProgramExcel = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.post('form-layers/import', data)
    )
}

const uploadProgramAPI = (data) => {
    axios.defaults.baseURL = base;
    return (
        axios.post('api-layers', data)
    )
}

const getProgramExcelPin = (id) => {
    axios.defaults.baseURL = base;
    return (
        axios.get('form-data-layers?form_layer_id=' + id)
    )
}


export default {
    getProvinsi, getKabupaten, getKecamatan, getKelurahan,
    getLayers, getMasterType, getGeojson, searchData,
    getUserById, getActivityById, getKerentananParamUser, validateToken, updateProfile,
    getValidasi, getValidasiProfile, createAdaptasi, editValidasi, getBahayaByParam,
    getAdaptasi, getAdaptasiProfile, createValidasi, editAdaptasi, getGeojsonDesa,
    getBahayaByDataVisual, getVisual, createVisual, editVisual, getDefaultAdaptasi,
    getKerentananTemplate, getDataCollection, importDataKerentanan, deleteDataKerentanan,
    login, register, getLayerPrograms, uploadProgramAPI, uploadProgramExcel, getProgramExcelTemplate, getlayerProgramExcel, getProgramExcelPin, changePassword
}