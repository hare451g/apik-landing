import React, { Component } from "react";
import LoadingOverlay from 'react-loading-overlay';
import parse from 'html-react-parser';
import SunEditor from 'suneditor-react';
import api from '../../config/api'

// Material-ui core
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Tooltip from '@material-ui/core/Tooltip';
import Checkbox from '@material-ui/core/Checkbox';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import IconButton from '@material-ui/core/IconButton';
import TablePagination from '@material-ui/core/TablePagination';
import Chip from '@material-ui/core/Chip';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';


// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import SnackbarContent from "components/Snackbar/SnackbarContent.js";
import Snackbar from "components/Snackbar/Snackbar.js";

// Image
import noImage from '../../assets/img/no-image-box.png'

// Css
import styleCss from "../../../src/assets/css/views/global.module.css"
import 'suneditor/dist/css/suneditor.min.css';

//Icon
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import SearchIcon from '@material-ui/icons/Search';





class SurveiOnline extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataList: [],
            dataListConst: [],
            levelList: [],
            page: 0,
            rowsPerPage: 10,
            isLoadinOverlay: false,
            isLoading: false,
            isOpen: false,
            isChange: true,
            name: '',
            description: '',
            programLevel: "",
            start_date: '',
            end_date: '',
            isAlertOpen: false,
            AlertMessage: '',
            alertState: 'success',
            isHasPicture: false,
            isEdit: false,
            dialogText: '',
            isDialogOpen: false,
            dialogId: '',
            dialogState: '',
            status: false,
            isHasPicture: false,
            upload: ''

        }
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
        this.goDetail = this.goDetail.bind(this);
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onChangeValue = this.onChangeValue.bind(this);
        this.doSave = this.doSave.bind(this);
        this.doEdit = this.doEdit.bind(this);
        this.doDelete = this.doDelete.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
        this.openNotification = this.openNotification.bind(this);
        this.showEditForm = this.showEditForm.bind(this);
        this.openDialog = this.openDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.dialogStateChecker = this.dialogStateChecker.bind(this);
        this.doSearch = this.doSearch.bind(this);
    }

    handleValidation(isEdit) {
        let data = this.state;
        console.log(data, 'validate')
        let errorCount = 0;

        let name = false;
        if (!data.name) {
            name = true;
            errorCount++;
        }
        let description = false;
        if (!data.description) {
            description = true;
            errorCount++;
        }

        let start_date = false;
        data.start_date = data.start_date + ''; //jika angka convert dulu ke string seperti ini
        if (!data.start_date) {
            start_date = true;
            errorCount++;
        }
        let end_date = false;
        data.end_date = data.end_date + ''; //jika angka convert dulu ke string seperti ini
        if (!data.end_date) {
            end_date = true;
            errorCount++;
        }


        this.setState({
            nameError: name,
            descriptionError: description,
            start_dateError: start_date,
            end_dateError: end_date,
        })

        if (errorCount !== 0) {
            this.setState({ isDialogOpen: false })
            this.openNotification('error', 'Gagal Membuat Event, pastikan data sudah diisi semua!');
            return false;
        } else {
            return true
        }
    }
    handleChangePage(event, newPage) {
        window.scrollTo(0, 0);
        this.setState({ page: newPage });
    }
    doSearch(e) {
        let value = e.target.value

        let dataFiltered = this.state.dataListConst.filter((data) => {
            if (value == null || value == '') {
                //  this.setState({ page: 0 })

                return data
            }
            else if (data.name_survey.toLowerCase().includes(value.toLowerCase())) {

                return data
            }

        })
        this.setState({ search: e.target.value, page: 0, dataList: dataFiltered })
    }
    handleChangeRowsPerPage(event, newRow) {
        console.log(newRow)
        this.setState({ rowsPerPage: newRow.key, page: 0 })
    }
  
    componentDidMount() {
        window.document.title = 'Kalfor - Survei';
        this.getList();
    }

    closeNotification() {
        this.setState({ isAlertOpen: false });
    }
    openNotification(condition, Message) {
        if (condition == 'success') {
            this.setState({
                isAlertOpen: true,
                alertState: 'success',
                AlertMessage: Message
            })
        } else {
            this.setState({
                isAlertOpen: true,
                alertState: 'danger',
                AlertMessage: Message
            })
        }
        setTimeout(function () {
            this.closeNotification()
        }
            .bind(this), 3000);
    }

    getList() {
        this.setState({ isLoading: true, isHasPicture: true })
        api.getSurvey()
            .then((res) => {
                var arrayList = res.data.response.arrSurvey

                arrayList.sort(function (a, b) {
                    return new Date(b.start_date) - new Date(a.start_date);
                });
                console.log(arrayList, 'isi array list')
                this.setState({ dataList: arrayList, isLoading: false, dataListConst: arrayList })

            }).catch(err => {
                console.log(err.response, 'error')
                this.setState({ isLoading: false })
                this.openNotification('error', err.response ? err.response.data.message : 'Gagal mengambil data!')
            })
    }

    formatDate(dateWithTime) {
        const dateSeparateTime = dateWithTime.split(' ')
        const dateTemp = dateSeparateTime[0].split('-')
        return dateTemp[2] + '-' + dateTemp[1] + '-' + dateTemp[0]
    }
    openDialog(state, id) {
        var text = ""
        if (state == 'create') {
            text = "Apakah anda yakin dengan data ?"


        } else if (state == 'edit') {
            text = "Apakah anda yakin dengan data ?"
        } else {
            text = "Apakah anda yakin menghapus data ?"
        }
        this.setState({ isDialogOpen: true, dialogText: text, dialogId: id, dialogState: state });

    }
    closeDialog() {

        this.setState({ isDialogOpen: false });
    }

    handleClickOpen() {
        this.setState({
            isOpen: true,
            name: '',
            description: '',
            programLevel: "",
            start_date: '',
            end_date: '',
            idProvinsi: '',
            idKabupaten: '',
            idKecamatan: '',
            idKelurahan: '',
            pj_program: '',
            status: 'active',
            uploadError: false,
            gambar: '',
            isHasPicture:false
        });
    }
    handleClose() {
        this.setState({ isOpen: false, isEdit: false });
    }
    handleChange() {
        this.setState({ isChange: true });
    }
    onChangeValue(e, idNumber) {
        //1  kabupaten
        //2 kecamatan
        //3 kelurahan
        if (e.target.name !== 'upload') {
            this.setState({ [e.target.name]: e.target.value, [e.target.name + 'Error']: false })
        } else {
            this.setState({ [e.target.name]: e.target.files[0] })
            console.log(e.target.files[0])
        }
    }

    getProgramById(id) {
        this.setState({ isLoadingOverlay: true, isHasPicture: true })
        api.getProgramById(id)
            .then(async (res) => {
                console.log(res, 'isires')
                var data = res.data.response.arrProgram;


                this.setState({

                    isLoadingOverlay: false,
                    id_program: data.id_program,
                    name_program: data.name_program,
                    programLevel: data.id_level_program,
                    start_date: data.start_date,
                    end_date: data.end_date,
                    idProvinsi: data.province_id,
                    idKabupaten: data.district_id,
                    idKecamatan: data.subdistrict_id,
                    idKelurahan: data.village_id,
                    photo: data.photo,
                    description_program: data.description_program,
                    pj_program: data.program_pj,
                    upload: ''


                })
            }).catch(err => {
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', err.response ? err.response.data.message : 'Gagal mengambil data!')
            })
    }
    showEditForm(data) {
        this.handleClickOpen();

        this.setState({
            isEdit: true,
            id_survey: data.id_survey,
            name: data.name_survey,
            description: data.desc_survey,
            status: data.status_survey,
            start_date: data.start_date,
            end_date: data.end_date,
            upload: '',
            isHasPicture: true,
            photo: data.image_survey

        })
        // this.getProgramById(id);
    }
    checkDateInRange(a, b) {
        var today = new Date();
        var newA = new Date(a);
        var newB = new Date(b);
        if ((newA <= today) && (today <= newB)) {
            return 'active'
        } else if ((newA > today))
            return 'not started'
        else {
            return 'finish'
        }
    }

    doSave() {
        var isAllowed = this.handleValidation();
        if (!isAllowed) {
            return
        }

        this.setState({ isLoadingDetail: true });

        var data = {
            name: this.state.name,
            description: this.state.description,
            start_date: this.state.start_date,
            end_date: this.state.end_date,
            status: this.state.status,
            upload: this.state.upload,
            created_by: 2
        }
        this.setState({ isLoadingOverlay: true });
        api.createSurvey(data)
            .then((res) => {
                this.setState({ isLoadingOverlay: false, isOpen: false });
                console.log(res, 'isi res');
                this.openNotification('success', 'Berhasil Membuat Survei');
                this.getList();
            }).catch(err => {
                // this.setState({ isLoading: false });
                this.setState({ isLoadingOverlay: false })
                console.log(err.response, 'isi error')
                this.openNotification('error', 'Gagal Membuat Survei!')


            })
    }
    doEdit() {
        var isAllowed = this.handleValidation(true);
        if (!isAllowed) {
            return
        }
        var data = {
            id_survey: this.state.id_survey,
            name: this.state.name,
            description: this.state.description,
            start_date: this.state.start_date,
            end_date: this.state.end_date,
            status: this.state.status,
            upload: this.state.upload

        }
        this.setState({ isLoadingOverlay: true });
        api.editSurvey(data)
            .then((res) => {
                this.setState({ isLoadingOverlay: false, isOpen: false, isEdit: false });
                this.openNotification('success', 'Berhasil Mengubah Program');
                this.getList();
            }).catch(err => {
                // this.setState({ isLoading: false });
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', 'Gagal Mengubah Program, pastikan data sudah diisi semua!')


            })
    }
    doDelete(id) {
        this.setState({ isLoadingOverlay: true });
        api.deleteSurvey(id)
            .then((res) => {
                this.setState({ isLoadingOverlay: false, isDialogOpen: false });

                console.log(res, 'isi res');
                this.openNotification('success', 'Berhasil Menghapus Survei');
                this.getList();
            }).catch(err => {
                // err.response
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', 'Gagal Menghapus Survei !')
            })
    }
    dialogStateChecker(state, id) {
        if (state == 'create') {
            this.doSave()
        }
        else if (state == 'edit') {
            this.doEdit()
        }
        else if (state == 'delete') {
            this.doDelete(id)
        }
    }
    renderDialog() {
        return (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    // onClose={this.closeDialog}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{this.state.dialogText}</DialogTitle>

                    <DialogActions>
                        <Button onClick={this.closeDialog} color="primary">
                            Tidak </Button>
                        <Button onClick={() => this.dialogStateChecker(this.state.dialogState, this.state.dialogId)} color="primary" autoFocus>
                            Ya</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }

    renderTable() {
        return (
            <GridContainer>
                {
                    this.state.dataList.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((data, index) => {
                        return (
                            <Grid xs={12} sm={12} md={12} key={'tableList' + index + 1} className={styleCss.dataList}>
                                <Card style={{ cursor: 'pointer' }}>
                                    <CardHeader color="success" style={{ textAlign: 'center', fontWeight: '500' }} onClick={() => this.goDetail(data.id_program)}>
                                        <h4 > [{data.code_survey}] {data.name_survey}</h4><div style={{ position: 'absolute', right: 10, top: 5 }}>{index + 1 + this.state.page * 10}
                                        </div>
                                    </CardHeader>
                                    <CardBody onClick={() => this.goDetail(data.id_survey)}>
                                        <GridContainer>
                                            <GridItem md={3} xs={12}>
                                                <img src={data.image_survey ? data.image_survey : noImage} style={{ maxWidth: '100%', itemAlign: 'center' }} />
                                            </GridItem>
                                            <GridItem md={9} xs={12}>

                                                <p style={{
                                                    overflow: 'hidden',
                                                    textOverflow: 'ellipsis',
                                                    maxHeight: 100,
                                                    textAlign: 'justify'
                                                }} >
                                                    {parse(data.desc_survey + '')}

                                                </p>
                                                <table>
                                                    <tbody>

                                                        <tr>
                                                            <td>Durasi Survei</td>
                                                            <td>: {this.formatDate(data.start_date)} sampai {this.formatDate(data.end_date)} &nbsp;&nbsp;
                                                                <Chip

                                                                    label={this.checkDateInRange(data.start_date, data.end_date) == 'active' ? " Sedang Berjalan" : this.checkDateInRange(data.start_date, data.end_date) == 'not started' ? 'Survei belum berjalan' : 'Survei sudah berakhir'}
                                                                    clickable
                                                                    color={this.checkDateInRange(data.start_date, data.end_date) == 'active' ? "primary" : this.checkDateInRange(data.start_date, data.end_date) == 'not started' ? 'default' : 'secondary'}
                                                                    size="small"

                                                                />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Status</td>
                                                            <td>: <Chip

                                                                label={data.status_survey}
                                                                clickable
                                                                color={data.status_survey == 'active' ? 'primary' : 'secondary'}
                                                                size="small"

                                                            /></td>
                                                        </tr>
                                                        {/* <tr>
                                                            <td> {this.checkDateInRange(data.start_date, data.end_date) ? <Chip

                                                                label="Sedang Berjalan"
                                                                clickable
                                                                color="primary"
                                                                size="small"

                                                            /> : ''}</td>
                                                        </tr> */}
                                                    </tbody>
                                                </table>

                                            </GridItem>
                                        </GridContainer>
                                    </CardBody>
                                    <CardFooter chart style={{ cursor: 'auto' }}>

                                        <div></div>
                                        <div style={{ float: 'right !important' }}>
                                            <Tooltip title="Edit">
                                                <IconButton aria-label="delete" onClick={() => this.showEditForm(data)} size="medium">
                                                    <EditIcon fontSize="inherit" />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Hapus">
                                                <IconButton aria-label="delete" onClick={() => this.openDialog('delete', data.id_survey)} size="medium">
                                                    <DeleteIcon fontSize="inherit" />
                                                </IconButton>
                                            </Tooltip>

                                        </div>




                                    </CardFooter>
                                </Card>

                            </Grid>
                        )

                    })
                }
            </GridContainer>
        )
    }

    render() {
        return (
            <div>
                <LoadingOverlay
                    active={this.state.isLoadingOverlay}
                    spinner
                    text='mengambil data mohon tunggu...'
                    style={{ display: 'none' }}
                >
                </LoadingOverlay>
                <Snackbar
                    place="tc"
                    color={this.state.alertState}
                    icon={this.state.alertState == 'success' ? CheckIcon : ErrorIcon}
                    message={this.state.AlertMessage}
                    open={this.state.isAlertOpen}
                    closeNotification={() => this.closeNotification}
                    close
                />
                <Card>
                    <Dialog className={styleCss.modal}

                        maxWidth={'md'}
                        fullWidth={true}
                        open={this.state.isOpen}
                        // onClose={this.handleClose}
                        scroll={'paper'}
                        aria-labelledby="scroll-dialog-title"
                        aria-describedby="scroll-dialog-description"
                    >
                        <DialogTitle id="scroll-dialog-title" >
                            {this.state.isEdit ? 'Edit Program' : 'Buat Survei '}
                            <img src='https://img.icons8.com/metro/26/000000/close-window.png' onClick={this.handleClose} className={styleCss.closeWindows} />

                        </DialogTitle>
                        <DialogContent dividers={true} >
                            <DialogContentText className={styleCss.dialogConText}
                                id="scroll-dialog-description"
                                // ref={'modal1'}
                                tabIndex={-1}

                            >
                                <form >
                                    <input name="_method" type="hidden" value="PATCH" />
                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Nama Survei</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <Input placeholder="Nama Survei" required name="name" onChange={(e) => this.onChangeValue(e)} value={this.state.name} inputProps={{ 'aria-label': 'description' }} className={styleCss.widthInput} />
                                                {
                                                    this.state.nameError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                }
                                            </GridItem>
                                        </GridContainer>
                                    </div>
                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Deskripsi</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <SunEditor name="summary" onChange={(e) => this.setState({ description: e, descriptionError: false })} setContents={this.state.description} />
                                                {
                                                    this.state.descriptionError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                }
                                            </GridItem>
                                        </GridContainer>
                                    </div>
                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Durasi Survei</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <form className={styleCss.containerDate} noValidate>
                                                    <div>
                                                        <TextField
                                                            name="start_date"
                                                            error={this.state.start_dateError}
                                                            value={this.state.start_date}
                                                            onChange={(e) => this.onChangeValue(e)}
                                                            format="dd/mm/yyyy"
                                                            type="date"
                                                            // defaultValue="2017-00-00"
                                                            className={styleCss.textFieldDate}
                                                            InputLabelProps={{
                                                                shrink: true,

                                                            }}

                                                        />
                                                        {
                                                            this.state.start_dateError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                        }
                                                    </div>
                                                    <p style={{ margin: "0px 10px" }}>  s/d</p>
                                                    <div>
                                                        <TextField
                                                            name="end_date"
                                                            error={this.state.end_dateError}
                                                            value={this.state.end_date}
                                                            onChange={(e) => this.onChangeValue(e)}
                                                            format="dd/mm/yyyy"
                                                            type="date"
                                                            // defaultValue="2017-00-00"
                                                            className={styleCss.textFieldDate}
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                        />
                                                        {
                                                            this.state.end_dateError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                        }
                                                    </div>
                                                </form>

                                            </GridItem>
                                        </GridContainer>
                                    </div>

                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Logo</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                {
                                                    this.state.isHasPicture ? <Button variant="contained" component="span" className={styleCss.buttonUpload} onClick={() => this.setState({ isHasPicture: false })} > Change Picture </Button> :
                                                        <div>
                                                            <input
                                                                accept="image/*"

                                                                onChange={(e) => this.onChangeValue(e)}
                                                                name="upload"
                                                                id="contained-button-file"
                                                                type="file"
                                                            />

                                                            {
                                                                this.state.uploadError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                            }
                                                            <FormHelperText >*Format yang diperbolehkan adalah jpeg, jpg , png dan ukuran maksimal 2mb</FormHelperText>
                                                            {
                                                                this.state.isEdit ? <a style={{ cursor: 'pointer' }} onClick={() => this.setState({ isHasPicture: true })} > Batal </a> : ''
                                                            }





                                                        </div>
                                                }



                                            </GridItem>


                                        </GridContainer>

                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>

                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                {
                                                    this.state.isHasPicture ?
                                                        <img style={{ maxWidth: '50%', marginTop: 20 }} src={this.state.photo}></img>
                                                        :
                                                        <div>
                                                            {
                                                                this.state.upload ? <img style={{ maxWidth: '50%', marginTop: 20 }} src={URL.createObjectURL(this.state.upload)}></img> : ''
                                                            }
                                                            <div><label>{this.state.upload ? this.state.upload.name : ''}</label></div>
                                                        </div>
                                                }




                                            </GridItem>
                                        </GridContainer>
                                    </div>


                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Status </label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <RadioGroup aria-label="status" name="status" value={this.state.status} onChange={(e) => this.onChangeValue(e)}>
                                                    <FormControlLabel value="active" control={<Radio />} label="Aktif" />
                                                    <FormControlLabel value="not active" control={<Radio />} label="Tidak Aktif" />
                                                </RadioGroup>

                                            </GridItem>
                                        </GridContainer>
                                    </div>

                                    <div style={{ marginBottom: "20px", marginTop: "10px", float: "right" }}>

                                        <Button variant="contained" component="span" onClick={this.handleClose} className={styleCss.buttonClose}>Kembali</Button>

                                        {
                                            this.state.isEdit ? <Button variant="contained" component="span" onClick={this.doEdit} className={styleCss.buttonConfirm}>Perbaharui</Button> :
                                                <Button variant="contained" component="span" type="submit" onClick={this.doSave} className={styleCss.buttonConfirm}>Simpan</Button>
                                        }




                                    </div>

                                </form>
                            </DialogContentText>
                        </DialogContent>

                    </Dialog>
                </Card>
                {this.renderDialog()}

                <Card>
                    <CardHeader color="success">
                        <h4 >Survei</h4>
                    </CardHeader>
                    <CardBody>

                        <div>
                            <Button onClick={this.handleClickOpen} variant="contained" component="span" className={styleCss.buttonConfirm}>Buat Survei</Button>

                        </div>
                        <div>
                        <Paper component="form" className={styleCss.paperSearch}>
                                <IconButton type="submit" className={styleCss.iconButton} aria-label="search" >
                                    <SearchIcon />
                                </IconButton>
                                <InputBase
                                    className={styleCss.inputSearch}
                                    placeholder="Cari Survei..."
                                    inputProps={{ 'aria-label': 'Cari Survei...' }}
                                    onChange={(e) => this.doSearch(e)}
                                />



                            </Paper>

                            {/* <input type="text" name="search" onChange={(e) => this.doSearch(e)} placeholder="Cari Survei.." className={styleCss.searchBox} /> */}
                        </div>

                        {/* <GridContainer >
                            <GridItem xs={12} md={3} >
                                <FormControl className={styleCss.dialogConText}   >
                                    <Select
                                        className={styleCss.selectEmpty}
                                    // onChange={(e) => this.onChangeValue(e, 2)}
                                    // value={this.state.id_district}
                                    // name=""                          
                                    >
                                        <MenuItem value="" disabled>
                                            Pilih Jenis lemabaga </MenuItem>
                                        <MenuItem value=""> Lembaga A</MenuItem>
                                        <MenuItem value=""> Lembaga B</MenuItem>
                                        <MenuItem value=""> Lembaga C</MenuItem>
                                    </Select>
                                </FormControl>

                            </GridItem>

                            <GridItem xs={12} md={3} >
                                <FormControl className={styleCss.dialogConText}   >
                                    <Select
                                        className={styleCss.selectEmpty}
                                    // onChange={(e) => this.onChangeValue(e, 2)}
                                    // value={this.state.id_district}
                                    // name=""                          
                                    >
                                        <MenuItem value="" disabled>
                                            Pilih Nilai </MenuItem>
                                        <MenuItem value=""> Nilai A</MenuItem>
                                        <MenuItem value=""> Nilai B</MenuItem>
                                        <MenuItem value=""> Nilai C</MenuItem>
                                    </Select>
                                </FormControl>

                            </GridItem>

                            <GridItem xs={12} md={3} >
                                <FormControl className={styleCss.dialogConText}   >
                                    <Select
                                        className={styleCss.selectEmpty}
                                    // onChange={(e) => this.onChangeValue(e, 2)}
                                    // value={this.state.id_district}
                                    // name=""                          
                                    >
                                        <MenuItem value="" disabled>
                                            Pilih Tahun </MenuItem>
                                        <MenuItem value=""> Tahun A</MenuItem>
                                        <MenuItem value=""> Tahun B</MenuItem>
                                        <MenuItem value="">Tahun C</MenuItem>
                                    </Select>
                                </FormControl>

                            </GridItem>

                            <GridItem xs={12} md={3} >
                                <Button style={{ backgroundColor: "#054737", color: "white", fontWeight: 'bold', marginTop: '0px', height: '36px', width: '100%' }}>Filter</Button>

                            </GridItem>

                        </GridContainer> */}






                    </CardBody>

                    <CardBody>
                        {this.state.dataList.length == 0 && this.state.isLoading == false ?
                            <Card style={{ cursor: 'pointer' }}>
                                <CardHeader color="success" style={{ textAlign: 'center', fontWeight: '500' }}>
                                    <h4 >Data tidak ada</h4>
                                </CardHeader>

                            </Card> : ''}
                        {this.state.isLoading ? <div style={{ textAlign: "center" }}><CircularProgress /></div> : this.renderTable()}

                    </CardBody>
                    <div style={{ float: 'right', width: '100%' }}>
                        <TablePagination
                            rowsPerPageOptions={[10]}

                            count={this.state.dataList.length}
                            rowsPerPage={this.state.rowsPerPage}
                            page={this.state.page}
                            SelectProps={{
                                inputProps: { 'aria-label': 'Jumlah Per halaman' },
                                native: false,
                            }}
                            onChangePage={this.handleChangePage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                        />
                    </div>
                </Card>


            </div>
        )
    }

}
export default SurveiOnline