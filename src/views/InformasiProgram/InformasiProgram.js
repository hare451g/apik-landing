import React, { Component } from "react";
import api from '../../config/api';
import parse from 'html-react-parser';
import SunEditor from 'suneditor-react';
import LoadingOverlay from 'react-loading-overlay';
import LoadingBar from 'react-top-loading-bar';

// core material-ui
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import TablePagination from '@material-ui/core/TablePagination';
import Chip from '@material-ui/core/Chip'
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';


// Core Components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import SnackbarContent from "components/Snackbar/SnackbarContent.js";
import Snackbar from "components/Snackbar/Snackbar.js";

// Material-Icon
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import SearchIcon from '@material-ui/icons/Search';

// Image
import noImage from '../../assets/img/no-image-box.png'

// Css Style
import styleCss from "../../../src/assets/css/views/global.module.css"
import 'suneditor/dist/css/suneditor.min.css';







class InformasiProgram extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataList: [],
            dataListConst: [],
            levelList: [],
            page: 0,
            rowsPerPage: 10,
            isLoadinOverlay: false,
            isLoading: false,
            isOpen: false,
            isChange: true,
            name_program: '',
            description_program: '',
            programLevel: "",
            from_date: '',
            to_date: '',
            idProvinsi: '',
            idKabupaten: '',
            idKecamatan: '',
            idKelurahan: '',
            pj_program: '',
            upload: '',
            listProvinsi: [],
            listKabupaten: [],
            listKecamatan: [],
            listKelurahan: [],
            isAlertOpen: false,
            AlertMessage: '',
            alertState: 'success',
            isHasPicture: false,
            isEdit: false,
            dialogText: '',
            isDialogOpen: false,
            dialogId: '',
            dialogState: '',
            search: ''

        }
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
        this.goDetail = this.goDetail.bind(this);
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onChangeValue = this.onChangeValue.bind(this);
        this.doSave = this.doSave.bind(this);
        this.doEdit = this.doEdit.bind(this);
        this.doSearch = this.doSearch.bind(this);
        this.doDelete = this.doDelete.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
        this.openNotification = this.openNotification.bind(this);
        this.showEditForm = this.showEditForm.bind(this);
        this.openDialog = this.openDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.dialogStateChecker = this.dialogStateChecker.bind(this);
    }

    handleChangePage(event, newPage) {
        window.scrollTo(0, 0);
        this.setState({ page: newPage });
    }
    handleChangeRowsPerPage(event, newRow) {
        console.log(newRow)
        this.setState({ rowsPerPage: newRow.key, page: 0 })
    }
   
    componentDidMount() {
        window.document.title = 'Kalfor - Informasi Program'
        this.getList();
    }

    closeNotification() {
        this.setState({ isAlertOpen: false });
    }
    openNotification(condition, Message) {
        if (condition == 'success') {
            this.setState({
                isAlertOpen: true,
                alertState: 'success',
                AlertMessage: Message
            })
        } else {
            this.setState({
                isAlertOpen: true,
                alertState: 'danger',
                AlertMessage: Message
            })
        }
        setTimeout(function () {
            this.closeNotification()
        }
            .bind(this), 3000);
    }

    getList() {
        this.setState({ isLoading: true })
        this.LoadingBar.continuousStart()

        api.getProgram()
            .then((res) => {
                this.LoadingBar.complete()

                var arrayList = res.data.response.arrProgram
                arrayList.sort(function (a, b) {
                    return new Date(b.from_date) - new Date(a.from_date);
                });
                this.setState({ dataList: arrayList, dataListConst: arrayList, isLoading: false, levelList: res.data.response.arrLevel })

            }).catch(err => {
                this.LoadingBar.complete()

                console.log(err.response, 'isi response')
                this.setState({ isLoading: false })
                this.openNotification('error', err.response ? err.response.data.message : 'Gagal mengambil data!')
            })
    }

    formatDate(dateWithTime) {
        const dateSeparateTime = dateWithTime.split(' ')
        const dateTemp = dateSeparateTime[0].split('-')
        return dateTemp[2] + '-' + dateTemp[1] + '-' + dateTemp[0]
    }
    openDialog(state, id) {
        var text = ""
        if (state == 'create') {
            text = "Apakah anda yakin dengan data ?"


        } else if (state == 'edit') {
            text = "Apakah anda yakin dengan data ?"
        } else {
            text = "Apakah anda yakin menghapus data ?"
        }
        this.setState({ isDialogOpen: true, dialogText: text, dialogId: id, dialogState: state });

    }
    closeDialog() {

        this.setState({ isDialogOpen: false });
    }

    handleClickOpen() {
        this.setState({
            isOpen: true,
            name_program: '',
            description_program: '',
            programLevel: "",
            from_date: '',
            to_date: '',
            idProvinsi: '',
            idKabupaten: '',
            idKecamatan: '',
            idKelurahan: '',
            pj_program: '',
            upload: '',
            name_programError: false,
            description_programError: false,
            programLevelError: false,
            from_dateError: false,
            to_dateError: false,
            idProvinsiError: false,
            idKabupatenError: false,
            idKecamatanError: false,
            idKelurahanError: false,
            pj_programError: false,
            uploadError: false,
            gambar: ''
        });
    }
    handleClose() {
        this.setState({ isOpen: false, isEdit: false });
    }

    doSearch(e) {
        let value = e.target.value

        let dataFiltered = this.state.dataListConst.filter((data) => {
            if (value == null || value == '') {
                //  this.setState({ page: 0 })

                return data
            }
            else if (data.name_program.toLowerCase().includes(value.toLowerCase())) {

                return data
            }

        })
        this.setState({ search: e.target.value, page: 0, dataList: dataFiltered })
    }

    handleValidation(isEdit) {
        let data = this.state;
        console.log(data, 'validate')
        let errorCount = 0;

        let name_program = false;
        if (!data.name_program) {
            name_program = true;
            errorCount++;
        }
        let description_program = false;
        if (!data.description_program) {
            description_program = true;
            errorCount++;
        }
        let programLevel = false;
        if (!data.programLevel) {
            programLevel = true;
            errorCount++;
        }
        let from_date = false;
        data.from_date = data.from_date + ''; //jika angka convert dulu ke string seperti ini
        if (!data.from_date) {
            from_date = true;
            errorCount++;
        }
        let to_date = false;
        data.to_date = data.to_date + ''; //jika angka convert dulu ke string seperti ini
        if (!data.to_date) {
            to_date = true;
            errorCount++;
        }
        // let idProvinsi = false;
        // if (!data.idProvinsi) {
        //     idProvinsi = true;
        //     errorCount++;
        // }
        // let idKabupaten = false;
        // if (!data.idKabupaten) {
        //     idKabupaten = true;
        //     errorCount++;
        // }
        // let idKecamatan = false;
        // if (!data.idKecamatan) {
        //     idKecamatan = true;
        //     errorCount++;
        // }
        // let idKelurahan = false;
        // if (!data.idKelurahan) {
        //     idKelurahan = true;
        //     errorCount++;
        // }
        let pj_program = false;
        if (!data.pj_program) {
            pj_program = true;
            errorCount++;
        }
        let upload = false;
        if (!data.upload && !isEdit) {
            upload = true;
            errorCount++;
        }
        this.setState({
            name_programError: name_program,
            description_programError: description_program,
            programLevelError: programLevel,
            from_dateError: from_date,
            to_dateError: to_date,
            // idProvinsiError: idProvinsi,
            // idKabupatenError: idKabupaten,
            // idKecamatanError: idKecamatan,
            // idKelurahanError: idKelurahan,
            pj_programError: pj_program,
            uploadError: upload,

        })

        if (errorCount !== 0) {
            this.setState({ isDialogOpen: false })
            this.openNotification('error', 'Gagal Membuat Event, pastikan data sudah diisi semua!');
            return false;
        } else {
            return true
        }
    }
    handleChange() {
        this.setState({ isChange: true });
    }
    onChangeValue(e, idNumber) {
        //1  kabupaten
        //2 kecamatan
        //3 kelurahan
        console.log(e.target.value)
        if (e.target.name !== 'upload') {
            this.setState({ [e.target.name]: e.target.value, [e.target.name + 'Error']: false })
        } else {
            if (e.target.files[0]) {
                var filesize = ((e.target.files[0].size / 1024) / 1024).toFixed(4)
            } else {
                return
            }
            if (filesize > 2) {

                this.openNotification('error', 'Ukuran file terlalu besar ! maksimal 2mb ')
                e.target.value = "";
                return
            } else {
                this.setState({ [e.target.name]: e.target.files[0], [e.target.name + 'Error']: false })
            }
        }


        if (idNumber == 1) {
            // this.getKabupaten(e.target.value)
            this.setState({ listKecamatan: [], listKelurahan: [], idKabupaten: "", idKecamatan: "", idKelurahan: "" })
        } else if (idNumber == 2) {
            // this.getKecamatan(e.target.value)
            this.setState({ listKelurahan: [], idKecamatan: "", idKelurahan: "" })
        }
        else if (idNumber == 3) {
            // this.getKelurahan(e.target.value)
            this.setState({ idKelurahan: "" })
        }


    }
    getProvinsi() {
        this.LoadingBar.continuousStart()

        this.setState({ isLoadingOverlay: true })
        api.getProvinsi()
            .then((res) => {
                this.setState({ listProvinsi: res.data.response['0'], isLoadingOverlay: false })
                console.log(res.data.response['0'], 'isi provinsi')
                this.LoadingBar.complete()

            }).catch(err => {
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', err.response ? err.response.data.message : 'Gagal mengambil data!')
                this.LoadingBar.complete()

            })
    }
    getKabupaten(idProvinsi) {
        this.LoadingBar.continuousStart()

        this.setState({ isLoadingOverlay: true })
        api.getKabupaten(idProvinsi)
            .then((res) => {
                this.setState({ listKabupaten: res.data.response['0'], isLoadingOverlay: false })
                this.LoadingBar.complete()

            }).catch(err => {
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', err.response ? err.response.data.message : 'Gagal mengambil data!')
                this.LoadingBar.complete()

            })
    }
    getKecamatan(idKabupaten) {
        this.LoadingBar.continuousStart()

        this.setState({ isLoadingOverlay: true })
        api.getKecamatan(idKabupaten)
            .then((res) => {
                this.setState({ listKecamatan: res.data.response['0'], isLoadingOverlay: false })
                this.LoadingBar.complete()

            }).catch(err => {
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', err.response ? err.response.data.message : 'Gagal mengambil data!')
                this.LoadingBar.complete()

            })
    }

    getKelurahan(idKecamatan) {
        this.LoadingBar.continuousStart()

        this.setState({ isLoadingOverlay: true })
        api.getKelurahan(idKecamatan)
            .then((res) => {
                this.setState({ listKelurahan: res.data.response['0'], isLoadingOverlay: false })
                this.LoadingBar.complete()

            }).catch(err => {
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', err.response ? err.response.data.message : 'Gagal mengambil data!')
                this.LoadingBar.complete()

            })
    }
    getProgramById(id) {
        this.setState({ isLoadingOverlay: true, isHasPicture: true })
        api.getProgramById(id)
            .then(async (res) => {
                console.log(res, 'isires')
                var data = res.data.response.arrProgram;
                // await this.getKabupaten(data.province_id);
                // await this.getKecamatan(data.district_id);
                // await this.getKelurahan(data.subdistrict_id);

                this.setState({

                    isLoadingOverlay: false,
                    id_program: data.id_program,
                    name_program: data.name_program,
                    programLevel: data.id_level_program,
                    from_date: data.from_date,
                    to_date: data.to_date,
                    idProvinsi: data.province_id,
                    idKabupaten: data.district_id,
                    idKecamatan: data.subdistrict_id,
                    idKelurahan: data.village_id,
                    photo: data.photo,
                    description_program: data.description_program,
                    pj_program: data.program_pj,
                    upload: ''


                })
            }).catch(err => {
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', err.response ? err.response.data.message : 'Gagal mengambil data!')
            })
    }
    showEditForm(id) {
        this.handleClickOpen();
        this.setState({ isEdit: true })
        this.getProgramById(id);

        this.setState(
            {
                name_programError: false,
                description_programError: false,
                programLevelError: false,
                from_dateError: false,
                to_dateError: false,
                idProvinsiError: false,
                idKabupatenError: false,
                idKecamatanError: false,
                idKelurahanError: false,
                pj_programError: false,
                uploadError: false,
            })

    }
    checkDateInRange(a, b) {
        var today = new Date();
        var newA = new Date(a);
        var newB = new Date(b);
        if ((newA <= today) && (today <= newB)) {
            return true
        } else {
            return false
        }
    }

    doSave() {
        var isAllowed = this.handleValidation();
        if (!isAllowed) {
            return
        }
        this.LoadingBar.continuousStart()
        this.setState({ isLoadingDetail: true });

        var data = {
            name_program: this.state.name_program,
            programLevel: this.state.programLevel,
            description_program: this.state.description_program,
            pj_program: this.state.pj_program,
            idProvinsi: this.state.idProvinsi,
            idKabupaten: this.state.idKabupaten,
            idKecamatan: this.state.idKecamatan,
            idKelurahan: this.state.idKelurahan,
            from_date: this.state.from_date,
            to_date: this.state.to_date,
            upload: this.state.upload



        }
        this.setState({ isLoadingOverlay: true });
        api.createProgram(data)
            .then((res) => {
                this.LoadingBar.complete()

                this.setState({ isLoadingOverlay: false, isOpen: false, isDialogOpen: false });
                console.log(res, 'isi res');
                this.openNotification('success', 'Berhasil Membuat Program');
                this.getList();
            }).catch(err => {
                this.LoadingBar.complete()

                // this.setState({ isLoading: false });
                this.setState({ isLoadingOverlay: false, isDialogOpen: false })
                console.log(err.response, 'isi error')
                this.openNotification('error', 'Gagal Membuat Program, pastikan data sudah diisi semua!')


            })
    }
    doEdit() {
        var isAllowed = this.handleValidation(true);
        if (!isAllowed) {
            return
        }
        this.LoadingBar.complete()

        var data = {
            id_program: this.state.id_program,
            name_program: this.state.name_program,
            programLevel: this.state.programLevel,
            description_program: this.state.description_program,
            pj_program: this.state.pj_program,
            idProvinsi: this.state.idProvinsi,
            idKabupaten: this.state.idKabupaten,
            idKecamatan: this.state.idKecamatan,
            idKelurahan: this.state.idKelurahan,
            from_date: this.state.from_date,
            to_date: this.state.to_date,
            upload: this.state.upload



        }
        this.setState({ isLoadingOverlay: true });
        api.editProgram(data)
            .then((res) => {
                this.LoadingBar.complete()

                this.setState({ isLoadingOverlay: false, isOpen: false, isEdit: false, isDialogOpen: false });
                this.openNotification('success', 'Berhasil Mengubah Program');
                this.getList();
            }).catch(err => {
                this.LoadingBar.complete()

                // this.setState({ isLoading: false });
                this.setState({ isLoadingOverlay: false, isDialogOpen: false })
                this.openNotification('error', 'Gagal Mengubah Program, pastikan data sudah diisi semua!')


            })
    }
    doDelete(id) {
        this.LoadingBar.continuousStart()

        this.setState({ isLoadingOverlay: true });
        api.deleteProgram(id)
            .then((res) => {
                this.LoadingBar.complete()

                this.setState({ isLoadingOverlay: false, isDialogOpen: false });

                console.log(res, 'isi res');
                this.openNotification('success', 'Berhasil Menghapus Data');
                this.getList();
            }).catch(err => {
                this.LoadingBar.complete()

                // err.response
                this.setState({ isLoadingOverlay: false })
                this.openNotification('error', 'Gagal Menghapus Program !')
            })
    }
    dialogStateChecker(state, id) {
        if (state == 'create') {
            this.doSave()
        }
        else if (state == 'edit') {
            this.doEdit()
        }
        else if (state == 'delete') {
            this.doDelete(id)
        }
    }
    renderDialog() {
        return (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    // onClose={this.closeDialog}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{this.state.dialogText}</DialogTitle>

                    <DialogActions>
                        <Button onClick={this.closeDialog} color="primary">
                            Tidak </Button>
                        <Button onClick={() => this.dialogStateChecker(this.state.dialogState, this.state.dialogId)} color="primary" autoFocus>
                            Ya</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }

    renderTable() {
        return (
            <GridContainer>
                {
                    this.state.dataList.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((data, index) => {
                        return (
                            <Grid xs={12} sm={12} md={12} className={styleCss.dataList}>
                                <Card style={{ cursor: 'pointer' }}>
                                    <CardHeader color="success" style={{ textAlign: 'center', fontWeight: '500' }} onClick={() => this.goDetail(data.id_program)}>
                                        <h4 >{data.name_program}</h4><div style={{ position: 'absolute', right: 10, top: 5 }}>{index + 1 + this.state.page * 10}
                                        </div>
                                    </CardHeader>
                                    <CardBody onClick={() => this.goDetail(data.id_program)}>
                                        <GridContainer>
                                            <GridItem md={3} xs={12} style={{ textAlign: 'center', margin: 'auto' }}>
                                                <img src={data.photo ? data.photo : noImage} style={{ maxWidth: '100%', height: '200px', itemAlign: 'center' }} />
                                            </GridItem>
                                            <GridItem md={9} xs={12}>

                                                <p style={{
                                                    overflow: 'hidden',
                                                    textOverflow: 'ellipsis',
                                                    height: 120,
                                                    textAlign: 'justify'
                                                }} >
                                                    {parse(data.description)}

                                                </p>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td>Penanggung Jawab</td>
                                                            <td>: {data.program_pj} </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Durasi Program</td>
                                                            <td>: {this.formatDate(data.from_date)} sampai {this.formatDate(data.to_date)}


                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> {this.checkDateInRange(data.from_date, data.to_date) ? <Chip

                                                                label="Sedang Berjalan"
                                                                clickable
                                                                color="primary"
                                                                size="small"

                                                            /> : ''}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </GridItem>
                                        </GridContainer>
                                    </CardBody>
                                    <CardFooter chart style={{ cursor: 'auto' }}>
                                        <div></div>
                                        <div style={{ float: 'right !important' }}>
                                            <Tooltip title="Edit">
                                                <IconButton aria-label="delete" onClick={() => this.showEditForm(data.id_program)} size="medium">
                                                    <EditIcon fontSize="inherit" />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Hapus">
                                                <IconButton aria-label="delete" onClick={() => this.openDialog('delete', data.id_program)} size="medium">
                                                    <DeleteIcon fontSize="inherit" />
                                                </IconButton>
                                            </Tooltip>

                                        </div>



                                    </CardFooter>
                                </Card>

                            </Grid>
                        )

                    })
                }
            </GridContainer>
        )
    }

    render() {
        return (
            <div>
                <LoadingOverlay
                    active={this.state.isLoadingOverlay}
                    spinner
                    text='mengambil data mohon tunggu...'
                    style={{ display: 'none' }}
                >
                </LoadingOverlay>
                <LoadingBar
                    height={3}
                    color='#FFFFFF'
                    onRef={ref => (this.LoadingBar = ref)}
                />
                <Snackbar
                    place="tc"
                    color={this.state.alertState}
                    icon={this.state.alertState == 'success' ? CheckIcon : ErrorIcon}
                    message={this.state.AlertMessage}
                    open={this.state.isAlertOpen}
                    closeNotification={() => this.closeNotification}
                    close
                />
                <Card>
                    <Dialog className={styleCss.modal}

                        maxWidth={'md'}
                        fullWidth={true}
                        open={this.state.isOpen}
                        // onClose={this.handleClose}
                        scroll={'paper'}
                        aria-labelledby="scroll-dialog-title"
                        aria-describedby="scroll-dialog-description"
                    >
                        <DialogTitle id="scroll-dialog-title" >
                            {this.state.isEdit ? 'Edit Program' : 'Buat Program '}
                            <img src='https://img.icons8.com/metro/26/000000/close-window.png' onClick={this.handleClose} className={styleCss.closeWindows} />

                        </DialogTitle>
                        <DialogContent dividers={true} >
                            <DialogContentText className={styleCss.dialogConText}
                                id="scroll-dialog-description"
                                // ref={'modal1'}
                                tabIndex={-1}

                            >
                                <form onSubmit={() => alert('aa')}>
                                    <input name="_method" type="hidden" value="PATCH" />
                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom} >
                                            <GridItem md={3} xs={12}>
                                                <label>Nama Program</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <Input error={this.state.name_programError} placeholder="Nama Program" required name="name_program" onChange={(e) => this.onChangeValue(e)} value={this.state.name_program} inputProps={{ 'aria-label': 'description' }} className={styleCss.widthInput} />
                                                {
                                                    this.state.name_programError ? <FormHelperText className={styleCss.helperText} >Field ini tidak boleh kosong.</FormHelperText> : ''
                                                }
                                            </GridItem>
                                        </GridContainer>
                                    </div>
                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Deskripsi</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <SunEditor name="summary" onChange={(e) => this.setState({ description_program: e })} setContents={this.state.description_program} />
                                                {
                                                    this.state.description_programError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                }
                                            </GridItem>
                                        </GridContainer>
                                    </div>

                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Level Program</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <div sclassName={styleCss.widthInput}   >
                                                    <Select
                                                        className={styleCss.selectEmpty}
                                                        error={this.state.programLevelError}
                                                        onChange={(e) => this.onChangeValue(e)}
                                                        value={this.state.programLevel}
                                                        name="programLevel"
                                                    >
                                                        <MenuItem value="" disabled> Level Program</MenuItem>
                                                        {this.state.levelList.map((data) => {
                                                            return (
                                                                <MenuItem value={data.id} > {data.name}</MenuItem>
                                                            )
                                                        })}
                                                    </Select>
                                                    {
                                                        this.state.programLevelError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                    }
                                                </div>

                                            </GridItem>
                                        </GridContainer>
                                    </div>
                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Penanggung Jawab</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <Input error={this.state.pj_programError} placeholder="Penanggung Jawab" name="pj_program" onChange={(e) => this.onChangeValue(e)} value={this.state.pj_program} inputProps={{ 'aria-label': 'description' }} sclassName={styleCss.widthInput} />
                                                {
                                                    this.state.pj_programError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                }
                                            </GridItem>
                                        </GridContainer>
                                    </div>

                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Durasi Program</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                <form className={styleCss.containerDate} noValidate>
                                                    <div>
                                                        <TextField
                                                            name="from_date"
                                                            error={this.state.from_dateError}
                                                            value={this.state.from_date}
                                                            onChange={(e) => this.onChangeValue(e)}
                                                            format="dd/mm/yyyy"
                                                            type="date"
                                                            // defaultValue="2017-00-00"
                                                            className={styleCss.textFieldDate}
                                                            InputLabelProps={{
                                                                shrink: true,

                                                            }}

                                                        />
                                                        {
                                                            this.state.from_dateError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                        }
                                                    </div>
                                                    <p style={{ margin: "0px 10px" }}>  s/d</p>
                                                    <div>
                                                        <TextField
                                                            name="to_date"
                                                            error={this.state.to_dateError}
                                                            value={this.state.to_date}
                                                            onChange={(e) => this.onChangeValue(e)}
                                                            format="dd/mm/yyyy"
                                                            type="date"
                                                            // defaultValue="2017-00-00"
                                                            className={styleCss.textFieldDate}
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                        />
                                                        {
                                                            this.state.to_dateError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                        }
                                                    </div>
                                                </form>

                                            </GridItem>
                                        </GridContainer>
                                    </div>
                                  

                                    <div className={styleCss.marginInput}>
                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>
                                                <label>Logo</label>
                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                {
                                                    this.state.isHasPicture ? <Button variant="contained" component="span" className={styleCss.buttonUpload} onClick={() => this.setState({ isHasPicture: false })} > Change Picture </Button> :
                                                        <div>
                                                            <input
                                                                accept="image/*"

                                                                onChange={(e) => this.onChangeValue(e)}
                                                                name="upload"
                                                                id="contained-button-file"
                                                                type="file"
                                                            />

                                                            {
                                                                this.state.uploadError ? <FormHelperText className={styleCss.helperText}>Field ini tidak boleh kosong.</FormHelperText> : ''
                                                            }
                                                            <FormHelperText >*Format yang diperbolehkan adalah jpeg, jpg , png dan ukuran maksimal 2mb</FormHelperText>
                                                            {
                                                                this.state.isEdit ? <a style={{ cursor: 'pointer' }} onClick={() => this.setState({ isHasPicture: true })} > Batal </a> : ''
                                                            }





                                                        </div>
                                                }



                                            </GridItem>


                                        </GridContainer>

                                        <GridContainer className={styleCss.gridcontainerBotom}>
                                            <GridItem md={3} xs={12}>

                                            </GridItem>
                                            <GridItem md={9} xs={12}>
                                                {
                                                    this.state.isHasPicture ?
                                                        <img style={{ maxWidth: '50%', marginTop: 20 }} src={this.state.photo}></img>
                                                        :
                                                        <div>
                                                            {
                                                                this.state.upload ? <img style={{ maxWidth: '50%', marginTop: 20 }} src={URL.createObjectURL(this.state.upload)}></img> : ''
                                                            }
                                                            <div><label>{this.state.upload ? this.state.upload.name : ''}</label></div>
                                                        </div>
                                                }




                                            </GridItem>




                                        </GridContainer>



                                    </div>
                                    <div style={{ marginBottom: "20px", marginTop: "10px", float: "right" }}>

                                        <Button variant="contained" component="span" onClick={this.handleClose} className={styleCss.buttonClose}>Kembali</Button>

                                        {
                                            this.state.isEdit ? <Button variant="contained" component="span" onClick={() => this.openDialog('edit')} className={styleCss.buttonConfirm}>Perbaharui</Button> :
                                                <Button variant="contained" component="span" onClick={() => this.openDialog('create')} className={styleCss.buttonConfirm}>Simpan</Button>
                                        }

                                    </div>

                                </form>
                            </DialogContentText>
                        </DialogContent>

                    </Dialog>
                </Card>
                {this.renderDialog()}

                <Card>
                    <CardHeader color="success">
                        <h4 >Informasi Program</h4>
                    </CardHeader>
                    <CardBody>

                        <div>
                            <Button onClick={this.handleClickOpen} variant="contained" component="span" className={styleCss.buttonConfirm}>Buat Program</Button>

                        </div>




                        <div>
                            <Paper component="form" className={styleCss.paperSearch}>
                                <IconButton type="submit" className={styleCss.iconButton} aria-label="search">
                                    <SearchIcon />
                                </IconButton>
                                <InputBase
                                    className={styleCss.inputSearch}
                                    placeholder="Cari Program..."
                                    inputProps={{ 'aria-label': 'Cari Program...' }}
                                    onChange={(e) => this.doSearch(e)}
                                />



                            </Paper>

                            {/* <input type="text" name="search" onChange={(e) => this.doSearch(e)} placeholder="Cari Program.." className={styleCss.searchBox} /> */}



                        </div>

                        {/* <GridContainer >
                            <GridItem xs={12} md={3} >
                                <FormControl sclassName={styleCss.widthInput }   >
                                    <Select
                                        className={styleCss.selectEmpty}
                                    // onChange={(e) => this.onChangeValue(e, 2)}
                                    // value={this.state.id_district}
                                    // name=""                          
                                    >
                                        <MenuItem value="" disabled>
                                            Pilih Jenis lemabaga </MenuItem>
                                        <MenuItem value=""> Lembaga A</MenuItem>
                                        <MenuItem value=""> Lembaga B</MenuItem>
                                        <MenuItem value=""> Lembaga C</MenuItem>
                                    </Select>
                                </FormControl>

                            </GridItem>

                            <GridItem xs={12} md={3} >
                                <FormControl sclassName={styleCss.widthInput }   >
                                    <Select
                                        className={styleCss.selectEmpty}
                                    // onChange={(e) => this.onChangeValue(e, 2)}
                                    // value={this.state.id_district}
                                    // name=""                          
                                    >
                                        <MenuItem value="" disabled>
                                            Pilih Nilai </MenuItem>
                                        <MenuItem value=""> Nilai A</MenuItem>
                                        <MenuItem value=""> Nilai B</MenuItem>
                                        <MenuItem value=""> Nilai C</MenuItem>
                                    </Select>
                                </FormControl>

                            </GridItem>

                            <GridItem xs={12} md={3} >
                                <FormControl sclassName={styleCss.widthInput }   >
                                    <Select
                                        className={styleCss.selectEmpty}
                                    // onChange={(e) => this.onChangeValue(e, 2)}
                                    // value={this.state.id_district}
                                    // name=""                          
                                    >
                                        <MenuItem value="" disabled>
                                            Pilih Tahun </MenuItem>
                                        <MenuItem value=""> Tahun A</MenuItem>
                                        <MenuItem value=""> Tahun B</MenuItem>
                                        <MenuItem value="">Tahun C</MenuItem>
                                    </Select>
                                </FormControl>

                            </GridItem>

                            <GridItem xs={12} md={3} >
                                <Button style={{ backgroundColor: "#054737", color: "white", fontWeight: 'bold', marginTop: '0px', height: '36px', width: '100%' }}>Filter</Button>

                            </GridItem>

                        </GridContainer> */}






                    </CardBody>

                    <CardBody>
                        {this.state.dataList.length == 0 && this.state.isLoading == false ?
                            <Card style={{ cursor: 'pointer' }}>
                                <CardHeader color="success" style={{ textAlign: 'center', fontWeight: '500' }}>
                                    <h4 >Data tidak ada</h4>
                                </CardHeader>

                            </Card> : ''}
                        {this.state.isLoading ? <div style={{ textAlign: "center" }}><CircularProgress /></div> : this.renderTable()}

                    </CardBody>
                    <div style={{ float: 'right', width: '100%' }}>
                        <TablePagination
                            rowsPerPageOptions={[10]}

                            count={this.state.dataList.length}
                            rowsPerPage={this.state.rowsPerPage}
                            page={this.state.page}
                            SelectProps={{
                                inputProps: { 'aria-label': 'Jumlah Per halaman' },
                                native: false,
                            }}
                            onChangePage={this.handleChangePage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                        />
                    </div>
                </Card>


            </div>
        )
    }

}
export default InformasiProgram