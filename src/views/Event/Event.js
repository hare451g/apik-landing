import React, { Component } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Chart from 'react-apexcharts'
// import avatar from "assets/img/faces/marc.jpg";
import ppt from '../../assets/img/ppt.png'
import TablePagination from '@material-ui/core/TablePagination';
import fileppt from'../../assets/ppt/Presentation1.pptx'

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: '"Arial"',
    marginBottom: "3px",
    textDecoration: "none"
  }
};


class Event extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: 5,
      isDetail: false,
      dataGender: {
        options: {
          labels: ['Laki-laki', 'Perempuan'],

        },
        series: [50, 50]
      },
      dataDaerah: {
        options: {
          labels: ['Daerah', 'Kehutanan', 'Instansi Pemerintah', 'Instansi Swasta']
    
        },
        series: [35, 20, 45, 20]
      }
    }
  }



  render() {

    return (
      <div>
        {!this.state.isDetail ?
          <div>
            <Card>
              <CardHeader color="success">
                <h4 >List Event</h4>
              </CardHeader>
              <CardBody>
                <GridContainer>

                  <GridItem xs={12} sm={12} md={4} onClick={() => this.setState({ isDetail: true })}>
                    <Card style={{ cursor: 'pointer' }}>
                      <CardHeader color="success" style={{ textAlign: 'center', fontWeight: '500' }}>
                        <h4 >Event Kehutanan 1</h4>
                      </CardHeader>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4} onClick={() => this.setState({ isDetail: true })}>
                    <Card style={{ cursor: 'pointer' }}>
                      <CardHeader color="success" style={{ textAlign: 'center', fontWeight: '500' }}>
                        <h4 >Event Kehutanan 2</h4>
                      </CardHeader>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4} onClick={() => this.setState({ isDetail: true })}>
                    <Card style={{ cursor: 'pointer' }}>
                      <CardHeader color="success" style={{ textAlign: 'center', fontWeight: '500' }}>
                        <h4 >Event Kehutanan 3</h4>
                      </CardHeader>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4} onClick={() => this.setState({ isDetail: true })}>
                    <Card style={{ cursor: 'pointer' }}>
                      <CardHeader color="success" style={{ textAlign: 'center', fontWeight: '500' }}>
                        <h4 >Event Kehutanan 4</h4>
                      </CardHeader>
                    </Card>
                  </GridItem>

                </GridContainer>
              </CardBody>
            </Card>
            <div style={{ float: 'right' }}>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                colSpan={3}
                count={4}
                rowsPerPage={10}
                page={0}
                SelectProps={{
                  inputProps: { 'aria-label': 'Jumlah Per halaman' },
                  native: false,
                }}

              />
            </div>
          </div>
          :
          <div>
            <Button color="success" onClick={() => this.setState({ isDetail: false })}>  Kembali</Button>
            <div>
              <GridContainer>
                <GridItem xs={12} sm={12} >
                  <Card>
                    <CardHeader color="success">
                      <h4 >Event Detail</h4>
                      {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
                    </CardHeader>
                    <CardBody>

                      <GridContainer>
                        <GridItem xs={12} sm={12} >
                          <CustomInput
                            labelText="Jenis Event"
                            id="jenisEvent"
                            formControlProps={{
                              fullWidth: true
                            }}
                            inputProps={{
                              value: 'Rapat'
                            }}
                          />
                        </GridItem>
                        <GridItem xs={12} sm={12} >
                          <CustomInput
                            labelText="Kode"
                            id="kode"
                            formControlProps={{
                              fullWidth: true
                            }}
                            inputProps={{
                              value: '1111292923998AGB'
                            }}
                          />
                        </GridItem>
                      </GridContainer>


                  
                    </CardBody>
                   
                  </Card>
                </GridItem>
              
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} >
                  <Card>
                    <CardHeader color="success">
                      <h4 >Kehadiran Peserta</h4>
                      {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
                    </CardHeader>
                    <CardBody>

                      <GridContainer>
                        <GridItem xs={12} sm={12} >
                          <Card chart>
                            <CardHeader color="success">
                              <Chart options={this.state.dataGender.options} series={this.state.dataGender.series} type="pie" height="300" />
                            </CardHeader>
                            <CardBody>
                              <h4 >Kehadiran berdasarkan Jenis Kelamin Peserta</h4>

                            </CardBody>

                          </Card>

                        </GridItem>
                        <GridItem xs={12} sm={12} >
                          <Card chart>
                            <CardHeader color="success">
                              <Chart options={this.state.dataDaerah.options} series={this.state.dataDaerah.series} type="pie" height="300" />
                            </CardHeader>
                            <CardBody>
                              <h4 >Kehadiran Berdasarkan Daerah</h4>

                            </CardBody>

                          </Card>

                        </GridItem>
                      </GridContainer>



                    </CardBody>

                  </Card>
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} >
                  <Card >
                    <CardHeader color="success">
                      <h4 >Presentasi</h4>
                      {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
                    </CardHeader>
                    <CardBody>
                            
                      <GridContainer >
                        <GridItem xs={12} sm={3} styles={{cursor:'pointer'}}>
                        <a href={fileppt} target="_blank"> 
                          <Card chart>
                            <CardHeader >
                              <img src={ppt} style={{ maxWidth: '100%', textAlign: 'center' }} />
                            </CardHeader>
                            <CardBody>
                              <h4 >Budidaya hutan PPT</h4>

                            </CardBody>

                          </Card>
                          </a>
                        </GridItem>
                        <GridItem xs={12} sm={3} styles={{cursor:'pointer'}}>
                        <a href={fileppt} target="_blank"> 
                          <Card chart>
                            <CardHeader >
                              <img src={ppt} style={{ maxWidth: '100%', textAlign: 'center' }} />
                            </CardHeader>
                            <CardBody>
                              <h4 >Budidaya hutan PPT</h4>

                            </CardBody>

                          </Card>
                          </a>
                        </GridItem>
                        <GridItem xs={12} sm={3} styles={{cursor:'pointer'}}>
                        <a href={fileppt} target="_blank"> 
                          <Card chart>
                            <CardHeader >
                              <img src={ppt} style={{ maxWidth: '100%', textAlign: 'center' }} />
                            </CardHeader>
                            <CardBody>
                              <h4 >Budidaya hutan PPT</h4>

                            </CardBody>

                          </Card>
                          </a>
                        </GridItem>
                        <GridItem xs={12} sm={3} styles={{cursor:'pointer'}}>
                        <a href={fileppt} target="_blank"> 
                          <Card chart>
                            <CardHeader >
                              <img src={ppt} style={{ maxWidth: '100%', textAlign: 'center' }} />
                            </CardHeader>
                            <CardBody>
                              <h4 >Budidaya hutan PPT</h4>

                            </CardBody>

                          </Card>
                          </a>
                        </GridItem>
                        

                      </GridContainer>



                    </CardBody>

                  </Card>
                </GridItem>
              </GridContainer>

            </div >
          </div>
        }
      </div>
    );
  }
}
export default Event;