import React from "react";

// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";

import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';


// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";



// Image


import { bugs, website, server } from "variables/general.js";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";


import styles from "assets/jss/material-dashboard-react/views/registerStyle.js";
import styleCss from "../../../src/assets/css/views/global.module.css"

import { Typography } from "@material-ui/core";

const useStyles = makeStyles(styles);




export default function Register() {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
  });

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  };
  

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };



  return (
    <div >
      <Card className={styleCss.registerBody}>
        <CardHeader color="success">
          <h4 className={classes.cardTitleWhite}>REGISTER</h4>
        </CardHeader>

        <CardBody>
          <div className={styleCss.cBodyRegister} >
            <GridContainer style={{ marginBottom: "30px !important" }}>
              <GridItem md={3} xs={12}>
                <label>Nama</label>
              </GridItem>
              <GridItem md={9} xs={12}>
                <Input placeholder="Name"  inputProps={{ 'aria-label': 'description' }} style={{ width: "100%" }} />

              </GridItem>
            </GridContainer>
          </div>

          <div className={styleCss.cBodyRegisterBot} >
            <GridContainer>
              <GridItem md={3} xs={12}>
                <label>Alamat</label>
              </GridItem>
              <GridItem md={9} xs={12}>
                <Input placeholder="Address"  inputProps={{ 'aria-label': 'description' }} style={{ width: "100%" }} />

              </GridItem>
            </GridContainer>
          </div>
          <div className={styleCss.cBodyRegisterBot}>
            <GridContainer>
              <GridItem md={3} xs={12}>
                <label>No. Handphone</label>
              </GridItem>
              <GridItem md={9} xs={12}>
                <Input placeholder="No. Handphone" inputProps={{ 'aria-label': 'description' }} style={{ width: "100%" }} />

              </GridItem>
            </GridContainer>
          </div>

          <div className={styleCss.cBodyRegisterBot}>
            <GridContainer>
              <GridItem md={3} xs={12}>
                <label>Instansi</label>
              </GridItem>
              <GridItem md={9} xs={12}>
                <Input placeholder="Instansi" inputProps={{ 'aria-label': 'description' }} style={{ width: "100%" }} />

              </GridItem>
            </GridContainer>
          </div>
          <div className={styleCss.cBodyRegisterBot}>
            <GridContainer>
              <GridItem md={3} xs={12}>
                <label>Email</label>
              </GridItem>
              <GridItem md={9} xs={12}>
                <Input placeholder="Email" inputProps={{ 'aria-label': 'description' }} style={{ width: "100%" }} />

              </GridItem>
            </GridContainer>
          </div>

          <div className={styleCss.cBodyRegisterBot}>
            <GridContainer>
              <GridItem md={3} xs={12}>
                <label>Password</label>
              </GridItem>
              <GridItem md={9} xs={12}>
              
          <Input
            id="standard-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            placeholder="Password"
            style={{ width: "100%" }} 
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />

              </GridItem>
            </GridContainer>
          </div>

          <div className={styleCss.cBodyRegisterBot}>
            <GridContainer>
              <GridItem md={3} xs={12}>
                <label>Konfirmasi Password</label>
              </GridItem>
              <GridItem md={9} xs={12}>
              
          <Input
            id="standard-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            placeholder="Konfirmasi Password"
            style={{ width: "100%" }} 
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />

              </GridItem>
            </GridContainer>
          </div>

          <Button variant="contained"  href="#contained-buttons" className={styleCss.btnDaftar} >
        DAFTAR
      </Button>
      <Button variant="contained"  href="#contained-buttons" className={styleCss.btnKembali} >
        Kembali
      </Button>
          
        </CardBody>
      </Card>





    </div>
  );
}
