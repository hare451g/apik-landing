import React, { Component, useEffect, useState } from "react";
import api from 'config/api';
import gf from 'config/globalFunc';
import { Radar, Chart } from 'react-chartjs-2';
import { useHistory } from "react-router-dom";

// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';

// @material-ui/icons
import CreateIcon from '@material-ui/icons/Create';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import CloseIcon from '@material-ui/icons/Close';
import PrintIcon from '@material-ui/icons/Print';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Skeleton from '@material-ui/lab/Skeleton';
import { faCity, faLandmark, faArchway, faBuilding, faLayerGroup, faExclamationCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';


// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { TreeItem } from '@material-ui/lab';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Navbar from "components/Navbars/Navbar.js";


import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

// Image
import diagonal1 from '../../assets/img/chart1.PNG';
import diagonal2 from '../../assets/img/chart2.PNG';
import diagonal3 from '../../assets/img/chart3.PNG';


import styles from "assets/jss/material-dashboard-react/views/registerStyle.js";
import styleCss from "../../../src/assets/css/views/global.module.css"

import { Typography } from "@material-ui/core";

const useStyles = makeStyles(styles);
Chart.defaults.global.legend.display = false;



function handleClick(event) {
    event.preventDefault();
    console.info('You clicked a breadcrumb.');
}

const datas = {
    labels: ['SU O', 'SU T', 'SU M', 'CH O', 'CH T', 'CH M'],
    datasets: [
        {
            data: [1, 5, 3, 0, 0, 0],
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            pointBackgroundColor: 'rgba(255,99,132,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(255,99,132,1)',
        },
        {
            data: [0, 0, 0, 2, 5, 6],

            backgroundColor: 'rgba(0,166,166,0.2)',
            borderColor: 'rgba(0,166,166,1)',
            pointBackgroundColor: 'rgba(0,166,166,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(0,166,166,1)',
        }
    ]
}

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function AdminDaerahProfile() {
    const classes = useStyles();
    const history = useHistory();
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });
    const [open, setOpen] = React.useState(true);
    const [openDialog, setOpenDialog] = React.useState(false);
    const [dataActivity, setDataActivity] = React.useState([]);
    const [dataUser, setDataUser] = React.useState({});
    const [failDU, setFailDU] = React.useState(false);
    const [failActivity, setFailActivity] = React.useState(false);
    const [failBahaya, setFailBahaya] = React.useState(false);
    const [loadUser, setLoadUser] = React.useState(true);
    const [loadActivity, setLoadActivity] = React.useState(true);
    const [loadBahaya, setLoadBahaya] = React.useState(true);
    const [loadAdaptasi, setLoadAdaptasi] = React.useState(true);
    const [loadValidasi, setLoadValidasi] = React.useState(true);
    const [loadKerentanan, setLoadKerentanan] = React.useState(true);
    const [dataKerentanan, setDataKerentanan] = React.useState([]);
    const [dataBahaya, setDataBahaya] = React.useState([]);
    const [dataValidasi, setDataValidasi] = React.useState([]);
    const [dataAdaptasi, setDataAdaptasi] = React.useState([]);
    const [head, setHead] = React.useState([]);
    const [body, setbody] = React.useState([]);
    const [bodyLength, setBodyLength] = React.useState([]);

    const handleClickOpen = () => {
        setOpenDialog(true);
    };
    const handleClose = () => {
        setOpenDialog(false);
    };
    const handleClick = () => {
        setOpen(!open);
    };
    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };
    const convertDate = (newData, oldName, name) => {
        return newData.map(item => {
            var temp = Object.assign({}, item);
            let tempDate = temp.created_date.split(' ')
            temp.created_date = tempDate[0]

            return temp;
        })
    }
    const getActivity = (id) => {

        setLoadActivity(true)
        api.getActivityById(id)
            .then((res) => {

                if (res.data.error_code == 0) {
                    let dataa = res.data.data;
                    dataa.reverse();
                    dataa = convertDate(dataa)

                    dataa = dataa.length == 0 ? [] : gf.groupBy(dataa, (c) => c.created_date)
                    dataa = Object.entries(dataa)
                    console.log(dataa);
                    setDataActivity(dataa)
                } else {

                }
                setLoadActivity(false)
            })
            .catch((err) => {
                setLoadActivity(false)
                console.log("Errorr:", err);
            })
    }

    const getKerentananParamUser = (id) => {
        setLoadKerentanan(true)
        api.getDataCollection(id)
            .then((res) => {

                if (res.data.error_code == 0) {
                    setDataKerentanan(res.data.data)
                    setLoadKerentanan(false)
                } else {
                    setLoadKerentanan(false)
                }

            })
            .catch((err) => {
                setLoadKerentanan(false)
                console.log("Errorr:", err);
            })
    }
    const getValidasi = (id) => {
        setLoadValidasi(true)
        api.getValidasiProfile(id)
            .then((res) => {

                if (res.data.error_code == 0) {
                    let dk = res.data.data;
                    setDataValidasi(dk)
                } else {

                }
                setLoadValidasi(false)

            })
            .catch((err) => {
                setLoadValidasi(false)
                console.log("Errorr:", err);
            })
    }
    const getAdaptasi = (id) => {
        setLoadAdaptasi(true)
        api.getAdaptasiProfile(id)
            .then((res) => {

                if (res.data.error_code == 0) {
                    let dk = res.data.data;
                    let dHead = []
                    let dBody = []
                    let lengthh = []
                    for (let i = 0; i < dk.length; i++) {
                        dHead.push(dk[i].wilayah__name)
                        let tempNumb = 0
                        for (let j = 0; j < dk[i].details.length; j++) {

                            if (dBody[i] == undefined) {
                                dBody.push([Object.keys(dk[i].details[j]).length !== 0 ? dk[i].details[j].adaptation : ''])
                            } else {
                                dBody[i].push(Object.keys(dk[i].details[j]).length !== 0 ? dk[i].details[j].adaptation : '')
                            }
                            tempNumb++
                            if (lengthh[j] == undefined) {
                                lengthh.push(1)
                            }
                        }
                    }
                    for (let i = 0; i < dBody.length; i++) {
                        for (let j = 0; j < lengthh.length; j++) {
                            if (dBody[i][j] == undefined) {
                                dBody[i].push('-')
                            }
                        }
                    }
                    setBodyLength(lengthh)
                    setbody(dBody)
                    setHead(dHead)
                    // alert(dataTabel.dHead)
                    setDataAdaptasi(dk)
                } else {

                }
                setLoadAdaptasi(false)

            })
            .catch((err) => {
                setLoadAdaptasi(false)
                console.log("Errorr:", err);
            })
    }

    const getBahayaByDataVisual = (id) => {
        setLoadBahaya(true)
        api.getBahayaByDataVisual(id)
            .then((res) => {
                if (res.data.error_code == 0) {
                    let dk = res.data.data;

                    for (let i = 0; i < dk.length; i++) {
                        dk[i].datas = {
                            labels: ['SU O', 'SU T', 'SU M', 'CH O', 'CH T', 'CH M'],
                            datasets: [
                                {
                                    data: [dk[i].su_bulanan, dk[i].su_tahunan, dk[i].su_optimum, 0, 0, 0],
                                    backgroundColor: 'rgba(0,166,166,0.5)',
                                    borderColor: 'rgba(0,166,166,1)',
                                    pointBackgroundColor: 'rgba(0,166,166,1)',
                                    pointBorderColor: '#fff',
                                    pointHoverBackgroundColor: '#fff',
                                    pointHoverBorderColor: 'rgba(0,166,166,1)',
                                    label: 'SU'
                                },
                                {
                                    data: [0, 0, 0, dk[i].ch_bulanan, dk[i].ch_tahunan, dk[i].ch_optimum],
                                    backgroundColor: 'rgba(255,99,132,0.5)',
                                    borderColor: 'rgba(255,99,132,1)',
                                    pointBackgroundColor: 'rgba(255,99,132,1)',
                                    pointBorderColor: '#fff',
                                    pointHoverBackgroundColor: '#fff',
                                    pointHoverBorderColor: 'rgba(255,99,132,1)',
                                    label: 'CH'
                                }
                            ]
                        }
                        dk[i].options = {
                            responsive: true,
                            maintainAspectRatio: false
                        }
                    }
                    console.log(dk, 'isi bahaya')

                    setDataBahaya(dk)
                } else {

                }
                setLoadBahaya(false)

            })
            .catch((err) => {
                setLoadBahaya(false)
                console.log("Errorr:", err);
            })
    }
    const getUserById = (id) => {
        setLoadUser(true)
        api.getUserById(id)
            .then((res) => {
                if (res.data.error_code == 0) {
                    let datauser = res.data.data[0]
                    let dt = datauser.data;

                    if (dt !== null) {
                        if (Object.keys(dt.info_geografi).length !== 0) {
                            dt.info_geografi = {
                                bb: dt.info_geografi.bb ? dt.info_geografi.bb : '-',
                                bu: dt.info_geografi.bu ? dt.info_geografi.bu : '-',
                                bs: dt.info_geografi.bs ? dt.info_geografi.bs : '-',
                                bt: dt.info_geografi.bt ? dt.info_geografi.bt : '-',
                            }
                        } else {
                            dt.info_geografi = {
                                bb: '-',
                                bu: '-',
                                bs: '-',
                                bt: '-',
                            }
                        }
                        if (Object.keys(dt.administrasi_wilayah).length !== 0) {
                            dt.administrasi_wilayah = {
                                total_kec: dt.administrasi_wilayah.total_kec ? dt.administrasi_wilayah.total_kec : '-',
                                total_kel: dt.administrasi_wilayah.total_kel ? dt.administrasi_wilayah.total_kel : '-',
                                total_desa: dt.administrasi_wilayah.total_desa ? dt.administrasi_wilayah.total_desa : '-',
                            }
                        } else {
                            dt.administrasi_wilayah = {
                                total_kec: '-',
                                total_kel: '-',
                                total_desa: '-'
                            }
                        }
                        if (Object.keys(dt.penyakit).length == 0) {
                            dt.penyakit = ['-']
                        }
                        datauser.data = dt

                    } else {
                        dt.info_geografi = {
                            bb: '-',
                            bu: '-',
                            bs: '-',
                            bt: '-',
                        }
                        dt.administrasi_wilayah = {
                            total_kec: '-',
                            total_kel: '-',
                            total_desa: '-'
                        }
                        dt.penyakit = ['-']

                        datauser.data = dt

                    }
                    console.log(datauser, 'isi data user')
                    setDataUser(datauser)
                    setLoadUser(false)
                } else {

                    history.push('home')
                }

            })
            .catch((err) => {
                setLoadUser(false)
                console.log("Errorr:", err);
            })
    }
    const downloadFormat = () => {
        let urls = api.getKerentananTemplate();
        window.open(urls)
    }
    const renderProfile = () => {
        return (
            <div>
                <div >
                    <IconButton className={styleCss.ProfilePic}>
                        <Typography className={styleCss.nameProfile}>B </Typography>

                        <IconButton className={styleCss.editProfile} onClick={() => history.push('/profile-edit-admin-daerah')}>
                            <CreateIcon />
                        </IconButton>

                    </IconButton>
                    <Typography className={styleCss.namaAdminDaerah}> {dataUser.name} </Typography>
                    <Typography className={styleCss.namaAdminDaerah}> {dataUser.wilayah__name}</Typography>
                    <Typography className={styleCss.emailAdminDaerah}> {dataUser.email}</Typography>


                </div>
                <TreeView
                    className={styleCss.TreeView}
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    defaultExpanded={['1']}
                    multiSelect
                >
                    <TreeItem className={styleCss.TreeItemColor} nodeId="1" label="Informasi Grafis">
                        <TreeItem className={styleCss.TreeItem} nodeId="2" label={"Batas Utara : " + dataUser.data.info_geografi.bu} />
                        <TreeItem className={styleCss.TreeItem} nodeId="3" label={"Batas Selatan : " + dataUser.data.info_geografi.bs} />
                        <TreeItem className={styleCss.TreeItem} nodeId="4" label={"Batas Barat : " + dataUser.data.info_geografi.bb} />
                        <TreeItem className={styleCss.TreeItem} nodeId="5" label={"Batas Timur : " + dataUser.data.info_geografi.bt} />
                    </TreeItem>

                </TreeView>
                <TreeView
                    className={styleCss.TreeView}
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    defaultExpanded={['1']}
                    multiSelect
                >
                    <TreeItem className={styleCss.TreeItemColor} nodeId="1" label="Adminitrasi Wilayah">
                        <TreeItem className={styleCss.TreeItem} nodeId="2" label={"Jumlah Kecamatan : " + dataUser.data.administrasi_wilayah.total_kec} />
                        <TreeItem className={styleCss.TreeItem} nodeId="3" label={"Jumlah Kelurahan : " + dataUser.data.administrasi_wilayah.total_kel} />
                        <TreeItem className={styleCss.TreeItem} nodeId="4" label={"Jumlah Desa : " + dataUser.data.administrasi_wilayah.total_desa} />
                    </TreeItem>

                </TreeView>
                <TreeView
                    className={styleCss.TreeView}
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    defaultExpanded={['1']}
                    multiSelect
                >
                    <TreeItem className={styleCss.TreeItemColor} nodeId="1" label="Penyakit Prioritas">
                        {
                            dataUser.data.penyakit.map((dp, index) => {
                                return (
                                    <TreeItem nodeId={index + 2} className={styleCss.TreeItem} label={dp} />
                                )
                            })
                        }
                    </TreeItem>

                </TreeView>
            </div>

        )
    }

    useEffect(() => {
        if (!localStorage.getItem('token')) history.push('home')
        getUserById(localStorage.getItem('email'));
        getActivity(localStorage.getItem('userId'));
        getKerentananParamUser(localStorage.getItem('userId'))
        getBahayaByDataVisual(localStorage.getItem('userId'))
        getValidasi(localStorage.getItem('userId'))
        getAdaptasi(localStorage.getItem('userId'))

        //get native Map instance
    }, []);


    return (
        <div className={styleCss.Container}>
            <Navbar
                // routes={routes}
                // handleDrawerToggle={handleDrawerToggle}
                // {...rest}
                width={'100%'}
            />

            <div style={{ marginTop: "90px", marginLeft: "-10px", marginBottom: "-10px" }}>
                <Breadcrumbs separator="›" aria-label="breadcrumb">
                    <Link color="inherit" onClick={() => history.push('home')} className={styleCss.Link}>
                        <ArrowBackIosIcon className={styleCss.IconBreadcrumbs} />
                        Kembali ke peta
      </Link>

                </Breadcrumbs>
            </div>


            <CardBody >
                <GridContainer>
                    <GridItem md={3}>
                        <Card className={styleCss.CardProfileRingkasan}>
                            <div className={styleCss.headerCard}>Profile</div>


                            {
                                loadUser ?
                                    <div style={{ paddingLeft: 20, paddingRight: 20 }}>
                                        <div style={{ textAlign: 'center', padding: 66, paddingBottom: 0 }}>
                                            <Skeleton width={120} height={120} variant="circle" />
                                        </div>
                                        <div style={{ marginTop: 50 }}>
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                        </div>
                                        <div style={{ marginTop: 50 }}>
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                        </div>

                                        <div style={{ marginTop: 50 }}>
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                        </div>


                                    </div> :
                                    renderProfile()
                            }

                        </Card>

                    </GridItem>

                    <GridItem md={9}>
                        <Card className={styleCss.CardProfileRingkasan}>
                            <div className={styleCss.headerCard}>Ringkasan</div>
                            <div>
                                <GridContainer >
                                    <GridItem md={8}>
                                        <div  >
                                            <Typography className={styleCss.JudulIsiRingkasan}>
                                                <ArrowDropDownIcon style={{ color: '#00A6A6' }} />
                                                 Data Kerentanan</Typography>
                                            <div>
                                                <Typography className={styleCss.IsiRingkasan}> Baru Ditambahkan :</Typography>
                                                <GridContainer>
                                                    <GridItem md={9}>
                                                        {
                                                            dataKerentanan.slice(0, 2).map((dk, index) => {
                                                                return (
                                                                    <div className={styleCss.ditambahkanCard}>
                                                                        <span><InsertDriveFileIcon className={styleCss.IconFileDitambahkan} /></span>  <span className={styleCss.dataKerentanan} >{dk.name}</span> <span className={styleCss.ditambakanPadaTgl}>ditambahkan pada : {dk.year}</span>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                        {
                                                            dataKerentanan.length == 0 && !loadKerentanan ?
                                                                <div className={styleCss.ditambahkanCard} style={{ textAlign: 'center' }}>
                                                                    <span className={styleCss.dataKerentanan}  ><FontAwesomeIcon icon={faExclamationCircle} /> belum ada data kerentanan</span>
                                                                </div> :
                                                                loadKerentanan ?
                                                                    <div>
                                                                        <div className={styleCss.ditambahkanCard}>
                                                                            <Skeleton variant="rect" width="100%">
                                                                                <div style={{ height: 20 }} />
                                                                            </Skeleton>
                                                                        </div>

                                                                        <div className={styleCss.ditambahkanCard}>
                                                                            <Skeleton variant="rect" width="100%">
                                                                                <div style={{ height: 20 }} />
                                                                            </Skeleton>
                                                                        </div>

                                                                    </div> : ''

                                                        }


                                                    </GridItem>
                                                    {
                                                        loadKerentanan ? '' :
                                                            <GridItem md={3}>
                                                                <Button variant="outlined" onClick={() => history.push('/data-kerentanan')} className={styleCss.buttonLihatData}>Lihat Data</Button>
                                                                <Typography onClick={() => downloadFormat()} className={styleCss.UnduhData}>Unduh format data</Typography>
                                                            </GridItem>
                                                    }

                                                </GridContainer>
                                            </div>
                                        </div>

                                        <div  >
                                            <Typography className={styleCss.JudulIsiRingkasan}>
                                                <ArrowDropDownIcon style={{ color: '#00A6A6' }} />
                                            Visualisasi Data Bahaya</Typography>
                                            <div>
                                                <div style={{ marginLeft: "50px" }}>
                                                    <GridContainer>

                                                        {
                                                            loadBahaya ?
                                                                <GridItem md="12">
                                                                    <GridContainer>
                                                                        <GridItem md={4} key={1 + 'radars'}  >
                                                                            <Skeleton variant="rect" width="100%">
                                                                                <div style={{ height: 120 }} />
                                                                            </Skeleton>
                                                                        </GridItem>
                                                                        <GridItem md={4} key={2 + 'radars'}  >
                                                                            <Skeleton variant="rect" width="100%">
                                                                                <div style={{ height: 120 }} />
                                                                            </Skeleton>
                                                                        </GridItem>
                                                                        <GridItem md={4} key={3 + 'radars'}  >
                                                                            <Skeleton variant="rect" width="100%">
                                                                                <div style={{ height: 120 }} />
                                                                            </Skeleton>
                                                                        </GridItem>
                                                                    </GridContainer>
                                                                </GridItem> :
                                                                dataBahaya.slice(0, 3).map((db, index) => {
                                                                    return (
                                                                        <GridItem md={4} key={index + 'radar'}  >

                                                                            <div style={{ border: '1px solid', padding: '1 !important' }}>
                                                                                <Radar data={db.datas} options={db.options} />
                                                                            </div>
                                                                        </GridItem>
                                                                    )
                                                                })
                                                        }

                                                        {
                                                            dataBahaya.length == 0 && !loadBahaya ?
                                                                <GridItem md={12} key={'noradar'} style={{ textAlign: 'center', height: 100, marginTop: 50 }}>
                                                                    <span className={styleCss.dataKerentanan}  ><FontAwesomeIcon icon={faExclamationCircle} /> Tidak ada data Visualisasi Bahaya</span>
                                                                </GridItem>
                                                                :
                                                                ''

                                                        }


                                                    </GridContainer>
                                                </div>
                                                <div >
                                                    <button className={styleCss.lihatDataSemua} onClick={handleClickOpen} > Lihat semua data bahaya tersimpan</button>

                                                    <Dialog
                                                        className={styleCss.dialogPopUp}

                                                        open={openDialog}
                                                        TransitionComponent={Transition}
                                                        keepMounted

                                                        aria-labelledby="alert-dialog-slide-title"
                                                        aria-describedby="alert-dialog-slide-description"
                                                    >

                                                        <DialogTitle id="alert-dialog-slide-title"><IconButton aria-label="close" className={styleCss.closeButtonDialog} onClick={handleClose}>
                                                            <CloseIcon />
                                                        </IconButton></DialogTitle>
                                                        <div className={styleCss.divDataBahayaJudul}>
                                                            <Typography>Data Bahaya</Typography>
                                                            <label>Daftar data bahaya yang tersimpan di akun anda</label>
                                                        </div>

                                                        <DialogContent className={styleCss.dialogPopUpConnten}>

                                                            {
                                                                dataBahaya.map((db, index) => {
                                                                    return (
                                                                        <Card className={styleCss.IsiDataBahayaPopup}>
                                                                            <GridContainer>
                                                                                <GridItem md={3}>
                                                                                    <Radar data={db.datas} options={db.options} />
                                                                                </GridItem>
                                                                                <GridItem md={9}>

                                                                                    <GridContainer>
                                                                                        <GridItem md={7}>
                                                                                            <div className={styleCss.divKeteranganBahayaTersimpan}>
                                                                                                <Typography className={styleCss.KetBahaya1}>
                                                                                                    {db.level__name} - {db.penyakit__name}
                                                                                                </Typography>
                                                                                                <Typography className={styleCss.KetBahaya2}>
                                                                                                    {db.period__name} - {db.model__name}
                                                                                                </Typography>

                                                                                                <Typography className={styleCss.KetBahaya3}>
                                                                                                    {db.wilayah__name}
                                                                                                </Typography>

                                                                                                <label className={styleCss.KetBahaya4}> {gf.formatDateFromDT(db.created_date)} | {db.user__name}</label>
                                                                                            </div>
                                                                                        </GridItem>
                                                                                        <GridItem md={5} style={{ marginRight: "-10px" }}>
                                                                                            <GridContainer >
                                                                                                {/* <div className={styleCss.IconMenuBahayaTersimpan} >
                                                                                                    <Visibility />
                                                                                                    <CreateIcon />
                                                                                                    <DeleteIcon />
                                                                                                    <CloudDownloadIcon />
                                                                                                </div> */}
                                                                                            </GridContainer>

                                                                                            <IconButton className={styleCss.IconPrintBahayaTersimpan}>
                                                                                                <PrintIcon />
                                                                                            </IconButton>
                                                                                        </GridItem>
                                                                                    </GridContainer>

                                                                                </GridItem>
                                                                            </GridContainer>
                                                                        </Card>

                                                                    )
                                                                })
                                                            }



                                                        </DialogContent>

                                                    </Dialog>


                                                </div>
                                            </div>
                                        </div>

                                        <div  >
                                            <Typography className={styleCss.JudulIsiRingkasanValidasiData}>
                                                <ArrowDropDownIcon style={{ color: '#00A6A6' }} />
                                                Validasi Data Bahaya</Typography>
                                            <div style={{ overflowX: "auto", marginLeft: "50px" }}>
                                                {
                                                    loadBahaya ?
                                                        <Skeleton variant="rect" width="100%">
                                                            <div style={{ height: 200 }} />
                                                        </Skeleton>
                                                        :
                                                        <table className={styleCss.TableValidasiBahaya}>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama Wilayah</th>
                                                                <th>Nama Penyakit</th>
                                                                <th>Jumlah Kejadian</th>

                                                            </tr>
                                                            {
                                                                dataValidasi.map((dv, index) => {
                                                                    return (
                                                                        <tr>
                                                                            <td>{index + 1}</td>
                                                                            <td>{dv.wilayah__name}</td>
                                                                            <td>{dv.penyakit__name}</td>
                                                                            <td>{dv.kejadian}</td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }
                                                            {
                                                                dataValidasi.length == 0 ?
                                                                    <tr>
                                                                        <td colSpan="4" align="center"><FontAwesomeIcon icon={faExclamationCircle} />Data Validasi Belum  Ada</td>



                                                                    </tr> : ''
                                                            }

                                                        </table>

                                                }

                                            </div>
                                        </div>

                                        <div  >
                                            <Typography className={styleCss.JudulIsiRingkasanValidasiData}>
                                                <ArrowDropDownIcon style={{ color: '#00A6A6' }} />
                                                Pilihan Adaptasi</Typography>
                                            <div style={{ overflowX: "auto", marginLeft: "50px" }}>
                                                {
                                                    loadAdaptasi ?
                                                        <Skeleton variant="rect" width="100%">
                                                            <div style={{ height: 200 }} />
                                                        </Skeleton> :
                                                        <table className={styleCss.TableValidasiBahaya}>
                                                            <tr>
                                                                <th>No</th>
                                                                {
                                                                    head.length == 0 ?
                                                                        <th>Data Desa</th>
                                                                        :
                                                                        head.map((dh) => {
                                                                            return (
                                                                                <th>{dh} </th>
                                                                            )
                                                                        })
                                                                }


                                                            </tr>
                                                            {
                                                                body.length == 0 ?
                                                                    <tr>
                                                                        <td colSpan="2"><FontAwesomeIcon icon={faExclamationCircle} /> Belum ada data Adaptasi</td>
                                                                    </tr>
                                                                    :
                                                                    bodyLength.map((da, index) => {
                                                                        return (
                                                                            <tr>
                                                                                <td>{index + 1}</td>
                                                                                {
                                                                                    head.map((dh, indexC) => {
                                                                                        return (
                                                                                            <td>{body[indexC][index]}</td>
                                                                                        )
                                                                                    })
                                                                                }
                                                                            </tr>
                                                                        )
                                                                    })
                                                            }


                                                        </table>
                                                }


                                            </div>

                                        </div>

                                    </GridItem>
                                    <GridItem md={4}>
                                        <div className={styleCss.RecentActivities}>
                                            <div className={styleCss.TextRecentActivities}>
                                                <GridContainer><ArrowDropDownIcon style={{ color: '#00A6A6' }} />
                                                    <Typography className={styleCss.TextRecentActivitiesTypo} >
                                                        Recent Activities</Typography></GridContainer>
                                                {
                                                    loadActivity ?
                                                        <div>
                                                            <div style={{ width: 100 }}><Skeleton variant="text" /> </div>
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <div style={{ width: 100 }}><Skeleton variant="text" /> </div>
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <div style={{ width: 100 }}><Skeleton variant="text" /> </div>
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />

                                                            <div style={{ width: 100 }}><Skeleton variant="text" /> </div>
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />

                                                        </div>
                                                        :
                                                        dataActivity.map((dact, index) => {
                                                            return (

                                                                <div key={'da' + index}>
                                                                    <div className={styleCss.TextRecentActivitiesBorder}>
                                                                        <span className={styleCss.TextRecentActivitiesSPan}>{gf.formatDate(dact[0])}</span>

                                                                    </div>

                                                                    <div className={styleCss.marginToday}>
                                                                        <GridContainer >
                                                                            {
                                                                                dact[1].map((dactC, index2) => {
                                                                                    return (
                                                                                        <div key={'dac' + index + '' + index2} className={styleCss.activityContainer}>
                                                                                            <FiberManualRecordIcon className={styleCss.IconToday} />
                                                                                            <span className={styleCss.textToday}>{dactC.action}</span><span className={styleCss.labelToday} > - {dactC.remarks} </span>
                                                                                        </div>
                                                                                    )
                                                                                })
                                                                            }


                                                                        </GridContainer>
                                                                    </div>


                                                                </div>
                                                            )
                                                        })

                                                }





                                            </div>
                                        </div>
                                    </GridItem>
                                </GridContainer>

                            </div>

                        </Card>
                    </GridItem>
                </GridContainer>
            </CardBody>






        </div>
    );
}
