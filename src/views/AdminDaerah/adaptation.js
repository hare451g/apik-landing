
import React, { Component, useEffect, useState } from "react";
import YouTube from 'react-youtube';
import LoadingOverlay from 'react-loading-overlay';
import { Radar, Chart } from 'react-chartjs-2';
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";


// core components
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import InputLabel from "@material-ui/core/InputLabel";
import InfoIcon from '@material-ui/icons/Info';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import ExposureIcon from '@material-ui/icons/Exposure';
import DescriptionIcon from '@material-ui/icons/Description';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import Paper from '@material-ui/core/Paper';
import Snackbar from "components/Snackbar/Snackbar.js";

import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

import Checkbox from '@material-ui/core/Checkbox';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import Navbar from "components/Navbars/Navbar.js";
import routes from "routes.js";

// Css & js Style
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import styleCss from "../../assets/css/views/global.module.css";
import { bugs, website, server } from "variables/general.js";

// Icons
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import NotesIcon from '@material-ui/icons/Notes';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import ReplayIcon from '@material-ui/icons/Replay';
import PrintIcon from '@material-ui/icons/Print';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCity, faLandmark, faArchway, faBuilding, faLayerGroup, faExclamationCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons'


import {
    dailySalesChart,
    emailsSubscriptionChart,
    completedTasksChart
} from "variables/charts.js";


import { Divider, Typography } from "@material-ui/core";

import {
    Map, Marker, Popup, TileLayer,
    ZoomControl,
    Circle,
    FeatureGroup,
    LayerGroup,
    LayersControl,
    Rectangle,
    GeoJSON,
    WMSTileLayer
} from 'react-leaflet';
import L from 'leaflet';
import api from 'config/api';
import Control from 'react-leaflet-control';
import CircularProgress from '@material-ui/core/CircularProgress';

Chart.defaults.global.legend.display = false;
const useStyles = makeStyles(styles);
const BootstrapInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: theme.spacing(3),
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: "#00A6A6 ",
        border: '1px solid #ced4da',
        borderRadius: '30px',
        fontSize: 12,
        height: 17,
        color: '#FFFFFF',
        padding: '9px 10px 10px 18px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '"Helvetica Neue"',
            'Arial',
            'Helvetica',
            '"sans-serif"',
        ].join(','),
        '&:focus': {
            borderRadius: '30px',
            color: '#00000',
            borderColor: '#80bdff',
            backgroundColor: "#00A6A6 ",
            // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
    },
}))(InputBase);



const mapRef = React.createRef(null);
export default function Home() {

    const history = useHistory();
    const [alert, setAlert] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadOverlay, setIsLoadOverlay] = useState(false);
    const [sidebarleft, setSidebarLeft] = useState(false);
    const [sidebarRight, setSidebarRight] = useState(false);
    const [geoJsonData, setGeoJsonData] = useState()
    const [loadGeoJson, setLoadGeoJson] = useState(true)
    const [dropdownCodeMap, setDropdownCodeMap] = useState('')
    const classes = useStyles();

    const [leftMap, setLeftMap] = useState(false);
    const [rightMap, setRightMap] = useState(false);
    const [btnValidasi, setBtnValidasi] = useState(false);
    const [savedAdaptasi, setSavedAdaptasi] = useState({});
    const [baseLayer, setBaseLayer] = useState(false);
    const [disabledDd, setDisabledDd] = useState(false);
    const [selectedDesa, setSelectedDesa] = useState('');
    const [dataDaerah, setDataDaerah] = useState({ prov: '', kabkot: '', kec: '', desa: '' })

    const [dropdown, setDropdown] = useState({
        tingkat: '',
        jenis: '',
        tahun: '',
        model: ''
    });
    const [listTahun, setListTahun] = useState([]);
    const [listTingkat, setListTingkat] = useState([]);
    const [listJenis, setListJenis] = useState([]);
    const [listModel, setListModel] = useState([]);
    const [listLayer, setListLayer] = useState([]);
    const [dataLegend, setDataLegend] = useState(false);
    const [listLegend, setListLegend] = useState([]);
    const [idDesa, setIdDesa] = useState();
    const [rightSideData, setRightSideData] = useState([]);
    const [rightSideTitle, setRightSideTitle] = useState({ tingkat: { name: null }, jenis: { name: '' } });
    const [isReset, setIsReset] = useState(false);
    const [dataUser, setDataUser] = useState(false);
    const [isValidated, setIsValidated] = useState(false);
    const [isCreate, setIsCreate] = useState(false);
    const [dataBahayaParams, setDataBahayaParams] = useState({});
    const [loadBahayaParams, setloadBahayaParams] = useState(false);
    const [idEdit, setIdEdit] = useState('');
    const [listAdaptasi, setListAdaptasi] = useState([]);
    const [defaultAdaptasi, setDefaultAdaptasi] = useState([]);
    const [selectedChecked, setSelectedChecked] = useState([]);
    const [isHasOther, setIshasOther] = useState(false);
    const [otherText, setOtherText] = useState('');



    let idDataDesa = ''


    // map state, full, halfright, halfleft, fullhalf

    const [age, setAge] = React.useState('');
    const handleChangeSelect = (e) => {
        setIsReset(true)

        let states = e.target.name
        let values = JSON.parse(e.target.value)
        console.log(values)
        if (states === 'tingkat') {
            getMasterType('Jenis', values.key);
            getMasterType('Model', values.key);
            getMasterType('Period', values.key)
        }
        setDropdown({
            ...dropdown,
            jenis: states === 'tingkat' ? '' : dropdown.jenis,
            tahun: states === 'tingkat' || states === 'jenis' ? '' : dropdown.tahun,
            model: states === 'tingkat' || states === 'jenis' || states === 'tahun' ? '' : dropdown.model,
            [states]: e.target.value,

        })
    };

    const getMasterType = (type, group) => {
        api.getMasterType(type, group)
            .then((res) => {
                // setIsLoading(true);
                if (res.data.error_code == 0) {
                    if (type === 'Level') {
                        let dataL = res.data.data.filter(function (el) {
                            return el.type_id == 1;
                        });
                        setListTingkat(res.data.data)
                    } else if (type === 'Jenis') {
                        setListJenis(res.data.data)
                    } else if (type === 'Period') {
                        setListTahun(res.data.data)
                    } else if (type === 'Model') {
                        setListModel(res.data.data)
                    } else if (type === 'Legend') {
                        res.data.data[0].data.reverse()
                        setListLegend(res.data.data[0].data)
                    }
                } else {

                }

            })
            .catch((err) => {
                setIsLoading(false);

                console.log("Errorr:", err);

            })

    }


    const getGeojson = (wilayah_id, first) => {
        setLoadGeoJson(true)
        setIsReset(true)
        setIsLoading(true);
        console.log(wilayah_id, 'isi wil')
        api.getGeojsonDesa('desa', first ? wilayah_id : dataUser.wilayah_id)
            .then((res) => {
                setIsLoading(false);
                if (res.data.error_code == 0) {
                    let datae = res.data.data
                    setGeoJsonData(datae)

                    const map = mapRef.current.leafletElement;
                    let gjson = L.geoJson(res.data.data, {
                        style: {
                            "color": "#00A6A6",
                            "weight": 1,
                            "opacity": 1,
                            "fillOpacity": 0.5
                        }
                    }).addTo(map);
                    let bounds = gjson.getBounds()
                    map.fitBounds(bounds)

                    gjson.eachLayer(function (layer) {

                        //  initMaps(data);
                        layer.addEventListener("click", async function () {
                            let lay = layer.feature.properties
                            console.log(lay.IDDESA, ' iddesa')
                            await getDataSide(lay.IDDESA);

                            // getBahayaByParam(lay.IDDESA, dtingkat, djenis, dtahun, dmodel);

                            setSelectedDesa(lay.IDDESA);
                            setDataDaerah({
                                prov: lay.PROVINSI,
                                kabkot: lay.KABKOT,
                                kec: lay.KECAMATAN,
                                desa: lay.DESA
                            })
                        })
                        layer.addEventListener("mouseover", function (event) {
                            let lay = layer.feature.properties
                            let dataSend = lay.DESA;
                            layer.bindTooltip("<div>" + dataSend + "</div>").openTooltip();
                        });
                    })

                } else {


                }
                setLoadGeoJson(false)

            })
            .catch((err) => {
                setIsLoading(false);
                console.log("Errorr:", err);

            })
    }



    const setSidebar = (state) => {
        if (state == 'left') {
            setLeftMap(!leftMap)
        } else {
            if (dropdown.tahun && dropdown.jenis && dropdown.tingkat && isValidated) {
                // let parsemodel = dropdown.model ? JSON.parse(dropdown.model) : ''

                if (dropdown.model)
                    setRightMap(!rightMap)
            } else {
                openNotification('danger', 'Mohon lengkapi data dropdown dan pilih peta dahulu.')

            }

        }
    }
    const getMapLayer = () => {
        api.getLayers()
            .then((res) => {
                // setIsLoading(true);
                if (res.data.error_code == 0) {
                    setListLayer(res.data.data)
                } else {

                }

            })
            .catch((err) => {
                setIsLoading(false);
                // alert('gagal load peta')
                console.log("Errorr:", err);

            })
    }
    const reset = () => {
        window.location.reload()
    }

    const searchData = () => {


        if (!dropdown.tingkat) {
            openNotification('danger', 'Tingkat harus dipilih!')
            return
        } else if (!dropdown.jenis) {
            openNotification('danger', 'Jenis harus dipilih!')
            return
        } else if (!dropdown.tahun) {
            openNotification('danger', 'Tahun harus dipilih!')
            return
        } else {

            let dtingkat = JSON.parse(dropdown.tingkat)
            let djenis = JSON.parse(dropdown.jenis)
            let dtahun = JSON.parse(dropdown.tahun)
            let dmodel = JSON.parse(dropdown.model)
            let comb = ''
            if (dtingkat.name === 'Keterpaparan' || dtingkat.name === 'Kerentanan') {
                comb = dtingkat.code + djenis.code + dtahun.code + djenis.code
            } else {
                comb = dtingkat.code + djenis.code + dtahun.code + dmodel.code
            }
            console.log(dmodel, 'isi model')
            setRightSideTitle({
                tingkat: dtingkat,
                jenis: djenis,
                tahun: dtahun,
                model: dmodel
            })
            setDropdownCodeMap('apik:' + comb)
            setDisabledDd(true);
            setBaseLayer(true);
            setDataLegend(true);

            getGeojson(dtingkat, djenis, dtahun, dmodel)



        }


    }

    const doSubmit = () => {
        if (isCreate) {
            createAdaptasi()
        } else {
            editAdaptasi()
        }
    }
    const createAdaptasi = () => {
        setIsLoadOverlay(true)
        let detailAdaptasi = []

        for (let i = 0; i < selectedChecked.length; i++) {
            if (selectedChecked[i]) detailAdaptasi.push({ adaptation: defaultAdaptasi[i].name });
        }
        if (isHasOther) detailAdaptasi.push({ adaptation: otherText });

        let dats = {
            "wilayah_id": selectedDesa,
            "details": detailAdaptasi
        }
        console.log(dats, 'isi dats')
        api.createAdaptasi(dats)
            .then((res) => {
                setIsLoadOverlay(false)
                if (res.data.error_code == 0) {
                    openNotification('success', 'Berhasil Menyimpan data.')
                } else {
                    openNotification('danger', res.data.message)
                }
            })
            .catch((err) => {
                openNotification('danger', 'koneksi bermasalah')
                setIsLoadOverlay(false)
                console.log("Errorr:", err);

            })
    }
    const editAdaptasi = () => {
        setIsLoadOverlay(true)
        let detailAdaptasi = []

        for (let i = 0; i < selectedChecked.length; i++) {
            if (selectedChecked[i]) detailAdaptasi.push({ adaptation: defaultAdaptasi[i].name });
        }
        if (isHasOther) detailAdaptasi.push({ adaptation: otherText });

        let dats = {
            "id": idEdit,
            "wilayah_id": selectedDesa,
            "details": detailAdaptasi

        }
        api.editAdaptasi(dats)
            .then((res) => {
                setIsLoadOverlay(false)
                if (res.data.error_code == 0) {
                    openNotification('success', 'Berhasil Mengubah data.')
                } else {
                    openNotification('danger', res.data.message)
                }
            })
            .catch((err) => {
                setIsLoadOverlay(false)
                console.log("Errorr:", err);

            })
    }

    const getDefaultAdaptasi = (iddesa, tingkat, level, tahun, model) => {
        setIsLoading(true)
        return api.getDefaultAdaptasi(iddesa, tingkat, level, tahun, model ? model : '')
            .then((res) => {
                setIsLoading(false)
                if (res.data.error_code == 0) {
                    setIsValidated(true)
                    let vals = res.data.data ? res.data.data : []

                    return vals
                }
            })
            .catch((err) => {
                setIsLoading(false);
                console.log("Errorr:", err);

            })
    }

    const getDataSide = (iddesa, dtingkat, djenis, dtahun, dmodel) => {

        return api.getAdaptasi(iddesa)
            .then(async (res) => {
                if (res.data.error_code == 0) {
                    setIsValidated(true)
                    let dats = res.data.data
                    setSavedAdaptasi(dats)
                    if (dats.length == 0) {
                        setIsCreate(true)
                    } else {
                        console.log(dats, 'isi dataside')

                        setIsCreate(false)
                        setIdEdit(dats[0].id)
                    }
                    let vals = []
                    let dBahaya = await getDefaultAdaptasi(iddesa, 1, 1, 1, 1);
                    let dKer = await getDefaultAdaptasi(iddesa, 3, 1, 1, 0);
                    let dket = await getDefaultAdaptasi(iddesa, 4, 1, 1, 0);

                    for (let i = 0; i < dBahaya.length; i++) {
                        vals.push(dBahaya[i])
                    }
                    for (let i = 0; i < dKer.length; i++) {
                        vals.push(dKer[i])
                    }
                    for (let i = 0; i < dket.length; i++) {
                        vals.push(dket[i])
                    }
                    console.log(vals, 'combined')
                    let selected = new Array(vals.length)

                    if (dats.length !== 0) {
                        for (let i = 0; i < vals.length; i++) {
                            selected[i] = false
                            for (let j = 0; j < dats[0].details.length; j++) {
                                if (vals[i].name == dats[0].details[j].adaptation) {
                                    selected[i] = true
                                }
                            }
                        }
                    }

                    setSelectedChecked(selected)
                    setIshasOther(false)
                    setDefaultAdaptasi(vals)
                    setRightMap(true)
                }
            })
            .catch((err) => {
                setIsLoading(false);
                console.log("Errorr:", err);

            })
    }

    function closeNotification() {
        setAlert({
            isAlertOpen: false,
            alertState: 'danger',
        })
    }
    function openNotification(condition, Message) {
        if (condition == 'success') {
            setAlert({
                isAlertOpen: true,
                alertState: 'success',
                AlertMessage: Message
            })
        } else {
            setAlert({
                isAlertOpen: true,
                alertState: 'danger',
                AlertMessage: Message
            })
        }
        setTimeout(function () {
            closeNotification()
        }
            .bind(this), 5000);
    }

    const getUserById = (id) => {
        setIsLoading(true)
        api.getUserById(id)
            .then((res) => {
                if (res.data.error_code == 0) {
                    setDataUser(res.data.data[0])
                    let comb = res.data.data[0].wilayah__prov_no + '' + res.data.data[0].wilayah__kab_kot_no
                    getGeojson(comb, true)

                }
                setIsLoading(false)
            })
            .catch((err) => {
                setIsLoading(false)
                console.log("Errorr:", err);
            })
    }
    const setCheckAdaptasi = (e, index) => {
        console.log(e.target.checked, 'isi check')
        let ad = selectedChecked
        ad[index] = e.target.checked
        console.log(ad, 'changed check')
        setSelectedChecked(ad);
    }

    useEffect(() => {
        if (!localStorage.getItem('token')) history.push('home')
        localStorage.setItem('iddes', null);
        getMasterType('Level');
        getMasterType('Legend');
        getMapLayer();
        getUserById(localStorage.getItem('email'))



        //get native Map instance
    }, []);


    return (
        <div className={styleCss.mapContainer}>
            <LoadingOverlay
                active={isLoadOverlay}
                spinner
                text='Sedang menyimpan data...'
                style={{ color: 'white' }}
            >
            </LoadingOverlay>
            <Navbar
                // routes={routes}
                // handleDrawerToggle={handleDrawerToggle}
                // {...rest}
                width={rightMap ? 'calc(100% - 300px)' : '100%'}
            />
            <Snackbar
                place="tc"
                color={alert.alertState}
                icon={alert.alertState == 'success' ? CheckIcon : ErrorIcon}
                message={alert.AlertMessage}
                open={alert.isAlertOpen}
                closeNotification={() => closeNotification()}
                close
            />
            {/* <div className={styleCss.selectHome}>

                <FormControl  >
                    <GridContainer>

                        <NativeSelect
                            className={styleCss.SelectCari}

                            id="demo-customized-select-native"
                            value={dropdown.tingkat}
                            name="tingkat"
                            onChange={handleChangeSelect}
                            input={<BootstrapInput />}
                            disabled={disabledDd}

                        >
                            <option style={{ color: '#000000' }} aria-label="None" value="" disabled >Pilihan Tingkat</option>
                            {
                                listTingkat.map((drp, index) => {
                                    return (
                                        <option style={{ color: '#000000' }} key={'tingkat' + index} value={JSON.stringify(drp)}>{drp.name}</option>
                                    )
                                })
                            }

                        </NativeSelect>

                        <NativeSelect
                            style={{ marginRight: "10px" }}
                            id="demo-customized-select-native"
                            value={dropdown.jenis}
                            name="jenis"
                            onChange={handleChangeSelect}
                            input={<BootstrapInput />}
                            disabled={disabledDd}
                        >
                            <option style={{ color: '#000000' }} aria-label="None" value="" >Pilihan Jenis</option>
                            {
                                listJenis.map((drp, index) => {
                                    return (
                                        <option style={{ color: '#000000' }} key={'jenis' + index} value={JSON.stringify(drp)}>{drp.name}</option>
                                    )
                                })
                            }
                        </NativeSelect>

                        <NativeSelect
                            style={{ marginRight: "10px" }}
                            id="demo-customized-select-native"
                            value={dropdown.tahun}
                            name="tahun"
                            onChange={handleChangeSelect}
                            input={<BootstrapInput />}
                            disabled={disabledDd}
                        >
                            <option style={{ color: '#000000' }} aria-label="None" value="" >Pilihan Tahun</option>
                            {
                                listTahun.map((drp, index) => {
                                    return (
                                        <option style={{ color: '#000000' }} key={'tahun' + index} value={JSON.stringify(drp)}>{drp.name}</option>
                                    )
                                })
                            }
                        </NativeSelect>

                        {
                            dropdown.jenis.includes('APIK') || dropdown.jenis.includes('SIDIK') ?
                                ''
                                :
                                <NativeSelect

                                    id="demo-customized-select-native"
                                    value={dropdown.model}
                                    name="model"
                                    onChange={handleChangeSelect}
                                    input={<BootstrapInput />}
                                    disabled={disabledDd}
                                >
                                    <option style={{ color: '#000000' }} aria-label="None" value="" >Pilihan Model</option>
                                    {
                                        listModel.map((drp, index) => {
                                            return (
                                                <option style={{ color: '#000000' }} key={'model' + index} value={JSON.stringify(drp)}>{drp.name}</option>
                                            )
                                        })
                                    }

                                </NativeSelect>


                        }



                    </GridContainer>
                </FormControl>
                <IconButton aria-label="Cari" className={styleCss.ButtonCari} onClick={() => searchData()} >
                    <ArrowForwardIcon />
                </IconButton>
                {
                    isReset ?
                        <IconButton aria-label="Cari" className={styleCss.ButtonReset} onClick={() => reset()} >
                            <ReplayIcon />
                        </IconButton> : ''
                }





            </div> */}

            <div className={leftMap ? styleCss.sidebarLeft : styleCss.sidebarLeftNone}>
                <div className={leftMap ? styleCss.sidebarLeftIconActive : styleCss.sidebarLeftIcon} onClick={() => setSidebar('left')}>
                    <span className={styleCss.layerspan}><FontAwesomeIcon icon={faLayerGroup} /> Layer</span>
                </div>
                <div className={leftMap ? styleCss.sidebarLeftContent : styleCss.leftSidebarDataNone}>
                    <div className={styleCss.sidebarLeftContentTitle}>
                        Layer Properties
          </div>
                    <div >
                        <Paper className={styleCss.paperSearch}>

                            <InputBase
                                // type="button"
                                className={styleCss.inputSearch}
                                placeholder="Search Layer..."
                            />
                            <IconButton type="button" className={styleCss.iconButton} aria-label="search">
                                <SearchIcon />
                            </IconButton>



                        </Paper>

                    </div>
                    <div>
                        <FormControlLabel
                            style={{ width: '100%', padding: 5 }}
                            control={<Checkbox name="BaseLayer" checked={baseLayer} onClick={() => setBaseLayer(!baseLayer)} className={styleCss.checkboxTextFormat} />}
                            label="Base Layer"


                        />
                        {
                            listLayer.map((lyr, index) => {
                                return (
                                    <div>
                                        <FormControl component="fieldset" className={styleCss.checkboxTextFormat} >
                                            <FormLabel component="legend" className={styleCss.checkboxTextFormatTitle} style={{ fontWeight: '600 !important', width: '100%' }}>{lyr.created_by__name}</FormLabel>
                                            <FormGroup>
                                                {
                                                    lyr.layers.map((lyrC, index) => {
                                                        return (
                                                            <FormControlLabel
                                                                key={'layer' + index}
                                                                control={<Checkbox name="gilad" className={styleCss.checkboxTextFormat} />}
                                                                label={lyrC.program_name + ' - ' + lyrC.layer_name}
                                                            />
                                                        )
                                                    })
                                                }
                                            </FormGroup>
                                        </FormControl>
                                    </div>
                                )
                            })
                        }

                    </div>

                </div>

            </div>
            <Map center={[0.7893, 120.9213]} zoomControl={false} zoom={5} animate={false} className={rightMap ? styleCss.Maphalf : styleCss.Map}
                scrollWheelZoom={false}
                // onlayeradd={esrimap()}

                ref={mapRef}>
                {
                    isLoading ?
                        <div>
                            <CircularProgress className={styleCss.spinnerMap} />
                        </div>
                        :
                        <div>
                            <TileLayer
                                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                zIndex={-2}
                            />

                            {
                                baseLayer ?
                                    <WMSTileLayer
                                        layers='apik:apik_desa'
                                        url="https://geoserver.piarea.co.id:8443/geoserver/gwc/service/wms/"
                                        format="image/PNG"
                                        // opacity= "0.7"
                                        styles={dropdownCodeMap}

                                    /> : ''
                            }

                            {/* {
                                listLegend.length > 0 ?
                                    <Control position="bottomleft">
                                        <div className={styleCss.BoxLegend} >
                                            <div className={styleCss.LegendCms}>
                                                <GridContainer >
                                                    <GridItem xs={4}>
                                                        <NotesIcon className={styleCss.IconInfoLegendHeader} />
                                                    </GridItem>
                                                    <GridItem xs={8}>

                                                        <Typography className={styleCss.TextLegend}>Legend</Typography>

                                                    </GridItem>
                                                </GridContainer>
                                            </div>

                                            {
                                                dataLegend ?
                                                    <table>

                                                        {
                                                            listLegend.map((dl, index) => {
                                                                return (
                                                                    <tr>
                                                                        <td> <div style={{
                                                                            width: 14, height: 14, background: dl.fill, border: '1px solid' + dl.outline, marginLeft: 20, marginBottom: 5, marginTop: 5
                                                                        }}></div></td>
                                                                        <td>
                                                                            {dl.range}
                                                                        </td>
                                                                        <td>{dl.category}</td>


                                                                    </tr>
                                                                )
                                                            })
                                                        }

                                                    </table>
                                                    : <div style={{ padding: 20, textAlign: 'center' }}>
                                                        <FontAwesomeIcon icon={faExclamationCircle} /> Belum ada data terpilih
                          </div>

                                            }



                                        </div>
                                    </Control>
                                    : ''


                            } */}
                            {loadGeoJson ?
                                <GeoJSON data={geoJsonData} style={{ "color": "#00A6A6", "weight": 1, "opacity": 0.65 }} /> :
                                ''
                            }


                            <div style={{ marginTop: 50 }}>
                                <ZoomControl position="topright" ></ZoomControl>
                            </div>
                        </div>

                }




            </Map>

            <div className={rightMap ? styleCss.sidebarRight : styleCss.sidebarRightNone}>
                <div className={styleCss.sidebarRightIcon} onClick={() => setSidebar()} >
                    <FontAwesomeIcon icon={faInfoCircle} className={styleCss.sidebarRightIconPos} />

                </div>
                <div className={rightMap ? styleCss.sidebarRightContent : styleCss.sidebarRightContentNone}>
                    <div className={styleCss.sidebarRightContentTitle}>
                        Adaptasi Data {rightSideTitle.tingkat.name}{' - ' + rightSideTitle.jenis.name}
                    </div>
                    {/* 
                    <div>
                        <Radar data={dataBahayaParams.data} options={dataBahayaParams.options} />
                    </div> */}


                    <table>
                        <tr>

                            <td style={{ fontWeight: 600 }}>
                                Info Wilayah
                        </td>

                        </tr>
                        <Divider />
                        <tr>

                            <td><FontAwesomeIcon icon={faLandmark} /> Provinsi</td>
                            <td>: {dataDaerah.prov}</td>
                        </tr>
                        <tr>
                            <td><FontAwesomeIcon icon={faCity} /> Kab/Kota</td>
                            <td>: {dataDaerah.kabkot}</td>
                        </tr>
                        <tr>
                            <td><FontAwesomeIcon icon={faBuilding} /> Kecamatan</td>
                            <td>: {dataDaerah.kec}</td>
                        </tr>
                        <tr>
                            <td><FontAwesomeIcon icon={faArchway} /> Desa/Kel</td>
                            <td>: {dataDaerah.desa}</td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>

                    <div>
                        <div style={{ fontWeight: 600 }}>
                            Pilihan Adaptasi
                    </div>
                        <FormGroup >
                            {
                                defaultAdaptasi.map((da, index) => {
                                    return (
                                        <FormControlLabel
                                            className={styleCss.labelForm}
                                            style={{ color: 'black' }}
                                            control={

                                                <Checkbox color="default" key={'checked' + index} checked={selectedChecked[index]} onClick={(e) => setCheckAdaptasi(e, index)} />}
                                            label={da.name}
                                        />
                                    )
                                })
                            }
                            <div className={styleCss.adaptationOtherContainer}>
                                <Checkbox color="default" key={'checkedLainnya'} name="checkedA" checked={isHasOther || false} onClick={(e) => setIshasOther(e.target.checked)} />
                                <input style={{ paddingTop: 5, paddingBottom: 5, width: '100%' }} placeholder="Lainnya" value={otherText} onChange={(e) => setOtherText(e.target.value)} />
                            </div>

                        </FormGroup>
                    </div>

                    <div style={{ marginTop: 300, textAlign: 'center' }}>
                        <Button variant="outlined" onClick={() => doSubmit()} className={styleCss.btnSaveValidasi}>Save</Button>
                        <IconButton className={styleCss.IconPrintBahayaTersimpans}  >
                            <PrintIcon />
                        </IconButton>
                    </div>

                </div>
            </div>






            <div className={styleCss.footerHome}>
                @2020 Copyright APIK
        </div>
        </div >
    );
}
