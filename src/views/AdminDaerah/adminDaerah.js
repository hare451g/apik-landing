
import React, { Component, useEffect, useState } from "react";
import YouTube from 'react-youtube';

// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles, withStyles } from "@material-ui/core/styles";



// core components
import Grid from '@material-ui/core/Grid';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import InputLabel from "@material-ui/core/InputLabel";
import InfoIcon from '@material-ui/icons/Info';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import ExposureIcon from '@material-ui/icons/Exposure';
import DescriptionIcon from '@material-ui/icons/Description';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';

import Checkbox from '@material-ui/core/Checkbox';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';

// Css & js Style
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import styleCss from "../../../src/assets/css/views/global.module.css"
import { bugs, website, server } from "variables/general.js";

// Icons
import StopIcon from '@material-ui/icons/Stop';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import NotesIcon from '@material-ui/icons/Notes';
import NearMeIcon from '@material-ui/icons/NearMe';
import Brightness1Icon from '@material-ui/icons/Brightness1';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
// Image 
import hutan from "../../assets/img/forestK.jpg";

import forest from "../../assets/img/forest.svg"
import forest1 from "../../assets/img/forest1.svg"
import { Router, Route, Switch, Link } from "react-router-dom";


import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";


import { Typography } from "@material-ui/core";

import {
  Map, Marker, Popup, TileLayer,
  ZoomControl,
  Circle,
  FeatureGroup,
  LayerGroup,
  LayersControl,
  Rectangle,
  GeoJSON
} from 'react-leaflet';
import api from 'config/api'
import Control from 'react-leaflet-control';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(styles);
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);



const mapRef = React.createRef(null);
export default function AdminDaerah() {

  const [legendEsri, setLegendEsri] = useState('');
  const [isRenderMap, setIsRenderMap] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const classes = useStyles();
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedF: true,
    checkedG: true,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  const [age, setAge] = React.useState('');
  const handleChangeSelect = (event) => {
    setAge(event.target.value);
  };

  function dataLegend() {

    setIsLoading(true);
    setIsRenderMap(true);
    api.dataLegend()
      .then((res) => {
        if (res.data.layers.length !== 0) {
          setLegendEsri(res.data.layers[0].legend)
          setIsLoading(false);
          setIsRenderMap(false);
          esrimap()
        }

      })
      .catch((err) => {
        setIsLoading(false);
        alert('gagal load peta')
        console.log("Errorr:", err);

      })

  }

  function esrimap() {
    const map = mapRef.current.leafletElement;
    var esri = require('esri-leaflet');
    esri.tiledMapLayer({
      url: 'http://geoportal.menlhk.go.id/arcgis/rest/services/KLHK/Kawasan_Hutan/MapServer/',
      // zoomOffsetAllowance: 9,

    }).addTo(map)
  }
  useEffect(() => {
    dataLegend();

    //get native Map instance
  }, []);


  return (
    <div style={{ position: "relative" }}>


      <div className={styleCss.selectHome}>
        <FormControl  >
          <GridContainer>

            <NativeSelect
            className={styleCss.SelectCari}
             
              id="demo-customized-select-native"
              value={age}
              onChange={handleChangeSelect}
              input={<BootstrapInput />}
            >
              <option aria-label="None" value="" />
              <option value={10}>Ten</option>
              <option value={20}>Twenty</option>
              <option value={30}>Thirty</option>
            </NativeSelect>

            <NativeSelect
              style={{ marginRight: "10px" }}
              id="demo-customized-select-native"
              value={age}
              onChange={handleChangeSelect}
              input={<BootstrapInput />}
            >
              <option aria-label="None" value="" />
              <option value={10}>Ten</option>
              <option value={20}>Twenty</option>
              <option value={30}>Thirty</option>
            </NativeSelect>

            <NativeSelect
              style={{ marginRight: "10px" }}
              id="demo-customized-select-native"
              value={age}
              onChange={handleChangeSelect}
              input={<BootstrapInput />}
            >
              <option aria-label="None" value="" />
              <option value={10}>Ten</option>
              <option value={20}>Twenty</option>
              <option value={30}>Thirty</option>
            </NativeSelect>

            <NativeSelect

              id="demo-customized-select-native"
              value={age}
              onChange={handleChangeSelect}
              input={<BootstrapInput />}
            >
              <option aria-label="None" value="" />
              <option value={10}>Ten</option>
              <option value={20}>Twenty</option>
              <option value={30}>Thirty</option>
            </NativeSelect>

          

          </GridContainer>
        </FormControl>
        <IconButton aria-label="Cari" className={styleCss.ButtonCari} >
              <ArrowForwardIcon />
            </IconButton>

      </div>

      <Map center={[0.7893, 120.9213]} zoomControl={false} zoom={5} animate={false} className={styleCss.Map}
        // onlayeradd={esrimap()}

        ref={mapRef}>
        {
          isLoading ?
            <div>
              <CircularProgress className={styleCss.spinnerMap} />
            </div>
            :
            <div>

              {
                legendEsri ?
                  <Control position="bottomleft" >
                    <div style={{ background: 'rgba(0,0,0,0.2)', padding: 5, float: "right" }}>

                      <h4>Kawasan Hutan</h4>
                      {legendEsri.map((data, index) => {
                        const color = ['red', 'blue', 'green', 'DarkMagenta', 'Gray']
                        return (
                          <div><img src={'data:image/png;base64, ' + data.imageData} /><span style={{ padding: 5 }}>: {data.label}</span><br></br></div>
                        )
                      })}
                    </div>
                  </Control>
                  :
                  ''
              }

              <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                zIndex={-2}
              />
              <Control position="bottomleft">
                <div className={styleCss.BoxLegend} >

                  <div className={styleCss.LegendCms}>
                    <GridContainer >
                      <GridItem xs={4}>
                        <NotesIcon className={styleCss.IconInfoLegendHeader} />
                      </GridItem>
                      <GridItem xs={8}>

                        <Typography className={styleCss.TextLegend}>Legend</Typography>

                      </GridItem>
                    </GridContainer>
                  </div>

                  <div>
                    <GridContainer >
                      <GridItem xs={4}>
                        <StopIcon className={styleCss.IconLegend} style={{ color: "#D9001B" }} />
                      </GridItem>
                      <GridItem xs={8}>

                        <Typography className={styleCss.TextLegend}>Sangat Tinggi</Typography>

                      </GridItem>
                    </GridContainer>
                  </div>
                  <div>
                    <GridContainer >
                      <GridItem xs={4}>
                        <StopIcon className={styleCss.IconLegend} />
                      </GridItem>
                      <GridItem xs={8}>

                        <Typography className={styleCss.TextLegend}>Tinggi</Typography>

                      </GridItem>
                    </GridContainer>
                  </div>
                  <div>
                    <GridContainer >
                      <GridItem xs={4}>
                        <StopIcon className={styleCss.IconLegend} />
                      </GridItem>
                      <GridItem item xs={8}>

                        <Typography className={styleCss.TextLegend}>Sedang</Typography>

                      </GridItem>
                    </GridContainer>
                  </div>
                  <div>
                    <GridContainer >
                      <GridItem xs={4}>
                        <StopIcon className={styleCss.IconLegend} />
                      </GridItem>
                      <GridItem xs={8}>

                        <Typography className={styleCss.TextLegend}>Rendah</Typography>

                      </GridItem>
                    </GridContainer>
                  </div>
                  <div>
                    <GridContainer>
                      <GridItem xs={4}>
                        <StopIcon className={styleCss.IconLegend} />
                      </GridItem>
                      <GridItem xs={8}>

                        <Typography className={styleCss.TextLegend}>Sangat Rendah</Typography>

                      </GridItem>
                    </GridContainer>
                  </div>

                </div>

              </Control>



              <Control position="bottomleft">
                <div className={styleCss.BoxWilayah} >

                  <div className={styleCss.LegendCms}>
                    <GridContainer >
                      <Grid item xs={4}>
                        <NearMeIcon className={styleCss.IconInfoLegendHeader} />
                      </Grid>
                      <Grid item xs={8}>

                        <Typography className={styleCss.TextLegendJudul}>Info Wilayah</Typography>

                      </Grid>
                    </GridContainer>
                  </div>

                  <div>
                    <GridContainer>
                      <Grid item xs={4}>
                        <Brightness1Icon className={styleCss.IconInfo} />
                      </Grid>
                      <Grid item xs={8}>

                        <Typography className={styleCss.TextInfo}>Provinsi X</Typography>

                      </Grid>
                    </GridContainer>
                  </div>
                  <div>
                    <GridContainer >
                      <Grid item xs={4}>
                        <Brightness1Icon className={styleCss.IconInfo} />
                      </Grid>
                      <Grid item xs={8}>

                        <Typography className={styleCss.TextInfo}>Kabupaten Y</Typography>

                      </Grid>
                    </GridContainer>
                  </div>

                  <div>
                    <GridContainer >
                      <Grid item xs={4}>
                        <Brightness1Icon className={styleCss.IconInfo} />
                      </Grid>
                      <Grid item xs={8}>

                        <Typography className={styleCss.TextInfo}>Kecamatan Z</Typography>

                      </Grid>
                    </GridContainer>
                  </div>
                  <div>
                    <GridContainer >
                      <Grid item xs={4}>
                        <Brightness1Icon className={styleCss.IconInfo} />
                      </Grid>
                      <Grid item xs={8}>

                        <Typography className={styleCss.TextInfo}>Desa A</Typography>

                      </Grid>
                    </GridContainer>
                  </div>

                </div>

              </Control>
              <ZoomControl position="bottomright" />
            </div>

        }




      </Map>






    </div>
  );
}
