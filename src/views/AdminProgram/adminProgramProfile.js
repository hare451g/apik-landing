import React, { Component, useEffect, useState } from "react";
import api from 'config/api';
import gf from 'config/globalFunc';
// react plugin for creating charts
import { useHistory } from "react-router-dom";
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';

// @material-ui/icons
import LoadingOverlay from 'react-loading-overlay';
import CreateIcon from '@material-ui/icons/Create';
import Snackbar from "components/Snackbar/Snackbar.js";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import CloseIcon from '@material-ui/icons/Close';
import PrintIcon from '@material-ui/icons/Print';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Skeleton from '@material-ui/lab/Skeleton';
import { faCity, faLandmark, faCheck, faTimes, faArchway, faBuilding, faLayerGroup, faExclamationCircle, faInfoCircle, faMarker, faMapMarkerAlt, faMapPin, faTint } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { TreeItem } from '@material-ui/lab';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';


import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';

// Image
import diagonal1 from '../../assets/img/chart1.PNG';
import diagonal2 from '../../assets/img/chart2.PNG';
import diagonal3 from '../../assets/img/chart3.PNG';

import { bugs, website, server } from "variables/general.js";

import {
    dailySalesChart,
    emailsSubscriptionChart,
    completedTasksChart
} from "variables/charts.js";

import styles from "assets/jss/material-dashboard-react/views/registerStyle.js";
import styleCss from "../../../src/assets/css/views/global.module.css"

import { Typography } from "@material-ui/core";

const useStyles = makeStyles(styles);

function handleClick(event) {
    event.preventDefault();
    console.info('You clicked a breadcrumb.');
}

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function AdminProgramProfile() {
    const classes = useStyles();
    const history = useHistory();
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });
    const [open, setOpen] = React.useState(true);
    const [loading, setLoading] = React.useState(false);
    const [alert, setAlert] = useState({});
    const [openDialog, setOpenDialog] = React.useState(false);
    const [loadActivity, setLoadActivity] = React.useState(true);
    const [loadList, setLoadList] = React.useState(true);
    const [dataActivity, setDataActivity] = React.useState([]);
    const [dataList, setDataList] = React.useState([]);
    const [dataUser, setDataUser] = React.useState({ data: {} });
    const [loadUser, setLoadUser] = React.useState(true);
    const [listIcon, setLisIcon] = React.useState([
        { name: 'faMarker', icon: faMarker },
        { name: 'faMapMarkerAlt', icon: faMapMarkerAlt },
        { name: 'faMapPin', icon: faMapPin },
        { name: 'faTint', icon: faTint },
        { name: 'faBuilding', icon: faBuilding },
        { name: 'faLandmark', icon: faLandmark },
        { name: 'faCity', icon: faCity },

    ]);
    const [isAPI, setIsAPI] = React.useState('api');
    const [layerName, setLayerName] = React.useState('');
    const [apiName, setApiName] = React.useState('');
    const [excelInput, setExcelInput] = React.useState('');
    const [selectedIcon, setSelectedIcon] = React.useState('');
    const handleClickOpen = () => {
        setOpenDialog(true);
    };

    const handleClose = () => {
        setOpenDialog(false);
    };

    const handleClick = () => {
        setOpen(!open);
    };

    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };


    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };

    function closeNotification() {
        setAlert({
            isAlertOpen: false,
            alertState: 'danger',
        })
    }

    const openNotification = (condition, Message) => {
        if (condition == 'success') {
            setAlert({
                isAlertOpen: true,
                alertState: 'success',
                AlertMessage: Message
            })
        } else {
            setAlert({
                isAlertOpen: true,
                alertState: 'danger',
                AlertMessage: Message
            })
        }
        setTimeout(function () {
            closeNotification()
        }
            .bind(this), 5000);
    }

    const convertDate = (newData, oldName, name) => {
        return newData.map(item => {
            var temp = Object.assign({}, item);
            let tempDate = temp.created_date.split(' ')
            temp.created_date = tempDate[0]
            return temp;
        })
    }
    const downloadFormat = () => {
        let urlDownload = api.getProgramExcelTemplate()
        window.open(urlDownload);
    }
    const doUpload = () => {
        setLoading(true)

        if (isAPI == "api") {
            let data = {
                layer_name: layerName,
                program_name: localStorage.getItem('name'),
                api_target: apiName,
                url_legend: apiName
            };
            api.uploadProgramAPI(data)
                .then((res) => {
                    if (res.data.error_code == 0) {
                        openNotification('success', res.data.message)
                        getList();
                        setOpenDialog(false);
                    } else {
                        openNotification('danger', res.data.message)
                    }
                    setLoading(false)
                })
                .catch((err) => {
                    setLoading(false)
                    console.log("Errorr:", err);
                })
        } else {
            let formData = new FormData();
            formData.append('file', excelInput)
            formData.append('layer_name', layerName)
            formData.append('program_name', localStorage.getItem('name'))
            formData.append('map_pin', selectedIcon)
            api.uploadProgramExcel(formData)
                .then((res) => {
                    if (res.data.error_code == 0) {
                        openNotification('success', res.data.message)
                        getList();
                        setOpenDialog(false);
                    } else {
                        openNotification('danger', res.data.message)
                    }
                    setLoading(false)
                })
                .catch((err) => {
                    setLoading(false)
                    console.log("Errorr:", err);
                })
        }
    }

    const getActivity = (id) => {
        setLoadActivity(true)
        api.getActivityById(id)
            .then((res) => {
                if (res.data.error_code == 0) {
                    let dataa = res.data.data;
                    dataa.reverse();
                    dataa = convertDate(dataa)

                    dataa = dataa.length == 0 ? [] : gf.groupBy(dataa, (c) => c.created_date)
                    dataa = Object.entries(dataa)
                    console.log(dataa);
                    setDataActivity(dataa)
                } else {

                }
                setLoadActivity(false)
            })
            .catch((err) => {
                setLoadActivity(false)
                console.log("Errorr:", err);
            })
    }

    const getList = (id) => {
        setLoadList(true)
        api.getLayerPrograms(id)
            .then((res) => {
                if (res.data.error_code == 0) {
                    let dataa = res.data.data;
                    setDataList(dataa)
                } else {
                    openNotification('danger', res.data.message)
                }
                setLoadList(false)
            })
            .catch((err) => {
                setLoadList(false)
                console.log("Errorr:", err);
            })
    }

    const getUserById = (id) => {
        setLoadUser(true)
        api.getUserById(id)
            .then((res) => {
                if (res.data.error_code == 0) {
                    let datauser = res.data.data[0]
                    let dt = datauser.data;
                    if (typeof dt === 'object' && dt !== null) {

                    } else {
                        dt = {
                            Alamat: '',
                            KepalaDinas: '',
                            PenJawab: '',
                            Nomor: ''
                        }
                    }
                    datauser.data = dt
                    console.log(datauser, 'isi data user')
                    setDataUser(datauser)
                    setLoadUser(false)
                } else {

                    history.push('home')
                }

            })
            .catch((err) => {
                setLoadUser(false)
                console.log("Errorr:", err);
            })
    }

    const renderProfile = () => {
        return (
            <div>
                <div >
                    <IconButton className={styleCss.ProfilePic}>
                        <Typography className={styleCss.nameProfile}>B </Typography>

                        <IconButton className={styleCss.editProfile} onClick={() => history.push('/edit-program-profile')}>
                            <CreateIcon />
                        </IconButton>

                    </IconButton>
                    <Typography className={styleCss.namaAdminDaerah}> {dataUser.name} </Typography>
                    <Typography className={styleCss.emailAdminDaerah}> {dataUser.email}</Typography>


                </div>
                <TreeView
                    className={styleCss.TreeView}
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    defaultExpanded={['1']}
                    multiSelect
                >
                    <TreeItem className={styleCss.TreeItemColor} nodeId="1" label="Informasi Umum">
                        <TreeItem className={styleCss.TreeItem} nodeId="2" label={"Ka. Program : " + dataUser.data.kepalaDinas} />
                        <TreeItem className={styleCss.TreeItem} nodeId="3" label={"Pen. Jawab : " + dataUser.data.penJawab} />
                        <TreeItem className={styleCss.TreeItem} nodeId="4" label={"Alamat Dinas : " + dataUser.data.alamat} />
                    </TreeItem>

                </TreeView>
                <TreeView
                    className={styleCss.TreeView}
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    defaultExpanded={['1']}
                    multiSelect
                >
                    <TreeItem className={styleCss.TreeItemColor} nodeId="1" label="Informasi Kontak">
                        <TreeItem className={styleCss.TreeItem} nodeId="2" label={"Email : " + dataUser.email} />
                        <TreeItem className={styleCss.TreeItem} nodeId="3" label={"Nomor : " + dataUser.data.nomor} />
                    </TreeItem>

                </TreeView>

            </div>

        )
    }

    const renderDialog = () => {
        return (
            <Dialog
                className={styleCss.dialogPopUpUbahPassword}
                open={openDialog}
                maxWidth="sm"
                fullWidth={true}
            >
                <DialogTitle id="alert-dialog-slide-title">
                    <IconButton aria-label="close" className={styleCss.closeButtonDialog} onClick={() => setOpenDialog(false)}>
                        <CloseIcon />
                    </IconButton>

                </DialogTitle>
                <div className={styleCss.divKonfirmasiTindakan}>
                    <Typography className={styleCss.JudulDialogUploadData}>Upload data APIK</Typography>
                    {/* <Typography className={styleCss.JudulSubDialog}> 'Silahkan Login ke Akun anda'</Typography> */}
                </div>
                <DialogContent className={styleCss.dialogPopUpConntenDataUpload}>

                    <div>
                        <div>
                            <div className={styleCss.uploadInputTitle}>Nama File</div>
                            <Input className={styleCss.uploadInputText} onChange={(e) => setLayerName(e.target.value)} placeholder="Ketik disini..."></Input>
                        </div>
                        <div style={{ marginTop: 20, marginBottom: 20 }} >
                            <div className={styleCss.uploadRadioChoice} onClick={(e) => setIsAPI(e.target.value)}>
                                <input type="radio" name="site_name" value="api" defaultChecked /> API / URL
                            </div>
                            <div>
                                <Input className={styleCss.uploadInputText2} value={apiName} onChange={(e) => setApiName(e.target.value)} disableUnderline placeholder="Ketik disini..." disabled={isAPI == "api" ? false : true}></Input>
                            </div>
                            <div className={styleCss.uploadRadioChoice} onClick={(e) => setIsAPI(e.target.value)}>
                                <input type="radio" name="site_name" value="excel" /> File Excel
                            </div>
                            <div>
                                <input type="file" className={styleCss.uploadInputText} onChange={(e) => setExcelInput(e.target.files[0])} disabled={isAPI == "api" ? true : false} />
                            </div>
                            {isAPI == 'excel' ?
                                <div style={{ float: "right" }}>
                                    <span className={styleCss.uploadInputIcon} >pilih Icon lokasi titik : </span>
                                    <Select
                                        className={styleCss.iconSelector}
                                        disableUnderline
                                        onChange={(e) => setSelectedIcon(e.target.value)}

                                        name="selectedIcon"
                                    >
                                        <MenuItem value="" disabled></MenuItem>
                                        {listIcon.map((data) => {
                                            return (
                                                <MenuItem value={data.name} ><FontAwesomeIcon className={styleCss.uploadInputIcon} icon={data.icon} /></MenuItem>
                                            )
                                        })}
                                    </Select>
                                </div>
                                : ""}
                        </div>


                    </div>
                    <div style={{ marginTop: 50 }} >
                        <Button className={styleCss.buttonLogin} onClick={() => doUpload()}>Upload</Button>
                    </div>
                </DialogContent>


            </Dialog>

        )
    }

    useEffect(() => {
        if (!localStorage.getItem('token')) history.push('home')
        getUserById(localStorage.getItem('email'));
        getActivity(localStorage.getItem('userId'));
        getList();

        //get native Map instance
    }, []);

    return (
        <div className={styleCss.Container}>

            <div style={{ marginTop: "90px", marginLeft: "-10px", marginBottom: "-10px" }}>
                <Breadcrumbs separator="›" aria-label="breadcrumb">
                    <Link color="inherit" onClick={() => history.push('home')} className={styleCss.Link}>
                        <ArrowBackIosIcon className={styleCss.IconBreadcrumbs} />
                        Kembali ke peta
      </Link>

                </Breadcrumbs>
            </div>


            <CardBody >
                <GridContainer>
                    <GridItem md={3}>
                        <Card className={styleCss.CardProfileRingkasan}>
                            <div className={styleCss.headerCard}>Profile</div>

                            {
                                loadUser ?
                                    <div style={{ paddingLeft: 20, paddingRight: 20 }}>
                                        <div style={{ textAlign: 'center', padding: 66, paddingBottom: 0 }}>
                                            <Skeleton width={120} height={120} variant="circle" />
                                        </div>
                                        <div style={{ marginTop: 50 }}>
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                        </div>
                                        <div style={{ marginTop: 50 }}>
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                        </div>

                                        <div style={{ marginTop: 50 }}>
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                            <Skeleton variant="text" />
                                        </div>


                                    </div> :
                                    renderProfile()
                            }

                        </Card>

                    </GridItem>

                    <GridItem md={9}>
                        <Card className={styleCss.CardProfileRingkasan}>
                            <div className={styleCss.headerCard}>Ringkasan</div>
                            <div>
                                <GridContainer >
                                    <GridItem md={8}>
                                        <div className={styleCss.DataSaya}>
                                            <div  >

                                                <Typography className={styleCss.JudulDataSaya}>
                                                    <ArrowDropDownIcon style={{ color: '#00A6A6' }} />
                                                Data Saya</Typography>

                                                { }
                                                <div style={{ overflowX: "auto", overflowY: "auto", margin: " 15px 15px", marginBottom: "50px", maxHeight: "500px" }}>
                                                    {
                                                        loadList ?
                                                            <Skeleton variant="rect" width="100%">
                                                                <div style={{ height: 200 }} />
                                                            </Skeleton> :
                                                            <table className={styleCss.TableValidasiBahaya}>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nama File </th>
                                                                    <th>Jenis </th>
                                                                    <th>Tanggal </th>
                                                                    <th>Note </th>
                                                                    <th className={styleCss.AprvTh}>Aprv </th>

                                                                </tr>

                                                                {
                                                                    dataList.map((dt, index) => {
                                                                        return (
                                                                            <tr>
                                                                                <td>{index + 1}</td>
                                                                                <td>{dt.layer_name}</td>
                                                                                <td>{dt.jenis}</td>
                                                                                <td >{gf.formatDateFromDT(dt.created_date)}</td>
                                                                                <td>{dt.remarks}</td>
                                                                                <td>{dt.approval ? <FontAwesomeIcon style={{ color: '#00A6A6' }} icon={faCheck} /> : <FontAwesomeIcon style={{ color: 'red' }} icon={faTimes} />}</td>
                                                                            </tr>
                                                                        )
                                                                    })
                                                                }
                                                                {
                                                                    dataList.length == 0 && !loadList ?
                                                                        <tr>
                                                                            <td colSpan="6"><FontAwesomeIcon icon={faExclamationCircle} />Data Belum ada</td>

                                                                        </tr> : ''
                                                                }
                                                            </table>

                                                    }

                                                    <Typography className={styleCss.downloadFormatData} onClick={() => downloadFormat()}>Download format data excel</Typography>

                                                </div>
                                                <Button className={styleCss.uploadButtonadminProgram} onClick={() => setOpenDialog(!openDialog)} >Upload</Button>

                                            </div>
                                        </div>

                                    </GridItem>
                                    <GridItem md={4}>
                                        <div className={styleCss.RecentActivities}>
                                            <div className={styleCss.TextRecentActivities}>
                                                <GridContainer><ArrowDropDownIcon style={{ color: '#00A6A6' }} />
                                                    <Typography className={styleCss.TextRecentActivitiesTypo} >
                                                        Recent Activities</Typography></GridContainer>
                                                {
                                                    loadActivity ?
                                                        <div>
                                                            <div style={{ width: 100 }}><Skeleton variant="text" /> </div>
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <div style={{ width: 100 }}><Skeleton variant="text" /> </div>
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <div style={{ width: 100 }}><Skeleton variant="text" /> </div>
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />

                                                            <div style={{ width: 100 }}><Skeleton variant="text" /> </div>
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />
                                                            <Skeleton variant="text" width="200" />

                                                        </div>
                                                        :
                                                        dataActivity.map((dact, index) => {
                                                            return (

                                                                <div key={'da' + index}>
                                                                    <div className={styleCss.TextRecentActivitiesBorder}>
                                                                        <span className={styleCss.TextRecentActivitiesSPan}>{gf.formatDate(dact[0])}</span>

                                                                    </div>

                                                                    <div className={styleCss.marginToday}>
                                                                        <GridContainer >
                                                                            {
                                                                                dact[1].map((dactC, index2) => {
                                                                                    return (
                                                                                        <div key={'dac' + index + '' + index2} className={styleCss.activityContainer}>
                                                                                            <FiberManualRecordIcon className={styleCss.IconToday} />
                                                                                            <span className={styleCss.textToday}>{dactC.action}</span><span className={styleCss.labelToday} > - {dactC.remarks} </span>
                                                                                        </div>
                                                                                    )
                                                                                })
                                                                            }


                                                                        </GridContainer>
                                                                    </div>


                                                                </div>
                                                            )
                                                        })

                                                }
                                                {dataActivity.length == 0 && !loadActivity ?
                                                    <div className={styleCss.textToday} style={{ textAlign: 'center', marginTop: 20 }}>

                                                        <FontAwesomeIcon icon={faExclamationCircle} />Belum ada aktivitas

                                                    </div>

                                                    : ''
                                                }




                                            </div>
                                        </div>
                                    </GridItem>

                                </GridContainer>

                            </div>

                        </Card>
                    </GridItem>
                </GridContainer>
            </CardBody>



            {
                renderDialog()
            }
            <LoadingOverlay
                active={loading}
                spinner
                text='Mohon tunggu...'
                style={{ color: 'white' }}
            ></LoadingOverlay>
            <Snackbar
                place="tc"
                color={alert.alertState}
                icon={alert.alertState == 'success' ? CheckIcon : ErrorIcon}
                message={alert.AlertMessage}
                open={alert.isAlertOpen}
                closeNotification={() => closeNotification()}
                close
            />
        </div>
    );
}
