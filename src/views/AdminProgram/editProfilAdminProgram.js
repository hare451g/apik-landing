import React, { Component, useEffect, useState } from "react";
import api from 'config/api';
import LoadingOverlay from 'react-loading-overlay';
import { useHistory } from "react-router-dom";
import Snackbar from "components/Snackbar/Snackbar.js";

// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import CustomInput from "components/CustomInput/CustomInput.js";
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import FormGroup from '@material-ui/core/FormGroup';





// @material-ui/icons
import CreateIcon from '@material-ui/icons/Create';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import CloseIcon from '@material-ui/icons/Close';
import PrintIcon from '@material-ui/icons/Print';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import Search from "@material-ui/icons/Search";



// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { TreeItem } from '@material-ui/lab';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import InputBase from '@material-ui/core/InputBase';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';

// Image
import diagonal1 from '../../assets/img/chart1.PNG';
import diagonal2 from '../../assets/img/chart2.PNG';
import diagonal3 from '../../assets/img/chart3.PNG';

import { bugs, website, server } from "variables/general.js";

import {
    dailySalesChart,
    emailsSubscriptionChart,
    completedTasksChart
} from "variables/charts.js";


import styles from "assets/jss/material-dashboard-react/views/registerStyle.js";
import styleCss from "../../../src/assets/css/views/global.module.css"

import { Typography } from "@material-ui/core";
import { render } from "react-dom";

const useStyles = makeStyles(styles);

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function EditProfilAdminProgram() {
    const history = useHistory();
    const classes = useStyles();
    const [isLoading, setIsLoading] = React.useState(true);
    const [alert, setAlert] = useState({});
    const [open, setOpen] = React.useState(true);
    const [openDialog, setOpenDialog] = React.useState(false);
    const [value, setValue] = React.useState(0);
    const [loadUser, setLoadUser] = React.useState(false);
    const [dataUser, setDataUser] = React.useState(false);
    const [openDialogSubmit, setOpenDialogSubmit] = React.useState(false);
    const [openDialogYa, setOpenDialogYa] = React.useState(false);
    const [email, setEmail] = React.useState('');
    const [nomorKontak, setNomorKontak] = React.useState('');
    const [kabkot, setKabkot] = React.useState('');
    const [kepalaDinas, setKepalaDinas] = React.useState('');
    const [alamat, setAlamat] = React.useState('');
    const [penJawab, setPenJawab] = React.useState('');
    const [newPassword, setNewPassword] = React.useState('');
    const [currentPassword, setCurrentPassword] = React.useState('');
    const [rePassword, setRePassword] = React.useState('');
    const [nameChar, setNameChar] = React.useState('');


    const [state, setState] = React.useState({
        checkedA: true,
        checkedB: true,
        checkedF: true,
        checkedG: true,
    });
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    function closeNotification() {
        setAlert({
            isAlertOpen: false,
            alertState: 'danger',
        })
    }
    function openNotification(condition, Message) {
        if (condition == 'success') {
            setAlert({
                isAlertOpen: true,
                alertState: 'success',
                AlertMessage: Message
            })
        } else {
            setAlert({
                isAlertOpen: true,
                alertState: 'danger',
                AlertMessage: Message
            })
        }
        setTimeout(function () {
            closeNotification()
        }
            .bind(this), 5000);
    }

    const handleChangePassword = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    const handleChangeCheckBox = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };
    const handleChangeTab = (event, newValue) => {
        setValue(newValue);
    };

    const handleClickOpen = () => {
        setOpenDialog(true);
    };

    const handleClose = () => {
        setOpenDialog(false);
        setOpenDialogYa(false);
    };
    const handleClickOpenSubmit = () => {
        setOpenDialogSubmit(true);
    };

    const handleCloseSubmit = () => {
        setOpenDialogSubmit(false);
    };
    const handleClickOpenYA = () => {
        setOpenDialogYa(true);
    };

    const handleCloseYa = () => {
        setOpenDialogYa(false);
    };

    const handleClick = () => {
        setOpen(!open);
    };

    const Transition = React.forwardRef(function Transition(props, ref) {
        return <Slide direction="up" ref={ref} {...props} />;
    });

    const getUserById = (id) => {
        setLoadUser(true)
        setIsLoading(true)
        api.getUserById(id)
            .then((res) => {
                if (res.data.error_code == 0) {
                    let datauser = res.data.data[0]
                    let dt = datauser.data;
                    setNameChar(datauser.name.charAt(0))
                    setEmail(datauser.email)
                    setKabkot(datauser.name)
                    if (typeof dt === 'object' && dt !== null) {
                        setNomorKontak(dt.nomor ? dt.nomor : '')
                        setKepalaDinas(dt.kepalaDinas ? dt.kepalaDinas : '')
                        setAlamat(dt.alamat ? dt.alamat : '')
                        setPenJawab(dt.penJawab ? dt.penJawab : '')
                    } else {

                        setNomorKontak()
                        setKepalaDinas('')
                        setAlamat('')
                        setPenJawab('')

                    }
                    datauser.data = dt
                    console.log(datauser, 'isi data user')
                    setDataUser(datauser)
                    setLoadUser(false)
                    setIsLoading(false)
                } else {
                    history.push('home')
                }

            })
            .catch((err) => {
                setLoadUser(false)
                console.log("Errorr:", err);
            })
    }
    const doUpdate = () => {
        if (newPassword !== rePassword) {
            return
        }
        let dUpdate = {
            "id": dataUser.id,
            "name": kabkot,
            "email": email,
            "image": null,
            "data": {
                kepalaDinas: kepalaDinas,
                nomor: nomorKontak,
                alamat: alamat,
                penJawab: penJawab,


            },
            "active": true,
        }
        setIsLoading(true)
        api.updateProfile(dUpdate)
            .then((res) => {
                if (res.data.error_code == 0) {
                    setOpenDialogYa(true)
                    setOpenDialogSubmit(false)
                    setIsLoading(false)
                }
            })
    }

    const doChangePassword = () => {

        let dUpdate = {
            "id": dataUser.id,
            "current_password": currentPassword,
            "new_password": newPassword,
            "new_confirm_passoword": newPassword,

            "active": true,
        }
        setIsLoading(true)
        api.changePassword(dUpdate)
            .then((res) => {
                if (res.data.error_code == 0) {
                    setOpenDialogYa(true)
                    setOpenDialogSubmit(false)
                    setIsLoading(false)
                }
            })
            .catch((err) => {
                setIsLoading(false)
                openNotification('danger', 'Password lama tidak sesuai')
                console.log("Errorr:", err.response);
            })
    }


    useEffect(() => {
        if (!localStorage.getItem('token')) history.push('home')
        getUserById(localStorage.getItem('email'));

        //get native Map instance
    }, []);



    return (
        <div className={styleCss.ContainerEditProgramProfil}>

            <div style={{ marginTop: "90px", marginLeft: "-70px", marginBottom: "-10px" }}>
                <Breadcrumbs separator="›" aria-label="breadcrumb">
                    <Link color="inherit" href="/program-profile" onClick={handleClick} className={styleCss.Link}>
                        <ArrowBackIosIcon className={styleCss.IconBreadcrumbs} />
                        Kembali ke Profil
      </Link>

                </Breadcrumbs>
            </div>


            <CardBody >
                <Card className={styleCss.CardEditProfilAdminProgram}>
                    <div style={{ margin: "0px 30px" }}>
                        <div className={styleCss.DivJudulEditUbah}>
                            <Typography className={styleCss.EditProfil}>
                                Edit Profil
                    </Typography>
                            <label>Ubah informasi profil akunmu di bawah ini</label>
                        </div>
                        <GridContainer>
                            <GridItem md={12}>
                                <div style={{ margin: '10px 10px' }}>
                                    <div>
                                        <div className={styleCss.borderLabel}>
                                            <h5 className={styleCss.LabelEditProfilAkun}>Informasi Akun</h5>
                                            <GridContainer>
                                                <GridItem md={6}>
                                                    <div style={{ marginLeft: '30px', marginBottom: '30px' }}>
                                                        <IconButton className={styleCss.IconEditProfil}>
                                                            <Typography >
                                                                <div className={styleCss.nameEditProfil}> {nameChar}</div>
                                                            </Typography>
                                                        </IconButton>
                                                    </div>
                                                </GridItem>
                                                <GridItem md={6}>
                                                    <Button className={styleCss.ButtonCopot}>Copot</Button>
                                                    <br />
                                                    <Button className={styleCss.ButtonUbah}>Ubah</Button>
                                                </GridItem>
                                            </GridContainer>
                                            <div style={{ margin: '0px 11px' }}>
                                                <Typography className={styleCss.TypographyInformasiAkun}>Kota / Kabupaten</Typography>
                                                <Input className={styleCss.InputInformasiAkun} value={kabkot} onChange={(e) => setKabkot(e.target.value)} placeholder="Kota/Kabupaten"></Input>
                                                <Typography className={styleCss.TypographyInformasiAkun}>Email</Typography>
                                                <Input className={styleCss.InputInformasiAkun} readOnly value={email} placeholder="Email"></Input>
                                                <Typography className={styleCss.TypographyInformasiAkun}>Nomor Kontak</Typography>
                                                <Input className={styleCss.InputInformasiAkun} value={nomorKontak} onChange={(e) => setNomorKontak(e.target.value)} placeholder="Nomor Kontak" ></Input>
                                                <div>
                                                    <Typography onClick={handleClickOpen} className={styleCss.TypographyUbahPassword}>Ubah Password</Typography>

                                                    <Dialog
                                                        className={styleCss.dialogPopUpUbahPassword}

                                                        open={openDialog}
                                                        // TransitionComponent={Transition}
                                                        // keepMounted
                                                        maxWidth="xs"
                                                        fullWidth={true}
                                                        aria-labelledby="alert-dialog-slide-title"
                                                        aria-describedby="alert-dialog-slide-description"

                                                    >

                                                        <DialogTitle id="alert-dialog-slide-title"><IconButton aria-label="close" className={styleCss.closeButtonDialog} onClick={handleClose}>
                                                            <CloseIcon />
                                                        </IconButton></DialogTitle>
                                                        <div className={styleCss.divDataUploadDataJudul}>
                                                            <Typography className={styleCss.JudulDialogUploadData}>Edit Password</Typography>
                                                            <label>Silahkan ketik email dan password</label>
                                                        </div>

                                                        <DialogContent className={styleCss.dialogPopUpConntenDataUpload} style={{ textAlign: 'left' }}>
                                                            <Typography className={styleCss.TypographyInformasiAkun}>Password Lama</Typography>
                                                            <FormControl style={{ width: '100%', fontSize: '12px !important' }}>
                                                                <Input
                                                                    // placeholder="Type here..."
                                                                    style={{ width: '100%', fontSize: '12px !important' }}
                                                                    id="standard-adornment-password"
                                                                    type={values.showPassword ? 'text' : 'password'}
                                                                    value={currentPassword}
                                                                    onChange={(e) => setCurrentPassword(e.target.value)}
                                                                    endAdornment={
                                                                        <InputAdornment position="end">
                                                                            <IconButton
                                                                                aria-label="toggle password visibility"
                                                                                onClick={handleClickShowPassword}
                                                                                onMouseDown={handleMouseDownPassword}
                                                                            >
                                                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                                                            </IconButton>
                                                                        </InputAdornment>
                                                                    }
                                                                />
                                                            </FormControl>

                                                            <Typography className={styleCss.TypographyInformasiAkun}>Password Baru</Typography>
                                                            <FormControl style={{ width: '100%' }}>
                                                                <Input
                                                                    // placeholder="Type here..."
                                                                    id="standard-adornment-password"
                                                                    type={values.showPassword ? 'text' : 'password'}
                                                                    value={newPassword}
                                                                    onChange={(e) => setNewPassword(e.target.value)}
                                                                    endAdornment={
                                                                        <InputAdornment position="end">
                                                                            <IconButton
                                                                                aria-label="toggle password visibility"
                                                                                onClick={handleClickShowPassword}
                                                                                onMouseDown={handleMouseDownPassword}
                                                                            >
                                                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                                                            </IconButton>
                                                                        </InputAdornment>
                                                                    }
                                                                />
                                                            </FormControl>
                                                            <Typography className={styleCss.TypographyInformasiAkun}>Ketik Ulang Password Baru</Typography>
                                                            <FormControl style={{ width: '100%' }}>
                                                                <Input
                                                                    // placeholder="Type here..."
                                                                    id="standard-adornment-password"
                                                                    type={values.showPassword ? 'text' : 'password'}
                                                                    value={rePassword}
                                                                    onChange={(e) => setRePassword(e.target.value)}
                                                                    endAdornment={
                                                                        <InputAdornment position="end">
                                                                            <IconButton
                                                                                aria-label="toggle password visibility"
                                                                                onClick={handleClickShowPassword}
                                                                                onMouseDown={handleMouseDownPassword}
                                                                            >
                                                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                                                            </IconButton>
                                                                        </InputAdornment>
                                                                    }
                                                                />
                                                            </FormControl>
                                                            {
                                                                newPassword !== rePassword ?
                                                                    <div style={{ color: 'red', fontSize: 12 }}>
                                                                        ketik ulang password belum sama
                                                                    </div>
                                                                    : ''
                                                            }
                                                            <div style={{ textAlign: 'center', width: "100%" }}>
                                                                <Button className={styleCss.submitUbahPassword} onClick={() => doChangePassword()} >Submit</Button>
                                                            </div>

                                                        </DialogContent>

                                                    </Dialog>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                    <div style={{ marginTop: '30px', marginBottom: '30px' }}>
                                        <div className={styleCss.borderLabel}>
                                            <h5 className={styleCss.LabelEditProfilInformasiDinas}>Informasi Dinas</h5>
                                            <div style={{ margin: '0px 11px' }}>
                                                <Typography className={styleCss.TypographyInformasiAkun}>Kepala Dinas</Typography>
                                                <Input className={styleCss.InputInformasiAkun} value={kepalaDinas} onChange={(e) => setKepalaDinas(e.target.value)} placeholder="Nama Kepala Dinas"></Input>
                                                <Typography className={styleCss.TypographyInformasiAkun}>Alamat Dinas</Typography>
                                                <Input className={styleCss.InputInformasiAkun} value={alamat} onChange={(e) => setAlamat(e.target.value)} placeholder="Alamat Dinas"></Input>
                                                <Typography className={styleCss.TypographyInformasiAkun}>Penanggung Jawab</Typography>
                                                <Input className={styleCss.InputInformasiAkun} value={penJawab} onChange={(e) => setPenJawab(e.target.value)} placeholder="Nama Penanggung Jawab" ></Input>
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{ textAlign: 'center', width: '100%' }}>
                                        <Button className={styleCss.submmiteditProfilAdminProgram} onClick={handleClickOpenSubmit} >Submit</Button>
                                    </div>
                                    <div>

                                        <Dialog
                                            className={styleCss.dialogPopUpUbahPassword}
                                            open={openDialogSubmit}
                                            // TransitionComponent={Transition}
                                            // keepMounted
                                            aria-labelledby="alert-dialog-slide-title"
                                            aria-describedby="alert-dialog-slide-description"

                                        >

                                            <DialogTitle id="alert-dialog-slide-title"></DialogTitle>
                                            <div className={styleCss.divKonfirmasiTindakan}>
                                                <Typography className={styleCss.JudulDialogUploadData}>Konfirmasi Tindakan</Typography>
                                                <label>Apakah anda yakin melanjutkan tindakan ?</label>
                                            </div>

                                            <DialogContent className={styleCss.dialogPopUpConntenDataUpload}>
                                                <div className={styleCss.divButtonKonfirmasi} >
                                                    <Button onClick={handleCloseSubmit} className={styleCss.buttonKonfirmasiTidak}  >Tidak</Button>
                                                    <Button onClick={handleClickOpenYA} onClick={() => doUpdate()} className={styleCss.buttonKonfirmasiYa} >Ya</Button>
                                                </div>
                                            </DialogContent>

                                        </Dialog>
                                        <div>
                                            <Dialog
                                                className={styleCss.dialogPopUpUbahPassword}
                                                open={openDialogYa}
                                                // TransitionComponent={Transition}
                                                // keepMounted
                                                aria-labelledby="alert-dialog-slide-title"
                                                aria-describedby="alert-dialog-slide-description"

                                            >

                                                <DialogTitle id="alert-dialog-slide-title"><IconButton aria-label="close" className={styleCss.closeButtonDialog} onClick={handleClose}>
                                                    <CloseIcon />
                                                </IconButton></DialogTitle>
                                                <div className={styleCss.divKonfirmasiTindakan}>
                                                    <Typography className={styleCss.JudulDialogUploadData}>Pembaharuan Profil Berhasil</Typography>
                                                    <label>Terima Kasih telah memperbaharui profil daerah anda.</label>
                                                </div>

                                                <DialogContent className={styleCss.dialogPopUpConntenDataUpload}>
                                                </DialogContent>

                                            </Dialog>
                                        </div>
                                    </div>
                                </div>
                            </GridItem>

                        </GridContainer>
                    </div>
                </Card>

            </CardBody>

            <Snackbar
                place="tc"
                color={alert.alertState}
                icon={alert.alertState == 'success' ? CheckIcon : ErrorIcon}
                message={alert.AlertMessage}
                open={alert.isAlertOpen}
                closeNotification={() => closeNotification()}
                close
            />
            <LoadingOverlay
                active={isLoading}
                spinner
                text='Sedang melakukan pengecekan...'
                style={{ color: 'white' }}
            >
            </LoadingOverlay>



        </div>
    );
}
