import React from "react";

// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from "@material-ui/core/InputLabel";

// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';


// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import CustomInput from "components/CustomInput/CustomInput.js";

// Image
import hutan from "../../assets/img/forestK.jpg";
import building from "../../assets/img/building.jpg";
import mediapub from "../../assets/img/mediapub.jpg";
import projectinf from "../../assets/img/projectinf.jpg";
import perpus from "../../assets/img/perpus.jpg";
import calculator from "../../assets/img/calculator.jpg";
import survey from "../../assets/img/survey.jpg"

import { bugs, website, server } from "variables/general.js";

import {
    dailySalesChart,
    emailsSubscriptionChart,
    completedTasksChart
} from "variables/charts.js";


import styles from "assets/jss/material-dashboard-react/views/loginStyle.js";
import styleCss from "../../../src/assets/css/views/global.module.css"

import { Typography } from "@material-ui/core";

const useStyles = makeStyles(styles);


export default function Login() {
    const classes = useStyles();
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };


    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };



    return (
        <div  >

            <Card className={styleCss.login}>
                <CardHeader color="success">
                    <h4 className={classes.cardTitleWhite}>LOGIN</h4>
                </CardHeader>

                <CardBody >

                    <div style={{ marginBottom: "30px", marginTop: "30px" }}>
                        <GridContainer>
                            <GridItem md={12} xs={12}>
                                <label>Email</label>
                                <Input placeholder="Email" inputProps={{ 'aria-label': 'description' }} style={{ width: "100%" }} />

                            </GridItem>
                        </GridContainer>
                    </div>

                    <div style={{ marginBottom: "30px" }}>
                        <GridContainer>
                            <GridItem md={12} xs={12}>
                                <label>Password</label>
                                <Input
                                    id="standard-adornment-password"
                                    type={values.showPassword ? 'text' : 'password'}
                                    value={values.password}
                                    onChange={handleChange('password')}
                                    placeholder="Password"
                                    style={{ width: "100%" }}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                            >

                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                />

                            </GridItem>
                        </GridContainer>
                    </div>



                    <Button variant="contained" href="#contained-buttons"
                        style={{ width: "100%", backgroundColor: "#054737", color: "white", marginBottom: "15px" }}>
                        MASUK
      </Button>
                    <div style={{ marginTop: "20px" }}>
                        <GridContainer>
                            <GridItem md={6}>
                            <Button  onClick={() => window.location.href = "/register"}> Lupa Password?</Button>
    </GridItem>
                            <GridItem md={6} >
                                <div className={classes.justify}>
                                Belum mempunya akun ? <Button  onClick={() => window.location.href = "/register"}> Register</Button>

                                </div>
                            </GridItem>
                        </GridContainer>
                    </div>
                </CardBody>
            </Card>





        </div>
    );
}
