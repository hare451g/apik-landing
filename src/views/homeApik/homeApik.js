
import React, { Component, useEffect, useState, useRef } from "react";
import { Radar, Chart } from 'react-chartjs-2';
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import LoadingOverlay from 'react-loading-overlay';
import { makeStyles, withStyles } from "@material-ui/core/styles";




// core components
import Grid from '@material-ui/core/Grid';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import InputLabel from "@material-ui/core/InputLabel";
import InfoIcon from '@material-ui/icons/Info';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import ExposureIcon from '@material-ui/icons/Exposure';
import DescriptionIcon from '@material-ui/icons/Description';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import Paper from '@material-ui/core/Paper';
import Snackbar from "components/Snackbar/Snackbar.js";

import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

import Checkbox from '@material-ui/core/Checkbox';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import Navbar from "components/Navbars/Navbar.js";
import Slider from '@material-ui/core/Slider';
import routes from "routes.js";

// Css & js Style
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import styleCss from "../../assets/css/views/global.module.css";
import { bugs, website, server } from "variables/general.js";

// Icons
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import NotesIcon from '@material-ui/icons/Notes';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import ReplayIcon from '@material-ui/icons/Replay';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PrintIcon from '@material-ui/icons/Print';
import { faCity, faLandmark, faArchway, faBuilding, faLayerGroup, faExclamationCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons'


import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";


import { Divider, Typography } from "@material-ui/core";

import {
  Map, Marker, Popup, TileLayer,
  ZoomControl,
  Circle,
  FeatureGroup,
  LayerGroup,
  LayersControl,
  Rectangle,
  GeoJSON,
  WMSTileLayer
} from 'react-leaflet';
import L from 'leaflet';
import api from 'config/api';
import gf from 'config/globalFunc';
import Control from 'react-leaflet-control';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(styles);
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    cursor: 'pointer !important',
    borderRadius: 4,
    position: 'relative',
    backgroundColor: "#00A6A6 ",
    border: '1px solid #ced4da',
    borderRadius: '30px',
    fontSize: 12,
    height: 17,
    color: '#FFFFFF',
    padding: '9px 10px 10px 18px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '"Helvetica Neue"',
      'Arial',
      'Helvetica',
      '"sans-serif"',
    ].join(','),
    '&:focus': {
      borderRadius: '30px',
      color: '#00000',
      borderColor: '#80bdff',
      backgroundColor: "#00A6A6 ",
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
    '&:disabled': {
      cursor: 'not-allowed !important',
    }
  },
}))(InputBase);


// Chart.defaults.global.legend.display = false;

export default function Home() {


  const [alert, setAlert] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [sidebarleft, setSidebarLeft] = useState(false);
  const [sidebarRight, setSidebarRight] = useState(false);
  const [geoJsonData, setGeoJsonData] = useState()
  const [loadGeoJson, setLoadGeoJson] = useState(true)
  const [dropdownCodeMap, setDropdownCodeMap] = useState('')
  const [isLoadOverlay, setIsLoadOverlay] = useState(false);
  const classes = useStyles();

  const [leftMap, setLeftMap] = useState(false);
  const [rightMap, setRightMap] = useState(false);
  const [btnValidasi, setBtnValidasi] = useState(false);
  const [valueValidasi, setValueValidasi] = useState(false);
  const [baseLayer, setBaseLayer] = useState(false);
  const [disabledDd, setDisabledDd] = useState(false);
  const [isLoadLayer, setIsLoadLayer] = useState(true);
  const mapRef = React.useRef(null);

  const [dropdown, setDropdown] = useState({
    tingkat: '',
    jenis: '',
    tahun: '',
    model: ''
  });
  const [listTahun, setListTahun] = useState([]);
  const [listTingkat, setListTingkat] = useState([]);
  const [listJenis, setListJenis] = useState([]);
  const [listModel, setListModel] = useState([]);
  const [listLayer, setListLayer] = useState([]);
  const [listSelectedLayer, setListSelectedLayer] = useState([]);
  const [dataLegend, setDataLegend] = useState(false);
  const [listLegend, setListLegend] = useState([]);
  const [idDesa, setIdDesa] = useState();
  const [rightSideData, setRightSideData] = useState([]);
  const [rightSideTitle, setRightSideTitle] = useState({ tingkat: { name: null }, jenis: { name: '' } });
  const [isReset, setIsReset] = useState(false);
  const [dataBahayaParams, setDataBahayaParams] = useState({});
  const [isCreate, setIsCreate] = useState(false);
  const [selectedDesa, setSelectedDesa] = useState('');
  const [idEdit, setIdEdit] = useState('')
  const [dataUser, setDataUser] = useState();
  const [isCanEdit, setIsCanEdit] = useState(false);
  const [valueSlider, setValueSlider] = useState({
    CHT: 0, CHM: 0, CHO: 0,
    SUT: 0, SUM: 0, SUO: 0,
    defCHT: 0, defCHM: 0, defCHO: 0,
    defSUT: 0, defSUM: 0, defSUO: 0
  });
  const [result, setResult] = useState({
    CHT: 0, CHM: 0, CHO: 0,
    SUT: 0, SUM: 0, SUO: 0
  });
  const geodataRef = useRef(null);
  const labelSH = [
    {
      value: -100,
      label: '-100%',
    },
    {
      value: 0,
      label: '0',
    },

    {
      value: 100,
      label: '100%',
    },
  ];

  const labelSU = [
    {
      value: -2,
      label: '-2°C',
    },
    {
      value: 0,
      label: '0°C',
    },

    {
      value: 2,
      label: '2°C',
    },
  ];

  let idDataDesa = ''


  // map state, full, halfright, halfleft, fullhalf
  let roles = localStorage.getItem('roles')
  roles = JSON.parse(roles)
  let rolesid = roles ? roles[0].id : ''


  const doSubmit = () => {
    if (isCreate) {
      createVisual()
    } else {
      editVisual()
    }
  }
  const createVisual = () => {
    setIsLoadOverlay(true)
    let dats = {
      "wilayah_id": selectedDesa,
      "user_id": localStorage.getItem('userId'),
      "title": "Visual data",
      "ch_tahunan": result.CHT,
      "ch_bulanan": result.CHM,
      "ch_optimum": result.CHO,
      "su_tahunan": result.SUT,
      "su_bulanan": result.SUM,
      "su_optimum": result.SUO,
      "level_id": 1,
      "penyakit_id": rightSideTitle.jenis.type_id,
      "model_id": rightSideTitle.model.type_id,
      "period_id": rightSideTitle.tahun.type_id,
    }

    api.createVisual(dats)
      .then((res) => {
        setIsLoadOverlay(false)
        if (res.data.error_code == 0) {
          openNotification('success', 'Berhasil Menyimpan data.')
          setIsCreate(false)
        } else {
          openNotification('danger', res.data.message)
        }
      })
      .catch((err) => {
        setIsLoadOverlay(false)
        console.log("Errorr:", err);

      })
  }
  const editVisual = () => {
    setIsLoadOverlay(true)
    let dats = {
      "id": idEdit,
      "wilayah_id": selectedDesa,
      "user_id": localStorage.getItem('userId'),
      "title": "Visual data",
      "ch_tahunan": result.CHT,
      "ch_bulanan": result.CHM,
      "ch_optimum": result.CHO,
      "su_tahunan": result.SUT,
      "su_bulanan": result.SUM,
      "su_optimum": result.SUO,
      "level_id": 1,
      "penyakit_id": rightSideTitle.jenis.type_id,
      "model_id": rightSideTitle.model.type_id,
      "period_id": rightSideTitle.tahun.type_id,
    }
    api.editVisual(dats)
      .then((res) => {
        setIsLoadOverlay(false)
        if (res.data.error_code == 0) {
          openNotification('success', 'Berhasil Mengubah data.')
        } else {
          openNotification('danger', res.data.message)
        }
      })
      .catch((err) => {
        setIsLoadOverlay(false)
        console.log("Errorr:", err);

      })
  }


  const handleChangeSelect = (e) => {
    setIsReset(true)

    let states = e.target.name
    let values = JSON.parse(e.target.value)
    console.log(values)
    if (states === 'tingkat') {
      getMasterType('Jenis', values.key, values);
      getMasterType('Model', values.key);
      getMasterType('Period', values.key)
    }
    setDropdown({
      ...dropdown,
      jenis: states === 'tingkat' ? '' : dropdown.jenis,
      tahun: states === 'tingkat' || states === 'jenis' ? '' : dropdown.tahun,
      model: states === 'tingkat' || states === 'jenis' || states === 'tahun' ? '' : dropdown.model,
      [states]: e.target.value,

    })
  };



  const getMasterType = (type, group, values) => {
    api.getMasterType(type, group)
      .then((res) => {
        // setIsLoading(true);
        if (res.data.error_code == 0) {
          if (type === 'Level') {
            setListTingkat(res.data.data)
          } else if (type === 'Jenis') {
            let jns = res.data.data

            if (values.type_id == 3) jns = jns.filter(function (el) {
              return el.type_id == 5;
            });

            setListJenis(jns)
          } else if (type === 'Period') {
            setListTahun(res.data.data)
          } else if (type === 'Model') {
            setListModel(res.data.data)
          } else if (type === 'Legend') {
            res.data.data[0].data.reverse()
            setListLegend(res.data.data[0].data)
          }
        } else {

        }

      })
      .catch((err) => {
        setIsLoading(false);

        console.log("Errorr:", err);

      })

  }
  const getBahayaByParam = (iddesa, dtingkat, djenis, dtahun, dmodel) => {
    console.log(dmodel, 'model')
    api.getBahayaByParam(iddesa, dtingkat, djenis, dtahun, dmodel)
      .then((res) => {
        if (res.data.error_code == 0) {
          let dats = res.data.data
          if (dats.length !== 0) {

            setValueSlider({
              CHT: dats[0].ch_tahunan, CHM: dats[0].ch_bulanan, CHO: dats[0].ch_optimum,
              SUT: dats[0].su_tahunan, SUM: dats[0].su_bulanan, SUO: dats[0].su_optimum,
              defCHT: dats[0].ch_tahunan, defCHM: dats[0].ch_bulanan, defCHO: dats[0].ch_optimum,
              defSUT: dats[0].su_tahunan, defSUM: dats[0].su_bulanan, defSUO: dats[0].su_optimum
            });


            let val = gf.calculateRadar(djenis.type_id, dats)




            let dradar = {
              labels: ['SU O', 'SU T', 'SU M', 'CH O', 'CH T', 'CH M'],
              datasets: [
                {
                  data: [val.sumFinal, val.sutFinal, val.suoFinal, 0, 0, 0],
                  backgroundColor: 'rgba(0,166,166,0.5)',
                  borderColor: 'rgba(0,166,166,1)',
                  pointBackgroundColor: 'rgba(0,166,166,1)',
                  pointBorderColor: '#fff',
                  pointHoverBackgroundColor: '#fff',
                  pointHoverBorderColor: 'rgba(0,166,166,1)',
                  label: 'SU'
                },
                {
                  data: [0, 0, 0, val.dataCHM, val.dataCHT, val.dataCHO],
                  backgroundColor: 'rgba(255,99,132,0.5)',
                  borderColor: 'rgba(255,99,132,1)',
                  pointBackgroundColor: 'rgba(255,99,132,1)',
                  pointBorderColor: '#fff',
                  pointHoverBackgroundColor: '#fff',
                  pointHoverBorderColor: 'rgba(255,99,132,1)',
                  label: 'CH'
                }
              ]
            }
            let options = {
              responsive: true,
              maintainAspectRatio: false
            }
            setDataBahayaParams({ data: dradar, options: options })
          }
        }
      })
      .catch((err) => {
        setIsLoading(false);
        console.log("Errorr:", err);

      })
  }

  const getVisual = (iddesa, dtingkat, djenis, dtahun, dmodel) => {
    api.getVisual(iddesa, dtingkat, djenis, dtahun, dmodel)
      .then((res) => {
        if (res.data.error_code == 0) {
          let dats = res.data.data
          if (dats.length == 0) {
            setIsCreate(true)
          } else {
            setIsCreate(false)
            setIdEdit(dats[0].id)
          }
        }
      })
      .catch((err) => {
        setIsLoading(false);
        console.log("Errorr:", err);

      })
  }

  const getGeojson = (type, filter, isBeginning) => {
    setLoadGeoJson(true)
    if (!isBeginning) {
      setIsReset(true)
    }
    setIsLoading(true);
    api.getGeojson(type, filter)
      .then((res) => {
        setIsLoading(false);
        if (res.data.error_code == 0) {
          let datae = res.data.data
          setGeoJsonData(datae)
          const map = mapRef.current.leafletElement;
          let gjson = L.geoJson(res.data.data, {
            style: {
              "color": "#00A6A6",
              "weight": 1,
              "opacity": 1,
              "fillOpacity": 0
            }
          }).addTo(map);


          gjson.eachLayer(function (layer) {
            //  initMaps(data);
            layer.addEventListener("click", function () {
              let types = type === 'provinsi' ? 'kabupaten' : type === 'kabupaten' ? 'desa' : '';
              let lay = layer.feature.properties
              let dataSend = type === 'provinsi' ? lay.PROVNO : type === 'kabupaten' ? lay.KABKOTNO + '&prov_no=' + lay.PROVNO : lay.DESA;


              if (types) {
                localStorage.setItem('iddes', null)
                gjson.remove();
                map.fitBounds(layer.getBounds());
                getGeojson(types, dataSend);
                idDataDesa = ''
              } else {
                idDataDesa = lay.IDDESA
                setIdDesa(lay.IDDESA)
                localStorage.setItem('dprov', lay.PROVINSI)
                localStorage.setItem('dkab', lay.KABKOT)
                localStorage.setItem('dkec', lay.KECAMATAN)
                localStorage.setItem('ddes', lay.DESA)

                console.log(rightSideTitle, 'title side ')
                let tingkat = JSON.parse(dropdown.tingkat);
                let jenis = JSON.parse(dropdown.jenis);
                let tahun = JSON.parse(dropdown.tahun);
                let model = dropdown.model ? JSON.parse(dropdown.model) : '';
                setSelectedDesa(lay.IDDESA)
                let provkab = lay.PROVNO + '' + lay.KABKOTNO


                if (dataUser) {
                  let duprokab = dataUser.wilayah__prov_no + '' + dataUser.wilayah__kab_kot_no
                  if (duprokab == provkab) {
                    setIsCanEdit(true)
                    getVisual(lay.IDDESA, tingkat, jenis, tahun, model)
                    getBahayaByParam(lay.IDDESA, tingkat, jenis, tahun, model);
                  } else {
                    setIsCanEdit(false)
                  }
                } else {
                  setIsCanEdit(false)
                }

                getDataSide(idDataDesa)

                localStorage.setItem('iddes', lay.IDDESA)
              }


            })
            layer.addEventListener("mouseover", function (event) {
              let lay = layer.feature.properties
              let dataSend = type === 'provinsi' ? lay.PROVINSI : type === 'kabupaten' ? lay.KABKOT : lay.DESA;
              layer.bindTooltip("<div>" + dataSend + "</div>").openTooltip();
            });
          })

        } else {


        }
        setLoadGeoJson(false)

      })
      .catch((err) => {
        setIsLoading(false);
        console.log("Errorr:", err);

      })
  }



  const setSidebar = (state) => {
    if (state == 'left') {
      setLeftMap(!leftMap)
    } else {
      if (dropdown.tahun && dropdown.jenis && dropdown.tingkat) {
        let parsemodel = JSON.parse(dropdown.tingkat)
        if (parsemodel.type_id == 3 || parsemodel.type_id == 4) {
          setRightMap(!rightMap)
        } else {
          if (dropdown.model) {
            setRightMap(!rightMap)
          } else {
            openNotification('danger', 'Mohon lengkapi data dropdown dan pilih peta dahulu.')
          }
        }

      } else {
        openNotification('danger', 'Mohon lengkapi data dropdown dan pilih peta dahulu.')

      }

    }
  }
  const getMapLayer = () => {
    api.getLayers()
      .then((res) => {
        // setIsLoading(true);
        if (res.data.error_code == 0) {
          setListLayer(res.data.data)
        } else {

          openNotification('danger', res.data.message)

        }

      })
      .catch((err) => {
        setIsLoading(false);
        // alert('gagal load peta')
        console.log("Errorr:", err);

      })
  }
  const reset = () => {
    window.location.reload()

  }
  const searchData = () => {


    if (!dropdown.tingkat) {
      openNotification('danger', 'Tingkat harus dipilih!')
      return
    } else if (!dropdown.jenis) {
      openNotification('danger', 'Jenis harus dipilih!')
      return
    } else if (!dropdown.tahun) {
      openNotification('danger', 'Tahun harus dipilih!')
      return
    } else {

      let dtingkat = JSON.parse(dropdown.tingkat)
      let djenis = JSON.parse(dropdown.jenis)
      let dtahun = JSON.parse(dropdown.tahun)
      let dmodel = dropdown.model ? JSON.parse(dropdown.model) : ''
      let comb = ''
      if (dtingkat.name === 'Keterpaparan' || dtingkat.name === 'Kerentanan') {
        comb = dtingkat.code + djenis.code + dtahun.code + djenis.code
      } else {
        comb = dtingkat.code + djenis.code + dtahun.code + dmodel.code
      }
      setRightSideTitle({
        tingkat: dtingkat,
        jenis: djenis,
        tahun: dtahun,
        model: dmodel

      })
      setDropdownCodeMap('apik:' + comb)
      setDisabledDd(true);
      setBaseLayer(true);
      setDataLegend(true);

      getGeojson('provinsi', null, true, dtingkat, djenis, dtahun, dmodel)



    }


  }

  const getDataSide = (iddes) => {
    setIsLoading(true);
    api.searchData(dropdown, iddes)
      .then((res) => {
        setIsLoading(false);
        if (res.data.error_code == 0) {
          setRightSideData(res.data.data == null ? [] : res.data.data)
          setRightMap(true)

        } else {

        }

      })
      .catch((err) => {
        setIsLoading(false);
        console.log("Errorr:", err);

      })
  }

  function closeNotification() {
    setAlert({
      isAlertOpen: false,
      alertState: 'danger',
    })
  }
  const openNotification = (condition, Message) => {
    if (condition == 'success') {
      setAlert({
        isAlertOpen: true,
        alertState: 'success',
        AlertMessage: Message
      })
    } else {
      setAlert({
        isAlertOpen: true,
        alertState: 'danger',
        AlertMessage: Message
      })
    }
    setTimeout(function () {
      closeNotification()
    }
      .bind(this), 5000);
  }
  const onChangeSlider = (val, type) => {
    // if (type == 'CHT') {
    let vale = (valueSlider['def' + type] * (val / 100)) + valueSlider['def' + type]
    setValueSlider({
      ...valueSlider,
      [type]: vale
    })

    let jenis = JSON.parse(dropdown.jenis);

    let dats = [{
      ch_tahunan: valueSlider.CHT,
      ch_bulanan: valueSlider.CHM,
      ch_optimum: valueSlider.CHO,
      su_tahunan: valueSlider.SUT,
      su_bulanan: valueSlider.SUM,
      su_optimum: valueSlider.SUO

    }]

    let total = gf.calculateRadar(jenis.type_id, dats)

    setResult({
      SUM: total.sumFinal,
      SUT: total.sutFinal,
      SUO: total.suoFinal,
      CHM: total.dataCHM,
      CHT: total.dataCHT,
      CHO: total.dataCHO
    })

    let dradar = {
      labels: ['SU O', 'SU T', 'SU M', 'CH O', 'CH T', 'CH M'],
      datasets: [
        {
          data: [total.sumFinal, total.sutFinal, total.suoFinal, 0, 0, 0],
          backgroundColor: 'rgba(0,166,166,0.5)',
          borderColor: 'rgba(0,166,166,1)',
          pointBackgroundColor: 'rgba(0,166,166,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(0,166,166,1)',
          label: 'SU'
        },
        {
          data: [0, 0, 0, total.dataCHM, total.dataCHT, total.dataCHO],
          backgroundColor: 'rgba(255,99,132,0.5)',
          borderColor: 'rgba(255,99,132,1)',
          pointBackgroundColor: 'rgba(255,99,132,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(255,99,132,1)',
          label: 'CH'
        }
      ]
    }
    let options = {
      responsive: true,
      maintainAspectRatio: false
    }
    setDataBahayaParams({ data: dradar, options: options })

    // }
  }

  const getUserById = (id) => {
    api.getUserById(id)
      .then((res) => {
        if (res.data.error_code == 0) {
          let datauser = res.data.data[0]
          console.log(datauser, 'datauser')
          setDataUser(datauser)
        } else {
          openNotification('danger', res.data.message)
        }

      })
  }

  const layerCheckBox = (e, index1, index2) => {
    let defaultlistLayer = listLayer;
    let filterByIndex1 = defaultlistLayer.filter((data, index) => index == index1);
    let filterByIndex2 = filterByIndex1[0].layers.filter((data, index) => index == index2)
    setIsLoadOverlay(true)
    if (filterByIndex2[0].jenis == 'xlsx') {
      getSelectedLayerExcel(index1, index2, filterByIndex2[0])
    } else {
      getSelectedLayerAPI(index1, index2, filterByIndex2[0])
    }
  }

  const getSelectedLayerExcel = (index1, index2, selectedLayer) => {
    let defaultlistLayer = listLayer;
    let listSelected = listSelectedLayer;
    if (selectedLayer.checked) {
      // map.removeLayer()

      defaultlistLayer[index1].layers[index2].checked = false
      setListLayer(defaultlistLayer)
    }
    else {
      listSelected.push(selectedLayer)
      setListSelectedLayer(listSelected)
      console.log(listSelectedLayer, 'new selected')
      api.getProgramExcelPin(selectedLayer.id)
        .then((res) => {
          if (res.data.error_code == 0) {
            let dtexcel = res.data.data
            for (let i = 0; i < defaultlistLayer.length; i++) {
              if (i == index1) {
                for (let j = 0; j < defaultlistLayer[i].layers.length; j++) {
                  if (j == index2) {
                    defaultlistLayer[i].layers[j].checked = !defaultlistLayer[i].layers[j].checked
                    defaultlistLayer[i].layers[j].api = dtexcel
                  }
                }
              }
            }

          } else {
            openNotification('danger', res.data.message)
          }
          // setListLayer(defaultlistLayer)


        })
        .catch((error) => {
          // handle your errors here
          setIsLoadOverlay(false)
          openNotification('danger', 'Gagal mengambil data, URL bermasalah')
          // console.error(error, ' isi error')
        })
    }

    setTimeout(() => {
      setIsLoadOverlay(false)
    }, 1000);

    console.log(defaultlistLayer)
  }

  const getSelectedLayerAPI = (index1, index2, selectedLayer) => {
    let defaultlistLayer = listLayer;
    const map = mapRef.current.leafletElement;
    let listSelected = listSelectedLayer;

    if (selectedLayer.checked) {
      defaultlistLayer.filter((data, index) => data.id !== selectedLayer.id);
      defaultlistLayer[index1].layers[index2].checked = false
    } else {
      listSelected.push(selectedLayer)
      setListSelectedLayer(listSelected)
      fetch(defaultlistLayer[index1].layers[index2].api_target)
        .then(response => response.json())
        .then((jsonData) => {
          // jsonData is parsed json object received from url
          console.log(jsonData, 'response json')
          for (let i = 0; i < defaultlistLayer.length; i++) {
            if (i == index1) {
              for (let j = 0; j < defaultlistLayer[i].layers.length; j++) {
                if (j == index2) {
                  defaultlistLayer[i].layers[j].checked = !defaultlistLayer[i].layers[j].checked
                  defaultlistLayer[i].layers[j].api = jsonData

                  // let gjson = L.geoJson(jsonData, {
                  //   style: {
                  //     "color": "#00A6A6",
                  //     "weight": 1,
                  //     "opacity": 1,
                  //     "fillOpacity": 0
                  //   }
                  // }).addTo(map);

                }
              }
            }
          }


          setIsLoadOverlay(false)

        })
        .catch((error) => {
          // handle your errors here
          setIsLoadOverlay(false)
          openNotification('danger', 'Gagal mengambil data, URL bermasalah')
          // console.error(error, ' isi error')
        })
    }

    setTimeout(() => {
      setIsLoadOverlay(false)
    }, 500);
    setListLayer(defaultlistLayer)
    console.log(defaultlistLayer)
  }


  const addRemoveLayer = (selectedlyr) => {
    console.log(selectedlyr)
  }

  const doProccessLayer = () => {


  }

  useEffect(() => {
    localStorage.setItem('iddes', null);
    getMasterType('Level');
    getMasterType('Legend');
    getMapLayer();
    if (localStorage.getItem('token')) {
      // if()
      getUserById(localStorage.getItem('email'));
    }



    //get native Map instance
  }, []);


  return (
    <div className={styleCss.mapContainer}>
      <Navbar
        // routes={routes}
        // handleDrawerToggle={handleDrawerToggle}
        // {...rest}
        width={rightMap ? 'calc(100% - 300px)' : '100%'}
      />
      <LoadingOverlay
        active={isLoadOverlay}
        spinner
        text='Sedang menyimpan data...'
        style={{ color: 'white' }}
      >
      </LoadingOverlay>
      <Snackbar
        place="tc"
        color={alert.alertState}
        icon={alert.alertState == 'success' ? CheckIcon : ErrorIcon}
        message={alert.AlertMessage}
        open={alert.isAlertOpen}
        closeNotification={() => closeNotification()}
        close
      />
      <div className={styleCss.selectHome}>

        <FormControl  >
          <GridContainer>

            <NativeSelect
              className={styleCss.SelectCari}

              id="demo-customized-select-native"
              value={dropdown.tingkat}
              name="tingkat"
              onChange={handleChangeSelect}
              input={<BootstrapInput />}
              disabled={disabledDd}

            >
              <option style={{ color: '#000000', fontWeight: 300 }} aria-label="None" value="" disabled >Pilihan Tingkat</option>
              {
                listTingkat.map((drp, index) => {
                  return (
                    <option style={{ color: '#000000' }} key={'tingkat' + index} value={JSON.stringify(drp)}>{drp.name}</option>
                  )
                })
              }

            </NativeSelect>

            <NativeSelect
              style={{ marginRight: "10px" }}
              id="demo-customized-select-native"
              value={dropdown.jenis}
              name="jenis"
              onChange={handleChangeSelect}
              input={<BootstrapInput />}
              disabled={disabledDd}
            >
              <option style={{ color: '#000000' }} aria-label="None" value="" >Pilihan Jenis</option>
              {
                listJenis.map((drp, index) => {
                  return (
                    <option style={{ color: '#000000' }} key={'jenis' + index} value={JSON.stringify(drp)}>{drp.name}</option>
                  )
                })
              }
            </NativeSelect>

            <NativeSelect
              style={{ marginRight: "10px" }}
              id="demo-customized-select-native"
              value={dropdown.tahun}
              name="tahun"
              onChange={handleChangeSelect}
              input={<BootstrapInput />}
              disabled={disabledDd}
            >
              <option style={{ color: '#000000' }} aria-label="None" value="" >Pilihan Tahun</option>
              {
                listTahun.map((drp, index) => {
                  return (
                    <option style={{ color: '#000000' }} key={'tahun' + index} value={JSON.stringify(drp)}>{drp.name}</option>
                  )
                })
              }
            </NativeSelect>

            {
              dropdown.jenis.includes('APIK') || dropdown.jenis.includes('SIDIK') ?
                ''
                :
                <NativeSelect

                  id="demo-customized-select-native"
                  value={dropdown.model}
                  name="model"
                  onChange={handleChangeSelect}
                  input={<BootstrapInput />}
                  disabled={disabledDd}
                >
                  <option style={{ color: '#000000' }} aria-label="None" value="" >Pilihan Model</option>
                  {
                    listModel.map((drp, index) => {
                      return (
                        <option style={{ color: '#000000' }} key={'model' + index} value={JSON.stringify(drp)}>{drp.name}</option>
                      )
                    })
                  }

                </NativeSelect>


            }



          </GridContainer>
        </FormControl>
        <IconButton aria-label="Cari" className={styleCss.ButtonCari} onClick={() => searchData()} >
          <ArrowForwardIcon />
        </IconButton>
        {
          isReset ?
            <IconButton aria-label="Cari" className={styleCss.ButtonReset} onClick={() => reset()} >
              <ReplayIcon />
            </IconButton> : ''
        }





      </div>

      <div className={leftMap ? styleCss.sidebarLeft : styleCss.sidebarLeftNone}>
        <div className={leftMap ? styleCss.sidebarLeftIconActive : styleCss.sidebarLeftIcon} onClick={() => setSidebar('left')}>
          <span className={styleCss.layerspan}><FontAwesomeIcon icon={faLayerGroup} /> Layer</span>
        </div>
        <div className={leftMap ? styleCss.sidebarLeftContent : styleCss.leftSidebarDataNone}>
          <div className={styleCss.sidebarLeftContentTitle}>
            Layer Properties
          </div>
          <div >
            <Paper className={styleCss.paperSearch}>

              <InputBase
                // type="button"
                className={styleCss.inputSearch}
                placeholder="Search Layer..."
              />
              <IconButton type="button" className={styleCss.iconButton} aria-label="search">
                <SearchIcon />
              </IconButton>



            </Paper>

          </div>
          <div>
            <FormControlLabel
              style={{ width: '100%', padding: 5, color: 'white !important' }}
              control={<Checkbox name="BaseLayer" checked={baseLayer} onClick={() => setBaseLayer(!baseLayer)} className={styleCss.checkboxTextFormat} />}
              label="Base Layer"


            />
            {
              listLayer.map((lyr, index) => {
                return (
                  <div>
                    <FormControl component="fieldset" className={styleCss.checkboxTextFormat} >
                      <FormLabel component="legend" className={styleCss.checkboxTextFormatTitle} style={{ fontWeight: '600 !important', width: '100%' }}>{lyr.created_detail__name}</FormLabel>
                      <FormGroup>
                        {
                          lyr.layers.map((lyrC, index2) => {
                            return (
                              <FormControlLabel

                                control={<Checkbox name="gilad" key={'checked' + index + '' + index2} checked={lyrC.checked == true ? lyrC.checked : false} onClick={(e) => layerCheckBox(e, index, index2)} className={styleCss.checkboxTextFormat} />}
                                label={lyrC.program_name + ' - ' + lyrC.layer_name}
                              />
                            )
                          })
                        }
                      </FormGroup>
                    </FormControl>
                  </div>
                )
              })
            }

          </div>

        </div>

      </div>
      <Map center={[0.7893, 120.9213]} zoomControl={false} zoom={5} animate={false} className={rightMap ? styleCss.Maphalf : styleCss.Map}
        scrollWheelZoom={false}
        // onlayeradd={esrimap()}
        ref={mapRef}>
        {
          isLoading ?
            <div>
              <CircularProgress className={styleCss.spinnerMap} />
            </div>
            :
            <div>
              <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                zIndex={-2}
              />

              {
                baseLayer ?
                  <WMSTileLayer
                    layers='apik:apik_desa'
                    url="https://geoserver.piarea.co.id:8443/geoserver/gwc/service/wms/"
                    format="image/PNG"
                    // opacity= "0.7"
                    styles={dropdownCodeMap}

                  /> : ''
              }

              {
                listLegend.length > 0 ?
                  <Control position="bottomleft">
                    <div className={styleCss.BoxLegend} >
                      <div className={styleCss.LegendCms}>
                        <GridContainer >
                          <GridItem xs={4}>
                            <NotesIcon className={styleCss.IconInfoLegendHeader} />
                          </GridItem>
                          <GridItem xs={8}>

                            <Typography className={styleCss.TextLegend}>Legend</Typography>

                          </GridItem>
                        </GridContainer>
                      </div>

                      {
                        dataLegend ?
                          <table>

                            {
                              listLegend.map((dl, index) => {
                                return (
                                  <tr>
                                    <td> <div style={{
                                      width: 14, height: 14, background: dl.fill, border: '1px solid' + dl.outline, marginLeft: 20, marginBottom: 5, marginTop: 5
                                    }}></div></td>
                                    <td>
                                      {dl.range}
                                    </td>
                                    <td>{dl.category}</td>


                                  </tr>
                                )
                              })
                            }

                          </table>
                          : <div style={{ padding: 20, textAlign: 'center' }}>
                            <FontAwesomeIcon icon={faExclamationCircle} /> Belum ada data terpilih
                          </div>

                      }



                    </div>
                  </Control>
                  : ''


              }
              {loadGeoJson ?
                <GeoJSON data={geoJsonData} key={'geojson-gambar'} style={{ "color": "#00A6A6", "weight": 1, "opacity": 0.65 }} /> :
                ''
              }



              {listLayer.map((datas, index) => {
                datas.layers.map((lyr, index2) => {
                  return (
                    // <div>
                    // {
                    lyr.checked && lyr.jenis == 'api' ?

                      <GeoJSON data={lyr.api} key={'geojson' + index + '' + index2} style={{ "color": "#00A6A6", "weight": 1, "opacity": 1 }} /> :
                      lyr.checked && lyr.jenis == 'excel' ?
                        <LayerGroup id={'lyrgrp' + index + '' + index2}>
                          {
                            lyr.api.map((exl, index3) => {
                              console.log(exl, 'isi omo')
                              return (
                                <Marker position={[exl.lat, exl.long]} key={'latlong' + index + '' + index2 + '' + index3}>
                                  <Popup>
                                    {exl.form_layer__layer_name} : {exl.data_value}
                                  </Popup>
                                </Marker>
                              )
                            })}
                        </LayerGroup>

                        : ''

                    // }
                    // </div>
                  )
                })
              })}

              <div style={{ marginTop: 50 }}>
                <ZoomControl position="topright" ></ZoomControl>
              </div>
            </div>

        }




      </Map >

      <div className={rightMap ? styleCss.sidebarRight : styleCss.sidebarRightNone}>
        <div className={styleCss.sidebarRightIcon} onClick={() => setSidebar()} >
          <FontAwesomeIcon icon={faInfoCircle} className={styleCss.sidebarRightIconPos} />

        </div>
        <div className={rightMap ? styleCss.sidebarRightContent : styleCss.sidebarRightContentNone}>
          <div className={styleCss.sidebarRightContentTitle}>
            Visualisasi Data  {rightSideTitle.tingkat.name}{' - ' + rightSideTitle.jenis.name}
          </div>



          {
            (rightSideTitle.tingkat.name == "Bahaya") && (rolesid == 2) && isCanEdit ?
              <div>

                <table>
                  <tr>

                    <td style={{ fontWeight: 600 }}>
                      Info Wilayah
                        </td>

                  </tr>
                  <Divider />
                  <tr>

                    <td><FontAwesomeIcon icon={faLandmark} /> Provinsi</td>
                    <td>: {localStorage.getItem('dprov')}</td>
                  </tr>
                  <tr>
                    <td><FontAwesomeIcon icon={faCity} /> Kab/Kota</td>
                    <td>: {localStorage.getItem('dkab')}</td>
                  </tr>
                  <tr>
                    <td><FontAwesomeIcon icon={faBuilding} /> Kecamatan</td>
                    <td>: {localStorage.getItem('dkec')}</td>
                  </tr>
                  <tr>
                    <td><FontAwesomeIcon icon={faArchway} /> Desa/Kel</td>
                    <td>: {localStorage.getItem('ddes')}</td>
                  </tr>

                </table>

                <Radar data={dataBahayaParams.data} options={dataBahayaParams.options} />




                <div className={styleCss.sidebarRightContentTitle}>
                  Perubahan Curah Hujan
                  </div>
                <div>
                  <Typography id="discrete-slider-custom" gutterBottom>
                    Rata-rata Tahunan
                    </Typography>
                  <div style={{ width: '70%', display: 'inline-block' }}>
                    <Slider
                      defaultValue={0}
                      onChange={(event, val) => { onChangeSlider(val, 'CHT') }}
                      aria-labelledby="discrete-slider-custom"
                      step={1}
                      valueLabelDisplay="auto"
                      marks={labelSH}
                      min={-100}
                      max={100}

                    />
                  </div>
                  <input style={{ width: '15%', float: 'right', textAlign: 'center' }} readOnly value={valueSlider.CHT} />
                </div>
                <div>
                  <Typography id="discrete-slider-custom" gutterBottom>

                    Rata-rata Optimum
                    </Typography>
                  <div style={{ width: '70%', display: 'inline-block' }}>
                    <Slider
                      defaultValue={0}
                      onChange={(event, val) => { onChangeSlider(val, 'CHM') }}
                      aria-labelledby="discrete-slider-custom"
                      step={1}
                      valueLabelDisplay="auto"
                      marks={labelSH}
                      min={-100}
                      max={100}

                    />
                  </div>
                  <input style={{ width: '15%', float: 'right', textAlign: 'center' }} readOnly value={valueSlider.CHM} />
                </div>
                <div>
                  <Typography id="discrete-slider-custom" gutterBottom>
                    Rata-rata Musiman
                    </Typography>
                  <div style={{ width: '70%', display: 'inline-block' }}>
                    <Slider
                      defaultValue={0}
                      onChange={(event, val) => { onChangeSlider(val, 'CHO') }}
                      aria-labelledby="discrete-slider-custom"
                      step={1}
                      valueLabelDisplay="auto"
                      marks={labelSH}
                      min={-100}
                      max={100}

                    />
                  </div>
                  <input style={{ width: '15%', float: 'right', textAlign: 'center' }} readOnly value={valueSlider.CHO} />
                </div>

                <div className={styleCss.sidebarRightContentTitle}>
                  Perubahan Suhu
                  </div>
                <div>
                  <Typography id="discrete-slider-custom" gutterBottom>
                    Rata-rata Tahunan
                    </Typography>
                  <div style={{ width: '70%', display: 'inline-block' }}>
                    <Slider
                      defaultValue={0}
                      onChange={(event, val) => { onChangeSlider(val, 'SUT') }}
                      aria-labelledby="discrete-slider-custom"
                      step={0.1}
                      valueLabelDisplay="auto"
                      marks={labelSU}
                      min={-2}
                      max={2}

                    />
                  </div>
                  <input style={{ width: '15%', float: 'right', textAlign: 'center' }} readOnly value={valueSlider.SUT} />
                  <Typography id="discrete-slider-custom" gutterBottom>

                    Rata-rata Optimum
                    </Typography>
                  <div style={{ width: '70%', display: 'inline-block' }}>
                    <Slider
                      defaultValue={0}
                      onChange={(event, val) => { onChangeSlider(val, 'SUM') }}
                      aria-labelledby="discrete-slider-custom"
                      step={0.1}
                      valueLabelDisplay="auto"
                      marks={labelSU}
                      min={-2}
                      max={2}

                    />
                  </div>
                  <input style={{ width: '15%', float: 'right', textAlign: 'center' }} readOnly value={valueSlider.SUM} />
                  <Typography id="discrete-slider-custom" gutterBottom>
                    Rata-rata Musiman
                    </Typography>
                  <div style={{ width: '70%', display: 'inline-block' }}>
                    <Slider
                      defaultValue={0}
                      onChange={(event, val) => { onChangeSlider(val, 'SUO') }}
                      aria-labelledby="discrete-slider-custom"
                      step={0.1}
                      valueLabelDisplay="auto"
                      marks={labelSU}
                      min={-2}
                      max={2}

                    />
                  </div>
                  <input style={{ width: '15%', float: 'right', textAlign: 'center' }} readOnly value={valueSlider.SUO} />
                </div>

                <div style={{ textAlign: 'center' }}>
                  <Button variant="outlined" onClick={() => doSubmit()} className={styleCss.btnSaveValidasi}>Save</Button>
                  <IconButton className={styleCss.IconPrintBahayaTersimpans}  >
                    <PrintIcon />
                  </IconButton>
                </div>
              </div>
              :
              < table >
                {
                  rightSideData.length !== 0 ?
                    <table>
                      <tr>

                        <td style={{ fontWeight: 600 }}>
                          Info Wilayah
                        </td>

                      </tr>
                      <Divider />
                      <tr>

                        <td><FontAwesomeIcon icon={faLandmark} /> Provinsi</td>
                        <td>: {localStorage.getItem('dprov')}</td>
                      </tr>
                      <tr>
                        <td><FontAwesomeIcon icon={faCity} /> Kab/Kota</td>
                        <td>: {localStorage.getItem('dkab')}</td>
                      </tr>
                      <tr>
                        <td><FontAwesomeIcon icon={faBuilding} /> Kecamatan</td>
                        <td>: {localStorage.getItem('dkec')}</td>
                      </tr>
                      <tr>
                        <td><FontAwesomeIcon icon={faArchway} /> Desa/Kel</td>
                        <td>: {localStorage.getItem('ddes')}</td>
                      </tr>
                      <tr>
                        <td></td>
                      </tr>
                      <tr>
                        <td></td>
                      </tr>
                      <tr>
                        <td></td>
                      </tr>
                    </table> : ''
                }

                {
                  rightSideData.length == 0 ?
                    <tr style={{ textAlign: 'center' }}>
                      <FontAwesomeIcon icon={faExclamationCircle} />  Data belum tersedia
              </tr>

                    : <tr style={{ marginTop: 15 }}>
                      <td style={{ fontWeight: 600 }}>Faktor yang Berkontribusi </td>

                    </tr>
                }

                {
                  rightSideData.length == 0 ?
                    ''

                    : <Divider />
                }





                {

                  rightSideData.map((rdata, index) => {
                    return (
                      <tr>
                        <td>&#x25CF; {rdata.name}</td>
                        {/* <td>: {rdata.value.toFixed(2)}</td> */}
                      </tr>
                    )
                  })
                }

              </table>



          }
        </div>
      </div>






      <div className={styleCss.footerHome}>
        @2020 Copyright APIK
        </div>
    </div >
  );
}
