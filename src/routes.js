/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import EventIcon from '@material-ui/icons/Event';
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
// core components/views for Admin layout
import HomeApikPage from "views/homeApik/homeApik.js";
import adminDaerahProfilePage from "views/AdminDaerah/adminDaerahProfile.js"
import DataKerentananPage from "views/AdminDaerah/daftarDataKerentanan.js"
import EditProfileAdminDaerahPage from "views/AdminDaerah/editProfilAdminDaerah.js"
import AdminProgramProfilePage from "views/AdminProgram/adminProgramProfile.js"
import EditProgramProfilePage from "views/AdminProgram/editProfilAdminProgram.js"
import Validasi from "views/AdminDaerah/validation.js"
import Adaptasi from "views/AdminDaerah/adaptation.js"



import NotificationsPage from "views/Notifications/Notifications.js";
import UpgradeToPro from "views/UpgradeToPro/UpgradeToPro.js";
// core components/views for RTL layout
import RTLPage from "views/RTLPage/RTLPage.js";


const dashboardRoutes = [
  {
    path: "/home",
    name: "Home Apik",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: HomeApikPage,
    layout: "/admin"
  },
  {
    path: "/validasi",
    name: "Admin Daerah Validasi",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: Validasi,
    layout: "/admin"
  },
  {
    path: "/adaptasi",
    name: "Admin Daerah Adaptasi",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: Adaptasi,
    layout: "/admin"
  },
  {
    path: "/profile",
    name: "Admin Daerah Profile",
    rtlName: "ملف تعريفي للمستخدم",
    icon: EventIcon,
    component: adminDaerahProfilePage,
    layout: "/admin"
  },
  {
    path: "/data-kerentanan",
    name: "Data Kerentanan",
    rtlName: "خرائط",
    icon: LocationOn,
    component: DataKerentananPage,
    layout: "/admin"
  },
  {
    path: "/profile-edit-admin-daerah",
    name: "Edit Profil Admin Daerah",
    icon: LibraryBooks,
    component: EditProfileAdminDaerahPage,
    layout: "/admin"
  },
  {
    path: "/program-profile",
    name: "Admin Program Profile",
    icon: LibraryBooks,
    component: AdminProgramProfilePage,
    layout: "/admin"
  },
  {
    path: "/edit-program-profile",
    name: "EditProgramProfile",
    rtlName: "الرموز",
    icon: BubbleChart,
    component: EditProgramProfilePage,
    layout: "/admin"
  },




];

export default dashboardRoutes;
